require "numru/ggraph"
include NumRu

nx = 360
ny = 120
nz = 1
nv = 3

gues = NArray.to_na(File.read("gues_me.grd"), NArray::SFLOAT, nx, ny, nz, nv)
anal = NArray.to_na(File.read("anal_me.grd"), NArray::SFLOAT, nx, ny, nz, nv)
guessp = NArray.to_na(File.read("gues_sp.grd"), NArray::SFLOAT, nx, ny, nz, nv)
analsp = NArray.to_na(File.read("anal_sp.grd"), NArray::SFLOAT, nx, ny, nz, nv)

DCL::swpset("iwidth", DCL::swpget("iwidth") * 2)
DCL::swpset("iheight", DCL::swpget("iheight") * 2)
DCL::gropn(4)
DCL::sgscmn(14)

diff = anal[false, 0] - gues[false, 0]
p diff.max

maxval = [anal[false, 0].abs.max, gues[false, 0].abs.max].max

DCL::grfrm
DCL::grsvpt(0.2, 0.8, 0.2, 0.8)
DCL::grswnd(1, nx, 1, ny)
DCL::grstrf
DCL::uegtla(-maxval, maxval, -80)
DCL::uetonc(anal[false, 0])
DCL::usdaxs
DCL::udcntz(diff)
DCL::uxsttl("T", "anal", 0)
DCLExt.color_bar

maxdiff = diff.abs.max

DCL::grfrm
DCL::grsvpt(0.2, 0.8, 0.2, 0.8)
DCL::grswnd(1, nx, 1, ny)
DCL::grstrf
DCL::uegtla(-maxdiff, maxdiff, -80)
DCL::uetonc(diff)
DCL::usdaxs
DCL::uxsttl("T", "increment", 0)
DCLExt.color_bar

maxgssp = guessp[false, 0].abs.max

DCL::grfrm
DCL::grsvpt(0.2, 0.8, 0.2, 0.8)
DCL::grswnd(1, nx, 1, ny)
DCL::grstrf
DCL::uegtla(-maxgssp, maxgssp, -80)
DCL::uetonc(guessp[false, 0])
DCL::usdaxs
DCL::uxsttl("T", "gues sp", 0)
DCLExt.color_bar

maxansp = analsp[false, 0].abs.max

DCL::grfrm
DCL::grsvpt(0.2, 0.8, 0.2, 0.8)
DCL::grswnd(1, nx, 1, ny)
DCL::grstrf
DCL::uegtla(-maxansp, maxansp, -80)
DCL::uetonc(analsp[false, 0])
DCL::usdaxs
DCL::uxsttl("T", "anal sp", 0)
DCLExt.color_bar

spdiff = guessp[false, 0] - analsp[false, 0]
maxspdiff = spdiff.abs.max

DCL::grfrm
DCL::grsvpt(0.2, 0.8, 0.2, 0.8)
DCL::grswnd(1, nx, 1, ny)
DCL::grstrf
DCL::uegtla(-maxspdiff, maxspdiff, -80)
DCL::uetonc(spdiff)
DCL::usdaxs
DCL::uxsttl("T", "sp diff", 0)
DCLExt.color_bar


DCL::grcls
