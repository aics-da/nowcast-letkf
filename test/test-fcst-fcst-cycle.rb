require "fileutils"

Dir.chdir("../nowcast/letkf/run")

workdir = "../../../test/work"
initdir = "../../../test/init"
guessdir = "../../../test/out/gues"
analdir = "../../../test/out/anal"
model = "../../model/nowcast.exe"
enssize = 20
input = "../../../test/obs/obs0002.bin"

for cycle in 1..2
  puts "cycle: #{cycle}"
  if cycle == 1
    command = "ruby ensfcst.rb #{workdir} #{initdir} #{guessdir}/#{cycle} #{model} #{enssize} #{input}"
  else
    command = "ruby ensfcst.rb #{workdir} #{analdir}/#{cycle - 1} #{guessdir}/#{cycle} #{model} #{enssize} #{input}"
  end
  puts command
  system(command)


  ### CONVERT GUESS TO INITIAL DATA WITHOUT ANALYSIS
  enssize.times{|i|
    FileUtils.mkdir_p("#{analdir}/#{cycle}/#{i + 1}")
    ensid = format("%03d", i + 1)
    File.open("#{guessdir}/#{cycle}/gs01#{ensid}.grd"){|fin|
      size = fin.size
      raise unless size % 3 == 0
      File.open("#{analdir}/#{cycle}/#{i + 1}/vector_u_001.bin", "w"){|fout|
        fout.puts(fin.read(size / 3))
      }
      File.open("#{analdir}/#{cycle}/#{i + 1}/vector_v_001.bin", "w"){|fout|
        fout.puts(fin.read(size / 3))
      }
      File.open("#{analdir}/#{cycle}/#{i + 1}/vector_w_001.bin", "w"){|fout|
        fout.puts(fin.read(size / 3))
      }
    }

    system("cp -p #{initdir}/#{i + 1}/setting.namelist #{analdir}/#{cycle}/#{i + 1}/")
  }

end
