require "fileutils"
require "narray"

letkfnamelist = File.expand_path("./letkf.namelist")

Dir.chdir("../nowcast/letkf/run")

workdir = "../../../test/work"
initdir = "../../../test/init"
guesdir = "../../../test/out/gues"
analdir = "../../../test/out/anal"
model = "../../model/nowcast.exe"
letkf = "../letkf.exe"
obsope = "../obsope.exe"
enssize = 20
input = "../../../test/obs/obs0002.bin"

workdir = File.expand_path(workdir)
guesdir = File.expand_path(guesdir)
letkfdir = File.join(workdir, "letkf")
FileUtils.mkdir_p(letkfdir)
system("rm -f #{letkfdir}/*")
letkf = File.expand_path(letkf)

File.open(File.join(letkfdir, "lon.dat"), "w"){|f|
  f.puts NArray.sfloat(360).indgen!.mul!(1.0).add!(0.5).to_a.join(", ")
}
File.open(File.join(letkfdir, "lat.dat"), "w"){|f|
  f.puts NArray.sfloat(120).indgen!.mul!(1.0).add!(-59.5).to_a.join(", ")
}
File.open(File.join(letkfdir, "lev.dat"), "w"){|f|
  f.puts "0.975"
}

for cycle in 1..10
  puts "cycle: #{cycle}"
  ### ENSEMBLE FORECAST
  if cycle == 1
    command = "ruby ensfcst.rb #{workdir} #{initdir} #{guesdir}/#{cycle} #{model} #{enssize} #{input}"
  else
    command = "ruby ensfcst.rb #{workdir} #{analdir}/#{cycle - 1} #{guesdir}/#{cycle} #{model} #{enssize} #{input}"
  end
  puts command
  system(command)

  enssize.times{|i|
    FileUtils.mkdir_p("#{analdir}/#{cycle}/#{i + 1}")
    system("cp -p #{initdir}/#{i + 1}/setting.namelist #{analdir}/#{cycle}/#{i + 1}/")
  }

  ### OBSOPE
  p pwd = Dir.pwd
  FileUtils.cp(letkfnamelist, letkfdir, :preserve => true)
  FileUtils.cp(letkf, letkfdir, :preserve => true)
  FileUtils.cp(obsope, letkfdir, :preserve => true)
  Dir.chdir(letkfdir)
  p Dir.pwd
  #system("touch obsin.dat")
  obslen = NArray.int(1).fill!(6 * 4)
  obsdat = NArray.sfloat(6)
  File.open("obsin.dat", "w"){|fobs|
    for i in 0..(cycle == 10 ? 1 : 0)
      obsdat[0] = 2819  # u wind
      obsdat[1] = 180.0 + i * 10.0 #rlon
      obsdat[2] = 0.0   #rlat
      obsdat[3] = 0.0   #rlev
      obsdat[4] = 2.0   #odat
      obsdat[5] = 0.1   #oerr
      fobs.print obslen.to_s
      fobs.print obsdat.to_s
      fobs.print obslen.to_s
    end
  }
  enssize.times{|i|
    p i + 1
    memid = format('%03d', i + 1)
    gsf = File.join(guesdir, cycle.to_s, "gs01#{memid}.grd")
    raise "#{gsf} not found" unless File.exist?(gsf)
    FileUtils.symlink(gsf, letkfdir, {:force => true})
    FileUtils.symlink(gsf, File.join(letkfdir, "gues.grd"), {:force => true})
    raise unless system("./" + File.basename(obsope))
    FileUtils.mv("obsout.dat", "obs01#{memid}.dat")
  }

  ### LETKF
  raise unless system("mpiexec_mpt #{File.basename(letkf)}")
  raise unless system("mv #{letkfdir}/anal*.grd #{letkfdir}/gues_*.grd #{letkfdir}/NOUT* #{analdir}/#{cycle}")

  ### CREATE NEXT INITIAL CONDITION
  enssize.times{|i|
    ensid = format("%03d", i + 1)
    File.open("#{analdir}/#{cycle}/anal#{ensid}.grd"){|fin|
      size = fin.size
      raise unless size % 3 == 0
      File.open("#{analdir}/#{cycle}/#{i + 1}/vector_u_001.bin", "w"){|fout|
        fout.puts(fin.read(size / 3))
      }
      File.open("#{analdir}/#{cycle}/#{i + 1}/vector_v_001.bin", "w"){|fout|
        fout.puts(fin.read(size / 3))
      }
      File.open("#{analdir}/#{cycle}/#{i + 1}/vector_w_001.bin", "w"){|fout|
        fout.puts(fin.read(size / 3))
      }
    }
  }

  Dir.chdir(pwd)
end
