require "narray"

max_i = 11

na = []
na[0] = NArray.sfloat(360, 120)
for i in 1...max_i
  na[i] = na[0].dup
end

x0 = 180
y0 = 59.5
lscale = 10.0

for j in 0...(na[0].shape[1])
  for i in 0...(na[0].shape[0])
    na[0][i, j] = 100.0 * Math.exp(-((i - x0).to_f ** 2 + (j - y0).to_f ** 2) / (lscale * lscale))
  end
end

for i in 1...max_i
  na[i][1..-1, true] = na[i - 1][0..-2, true]
  na[i][0, true] = na[i - 1][-1, true]
end

for i in 0...max_i
  File.open("obs#{format('%04d', i + 1)}.bin", "w"){|f| f.print na[i].to_s}
end
