require "fileutils"
require "narray"
require "../../nowcast/letkf/run/parse_namelist"

nml = parse_namelist("setting.namelist.template")
domconf = nml["domain_config"]

datshape = [domconf["ddx"], domconf["ddy"], domconf["ddz"]].map!{|m| m.to_i}

u = NArray.sfloat(*datshape).fill!(2.0)
v = NArray.sfloat(*datshape).fill!(0.0)
w = NArray.sfloat(*datshape).fill!(0.0)

20.times{|i|
  memid = i + 1
  FileUtils.mkdir_p(memid.to_s)
  FileUtils.cp("setting.namelist.template", File.join(memid.to_s, "setting.namelist"))
  File.open(File.join(memid.to_s, "vector_u_001.bin"), "w"){|f| f.print u.to_s}
  File.open(File.join(memid.to_s, "vector_v_001.bin"), "w"){|f| f.print v.to_s}
  File.open(File.join(memid.to_s, "vector_w_001.bin"), "w"){|f| f.print w.to_s}
}
