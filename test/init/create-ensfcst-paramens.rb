require "fileutils"
require "narray"
require "../../nowcast/letkf/run/parse_namelist"

nml = parse_namelist("setting.namelist.template")
domconf = nml["domain_config"]

datshape = [domconf["ddx"], domconf["ddy"], domconf["ddz"]].map!{|m| m.to_i}

20.times{|i|
  u = NArray.sfloat(*datshape).randomn.mul!(1.0).add!(2.0)
  v = NArray.sfloat(*datshape).randomn.mul!(1.0)
  w = NArray.sfloat(*datshape).fill!(0.0)

  pwd = Dir.pwd
  p memid = i + 1
  FileUtils.mkdir_p(memid.to_s)
  File.open("setting.namelist.template"){|fin|
    FileUtils.mkdir_p(File.join("..", "work", memid.to_s))
    File.open(File.join("..", "work", memid.to_s, "setting.namelist"), "w"){|fout|
      fin.readlines.each{|l|
        case l
        when /use_precomputed_vector =/
          fout.puts "use_precomputed_vector = 0"
        when /calnt =/
          fout.puts "calnt = 1"
        when /frame_num =/
          fout.puts "frame_num = #{21 + i}"
        when /switching_lanczos =/
          fout.puts "switching_lanczos = 1"
        when /lanczos_window =/
          fout.puts "lanczos_window = #{21 + i}"
        when /lanczos_critical_wavelength =/
          fout.puts "lanczos_critical_wavelength = #{21 + i}"
        else
          fout.puts l
        end
      }
    }
  }
  Dir.chdir("../work/#{memid}")
  raise unless system("ln -sf ../../../nowcast/model/nowcast.exe .")
  raise unless system("ln -sf ../../obs/obs0001.bin input_0001.bin")
  raise unless system("ln -sf ../../obs/obs0002.bin input_0002.bin")
  puts `./nowcast.exe`
  raise unless system("cp -p vector_[uvw]_001.bin ../../init/#{memid}")
  Dir.chdir pwd

  FileUtils.cp("setting.namelist.template", File.join(memid.to_s, "setting.namelist"))
}
