require "fileutils"
require "narray"

$qsub_job_ids = []

def qsub(nodes, cpus, nmpi, dir, prg, nowait = false)
  host = `uname -n`
  if /hakushu/ =~ host
    script = <<"EOF"
#!/bin/sh
#PBS -l select=#{nodes}:ncpus=#{cpus}:mpiprocs=#{nmpi}
. /usr/share/modules/init/sh
module unload pgi-12.10
module load intel-2013.1.117
  cd #{dir}
  #{prg}
EOF

  elsif /yamazaki/ =~ host
    script = <<"EOF"
#!/bin/sh
#PBS -l nodes=#{nodes}:ppn=#{cpus.to_i / nodes.to_i}
. /usr/share/modules/init/sh
module unload pgi-12.10
module load intel-2013.1.117
module load mpt/2.05

  cd #{dir}
  #{prg}
EOF
  else
    raise "configuration for #{host} not found"
  end

  File.open("run-cycle-#{$$}.sh", "w"){|f| f.puts script}
  puts qsub = `qsub run-cycle-#{$$}.sh`
  raise unless /^([0-9]+)/ =~ qsub
  jobid = $1
  if nowait
    $qsub_job_ids.push jobid
  else
    while /#{jobid}/ =~ `qstat`
      sleep 10
    end
    puts "job #{jobid} finished"
  end
end

def qsub_wait
  waitflag = true
  p $qsub_job_ids
  while true
    sleep 10
    qstat = `qstat`
    $qsub_job_ids.dup.each{|jobid|
      unless /#{jobid}/ =~ qstat
        puts "job #{jobid} finished"
        $qsub_job_ids.delete(jobid)
      end
    }
    p $qsub_job_ids
    break if $qsub_job_ids.length == 0
  end
end

letkfnamelist = File.expand_path("./letkf.namelist")

Dir.chdir("../nowcast/letkf/run")

workdir = "../../../test/work"
initdir = "../../../test/init"
guesdir = "../../../test/out/gues"
analdir = "../../../test/out/anal"
model = "../../model/nowcast.exe"
vect2obs = "../../model/vect2obs.rb"
letkf = "../letkf.exe"
obsope = "../obsope.exe"
enssize = 20
inputdir = "../../../test/obs"

workdir  = File.expand_path(workdir)
guesdir  = File.expand_path(guesdir)
inputdir = File.expand_path(inputdir)
model    = File.expand_path(model)
vect2obs = File.expand_path(vect2obs)
obsope   = File.expand_path(obsope)
letkf    = File.expand_path(letkf)

trecdir = File.join(workdir, "trec")
letkfdir = File.join(workdir, "letkf")

FileUtils.mkdir_p(letkfdir)
system("rm -f #{letkfdir}/*")

File.open(File.join(letkfdir, "lon.dat"), "w"){|f|
  f.puts NArray.sfloat(360).indgen!.mul!(1.0).add!(0.5).to_a.join(", ")
}
File.open(File.join(letkfdir, "lat.dat"), "w"){|f|
  f.puts NArray.sfloat(120).indgen!.mul!(1.0).add!(-59.5).to_a.join(", ")
}
File.open(File.join(letkfdir, "lev.dat"), "w"){|f|
  f.puts "0.975"
}

for cycle in 1..10
  puts "cycle: #{cycle}"
  p pwd = Dir.pwd
  ### ENSEMBLE FORECAST
  input = File.join(inputdir, "obs#{format('%04d', cycle)}.bin")
  if cycle == 1
    command = "ruby ensfcst.rb #{workdir} #{initdir} #{guesdir}/#{cycle} #{model} #{enssize} #{input}"
  else
    command = "ruby ensfcst.rb #{workdir} #{analdir}/#{cycle - 1} #{guesdir}/#{cycle} #{model} #{enssize} #{input}"
  end
  puts command
  system(command)

  enssize.times{|i|
    FileUtils.mkdir_p("#{analdir}/#{cycle}/#{i + 1}")
    system("cp -p #{initdir}/#{i + 1}/setting.namelist #{analdir}/#{cycle}/#{i + 1}/")
  }

  ### TREC
  FileUtils.mkdir_p(trecdir)
  Dir.chdir(trecdir)
  FileUtils.cp(File.join(inputdir, "setting.namelist.template"), "setting.namelist", :preserve => true)
  FileUtils.symlink(model, "./", :force => true)
  FileUtils.symlink(File.join(inputdir, "obs#{format('%04d', cycle)}.bin"), "./input_0001.bin", :force => true)
  FileUtils.symlink(File.join(inputdir, "obs#{format('%04d', cycle + 1)}.bin"), "./input_0002.bin", :force => true)
  raise unless system("./#{File.basename(model)}")
  raise unless system("ruby #{vect2obs} #{File.join(letkfdir, "lon.dat")} #{File.join(letkfdir, "lat.dat")} #{File.join(letkfdir, "lev.dat")}")

  ### OBSOPE
  FileUtils.cp(letkfnamelist, letkfdir, :preserve => true)
  FileUtils.cp(letkf, letkfdir, :preserve => true)
  FileUtils.cp(obsope, letkfdir, :preserve => true)
  Dir.chdir(letkfdir)
  p Dir.pwd
  FileUtils.mv(File.join(trecdir, "obsin.dat"), ".")

  enssize.times{|i|
    p i + 1
    memid = format('%03d', i + 1)
    gsf = File.join(guesdir, cycle.to_s, "gs01#{memid}.grd")
    raise "#{gsf} not found" unless File.exist?(gsf)
    FileUtils.ln_sf(gsf, letkfdir)
    obsoped = File.join(letkfdir, memid)
    FileUtils.mkdir_p(obsoped)

    prog = <<"EOF"
set -e
ln -s ../letkf.namelist .
ln -s ../lon.dat .
ln -s ../lat.dat .
ln -s ../lev.dat .
ln -s ../obsin.dat .
ln -s #{gsf} ./gues.grd
../#{File.basename(obsope)}
mv obsout.dat ../obs01#{memid}.dat
EOF
    qsub(1, 1, 1, obsoped, prog, true)
  }
  qsub_wait

=begin
  enssize.times{|i|
    p i + 1
    memid = format('%03d', i + 1)
    gsf = File.join(guesdir, cycle.to_s, "gs01#{memid}.grd")
    raise "#{gsf} not found" unless File.exist?(gsf)
    FileUtils.symlink(gsf, letkfdir, {:force => true})
    FileUtils.symlink(gsf, File.join(letkfdir, "gues.grd"), {:force => true})
    #raise unless system("./" + File.basename(obsope))
    qsub(1, 40, 1, Dir.pwd, "./" << File.basename(obsope))
    FileUtils.mv("obsout.dat", "obs01#{memid}.dat")
  }
=end

  ### LETKF
  #raise unless system("mpiexec_mpt -n 1 ./#{File.basename(letkf)}")
  #puts `mpiexec_mpt -n 1 #{File.basename(letkf)}`
  #raise
  qsub(1, 40, 40, Dir.pwd, "mpiexec_mpt ./" << File.basename(letkf))
  raise unless system("mv #{letkfdir}/anal*.grd #{letkfdir}/gues_*.grd #{letkfdir}/NOUT* #{analdir}/#{cycle}")

  ### CREATE NEXT INITIAL CONDITION
  enssize.times{|i|
    ensid = format("%03d", i + 1)
    File.open("#{analdir}/#{cycle}/anal#{ensid}.grd"){|fin|
      size = fin.size
      raise unless size % 3 == 0
      File.open("#{analdir}/#{cycle}/#{i + 1}/vector_u_001.bin", "w"){|fout|
        fout.puts(fin.read(size / 3))
      }
      File.open("#{analdir}/#{cycle}/#{i + 1}/vector_v_001.bin", "w"){|fout|
        fout.puts(fin.read(size / 3))
      }
      File.open("#{analdir}/#{cycle}/#{i + 1}/vector_w_001.bin", "w"){|fout|
        fout.puts(fin.read(size / 3))
      }
    }
  }

  Dir.chdir(pwd)
end
