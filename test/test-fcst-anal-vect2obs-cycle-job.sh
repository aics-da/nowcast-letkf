#!/bin/sh
#PBS -l select=1:ncpus=40:mpiprocs=40
. /usr/share/modules/init/sh  ### this is required to use "module" command
module unload pgi-12.10
module load intel-2013.1.117

cd /home/otsuka/nowcast-letkf/test
#ulimit -s 100000000000
ulimit -s 1000000000
#export OMP_NUM_THREADS=40
ruby test-fcst-anal-vect2obs-cycle.rb

