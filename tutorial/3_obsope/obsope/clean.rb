target = ["*.grd",  "*.nc", "*.dat", "*~"]

20.times do |i|
  target.push("#{i + 1}")
end

system("make -f Makefile.gfortran clean") || raise
system("rm -rfv #{target.join(' ')}") || raise
