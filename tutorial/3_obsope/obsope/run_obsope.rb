require "fileutils"

### MAKE CONVERTER ###
system("make -f Makefile.gfortran") || raise

### SET PATH TO OBSOPE ###
obsope = "../../../nowcast/letkf/obsope/obsope.exe"
obsope = File.expand_path(obsope)
raise "obsope not found" unless File.exist?(obsope)

### SET PATH TO DATA ###
data = ARGV[0] || "../../data/cappi*.nc"
data = Dir.glob(data).sort.map!{|m| File.expand_path(m)}
raise if data.length == 0

### SET INPUT TIME INTERVAL ###
dt = (ARGV[1] || 480.0).to_f

### SETUP DATA FOR EACH ENSEMBLE MEMBER ###
pwd = Dir.pwd
20.times do |i|
  mem_dir = (i + 1).to_s ### STARTING FROM 1 ###
  FileUtils.mkdir_p(mem_dir) ### DIRECTORY FOR EACH MEMBER ###
  Dir.chdir(mem_dir)
  system("rm -rf ./*") || raise
  ### THIS IS JUST AN EXAMPLE: ALL MEMBERS ARE IDENTICAL ###
  %w( U V W ).each{|j|
    f = "../../first_guess/#{mem_dir}/burgers#{j}_001_002.nc"
    f = File.expand_path(f)
    raise "file #{f} not found" unless File.exist?(f)
    FileUtils.symlink(f, "./") ### 480-SECOND FORECAST ###
  }

  ### DATA CONVERTER ###
  system("../conv.exe #{data[0]} burgersU_001_002.nc burgersV_001_002.nc burgersW_001_002.nc lon lat 2000.0 #{dt}") || raise

  memid = format('%03d', i + 1)

  Dir.chdir(pwd)
  FileUtils.symlink("#{mem_dir}/gues.grd", "./gs01#{memid}.grd", :force => true)
  if i == 0
    FileUtils.symlink("#{mem_dir}/lon.dat", ".", :force => true)
    FileUtils.symlink("#{mem_dir}/lat.dat", ".", :force => true)
    FileUtils.symlink("#{mem_dir}/lev.dat", ".", :force => true)
  end
end

### OBS DATA ###
FileUtils.symlink("../trec/obsin.dat", ".", :force => true)

### RUN ###
system(obsope) || raise

### TEST ###
system("./test.exe") || raise




