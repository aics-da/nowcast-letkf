program test
  implicit none

  call read_letkf_obs()
  stop
  
contains

  subroutine read_letkf_obs()
    integer nobs, i, j
    real(4) :: letkf_obs(8)
    integer, parameter :: iunit = 91
    
    write(*, *) "reading first 10 data points from obs01001.dat"

    OPEN(iunit, FILE = "obs01001.dat", FORM = 'unformatted', ACCESS = 'sequential')
    do i = 1, 10
       read(iunit) letkf_obs
       write(*, *) letkf_obs
       
       read(iunit) letkf_obs
       write(*, *) letkf_obs
    end do !i
    close(iunit)
  end subroutine read_letkf_obs
end program test
