require "fileutils"

### SET PATH TO YOUR MODEL ###
model = "../../../../nowcast/src/nowcast.exe"
model = File.expand_path(model)
raise "model not found" unless File.exist?(model)

### SET PATH TO DATA ###
data = ARGV[0] || "../../data/cappi*.nc"
data = Dir.glob(data).sort.map!{|m| File.expand_path(m)}
raise if data.length == 0

### SET VARIABLE NAME ###
var_name = ARGV[1] || "REF"

### SET INPUT TIME INTERVAL ###
dt = (ARGV[2] || 480.0).to_f

dt_integ = 10.0 #[s]

### COMPUTE MOTION VECTOR ###
trec_dir = "./trec"
trec_dir = File.expand_path(trec_dir)
pwd = Dir.pwd
FileUtils.mkdir_p(trec_dir)
Dir.chdir(trec_dir)
system("rm -rf ./*") || raise
File.open("../setting.namelist.trec"){|fin| ### CONFIG FOR MOTION ###
  File.open("setting.namelist", "w"){|fout|
    dat = fin.read
    dat.gsub!(/\@\@INPUT_NC_VAR\@\@/, "'#{var_name}'") ### SET INPUT NC VAR NAME ###
    dat.gsub!(/\@\@INPUT_INTERVAL\@\@/, dt.to_s) ### SET INPUT TIME INTERVAL ###
    fout.puts dat
  }
}
File.symlink(data[0], "./input_0001.nc")
File.symlink(data[1], "./input_0002.nc")
system(model) || raise
Dir.chdir(pwd)

### SETUP DATA FOR EACH ENSEMBLE MEMBER ###
20.times do |i|
  mem_dir = (i + 1).to_s ### STARTING FROM 1 ###
  FileUtils.mkdir_p(mem_dir) ### DIRECTORY FOR EACH MEMBER ###
  Dir.chdir(mem_dir)
  system("rm -rf ./*") || raise
  File.open("../setting.namelist.fcst"){|fin| ### CONFIG FOR FORECAST ###
    File.open("setting.namelist", "w"){|fout|
      dat = fin.read
      dat.gsub!(/\@\@INPUT_NC_VAR\@\@/, "'#{var_name}'") ### SET INPUT NC VAR NAME ###
      dat.gsub!(/\@\@INPUT_INTERVAL\@\@/, dt.to_s) ### SET INPUT TIME INTERVAL ###
      dat.gsub!(/\@\@OUTPUT_INTERVAL\@\@/, dt.to_s) ### SET OUTPUT TIME INTERVAL ###
      dat.gsub!(/\@\@DT\@\@/, dt_integ.to_s) ### TIME INTERVAL FOR TIME INTEGRATION ###
      dat.gsub!(/\@\@CALNT\@\@/, ((dt / dt_integ).to_i + 1).to_s) ### SET FORECAST LENGTH ###
      fout.puts dat
    }
  }
  File.symlink(data[0], "./input_0001.nc")
  File.symlink(data[1], "./input_0002.nc")
  ### THIS IS JUST AN EXAMPLE: ALL MEMBERS ARE IDENTICAL ###
  %w( u v w ).each{|j|
    FileUtils.copy("#{trec_dir}/vector_#{j}_001.nc", "./", :preserve => true)
  }
  Dir.chdir(pwd)
end

### RUN ALL ENSEMBLE MEMBERS AT ONCE ###
system(model) || raise
puts "done"
