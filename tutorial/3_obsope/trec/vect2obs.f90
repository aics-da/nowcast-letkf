program vect2obs
  use netcdf
  implicit none

  character(1024) :: filenames(3), lonlatnames(2)
  real(4), allocatable :: u(:, :), v(:, :), lon(:), lat(:)
  real(4) :: obserr
  real(4), parameter :: rmiss = 1.0e4

  call get_info()
  call get_data()
  call write_letkf_obs()
  stop
  
contains

  subroutine get_info()
    integer :: n_arg, length, status, i
    character(1024), allocatable :: args(:)

    write(*, *) "usage: vect2obs.exe rain_filename u_filename v_filename lon_axis_name lat_axis_name observation_error_stddev"
    n_arg = command_argument_count()
    if(n_arg < 6) then
       write(*, *) "number of options is less than required"
       stop 999
    end if

    allocate(args(n_arg))
    do i = 1, n_arg
       call get_command_argument(i, args(i), length, status)
    end do !i

    filenames(:) = args(1:3)
    lonlatnames(:) = args(4:5)
    read(args(6), *) obserr
  end subroutine get_info

  subroutine check_nc_err(stat)
    integer, intent(in) :: stat
    if(stat .ne. nf90_noerr) then
       write(*, *) trim(nf90_strerror(stat))
       stop 999
    end if
  end subroutine check_nc_err
  
  subroutine get_data()
    integer ncid, dimid, varid, xid, yid, nx, ny
    write(*, *) "reading lon,lat from ", trim(filenames(1)) 
    call check_nc_err(nf90_open(trim(filenames(1)), NF90_NOWRITE, ncid))
    call check_nc_err(nf90_inq_dimid(ncid, trim(lonlatnames(1)), dimid))
    call check_nc_err(nf90_inquire_dimension(ncid, dimid, len = nx))
    allocate(lon(nx))
    call check_nc_err(nf90_inq_varid(ncid, trim(lonlatnames(1)), varid))
    call check_nc_err(nf90_get_var(ncid, varid, lon(:)))

    call check_nc_err(nf90_inq_dimid(ncid, trim(lonlatnames(2)), dimid))
    call check_nc_err(nf90_inquire_dimension(ncid, dimid, len = ny))
    allocate(lat(ny))
    call check_nc_err(nf90_inq_varid(ncid, trim(lonlatnames(2)), varid))
    call check_nc_err(nf90_get_var(ncid, varid, lat(:)))
    call check_nc_err(nf90_close(ncid))
    write(*, *) nx, ny
    
    allocate(u(nx, ny), v(nx, ny))
    write(*, *) "reading u from ", trim(filenames(2)) 
    call check_nc_err(nf90_open(trim(filenames(2)), NF90_NOWRITE, ncid))
    call check_nc_err(nf90_inq_varid(ncid, "u", varid))
    call check_nc_err(nf90_get_var(ncid, varid, u(:, :)))
    call check_nc_err(nf90_close(ncid))
    
    write(*, *) "reading v from ", trim(filenames(3)) 
    call check_nc_err(nf90_open(trim(filenames(3)), NF90_NOWRITE, ncid))
    call check_nc_err(nf90_inq_varid(ncid, "v", varid))
    call check_nc_err(nf90_get_var(ncid, varid, v(:, :)))
    call check_nc_err(nf90_close(ncid))
  end subroutine get_data

  subroutine write_letkf_obs()
    integer nobs, i, j
    real(4) :: letkf_obs(6)
    integer, parameter :: iunit = 91
    
    nobs = count(u .lt. rmiss) * 2
    write(*, *) nobs, "observations out of ",  size(u) * 2, "will be processed"
    write(*, *) "observation error is set to", obserr
    write(*, *) "writing data to obsin.dat"

    OPEN(iunit, FILE = "obsin.dat", FORM = 'unformatted', ACCESS = 'sequential')
    do j = 1, size(lat)
       do i = 1, size(lon)
          if(u(i, j) .ge. rmiss) cycle
          
          letkf_obs(1) = 2819 !!! CODE NUMBER FOR U WIND !!!
          letkf_obs(2) = lon(i)
          letkf_obs(3) = lat(j)
          letkf_obs(4) = 0.975 !!! DUMMY ELEVATION INFO !!!
          letkf_obs(5) = u(i, j)
          letkf_obs(6) = obserr
          write(iunit) letkf_obs

          letkf_obs(1) = 2820 !!! CODE NUMBER FOR V WIND !!!
          letkf_obs(2) = lon(i)
          letkf_obs(3) = lat(j)
          letkf_obs(4) = 0.975 !!! DUMMY ELEVATION INFO !!!
          letkf_obs(5) = v(i, j)
          letkf_obs(6) = obserr
          write(iunit) letkf_obs
       end do !i
    end do !j
    close(iunit)
  end subroutine write_letkf_obs
end program vect2obs
