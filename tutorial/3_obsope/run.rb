#data = "../data/cappi\\\*.nc" ### ESCAPE "*" TWICE ###
#var_name = "REF"
#time_intv = 480
data = "/home/aarruti/data/cappis/RMA2/20191011/cappi\\\*.nc" ### ESCAPE "*" TWICE ###
var_name = "precipitation"
time_intv = 520

### RUN ENSEMBLE FORECAST FROM t = 0 -> 1 ###
system("cd first_guess ; ruby run_ens.rb #{data} #{var_name} #{time_intv}") || raise

### OBTAIN NEW MOTION VECTORS AT t = 1 AND CONVERT THEM ###
system("cd trec ; ruby run_vect2obs.rb #{data} #{var_name} #{time_intv}") || raise

### RUN OBSOPE AT t = 1 ###
system("cd obsope ; ruby run_obsope.rb #{data} #{time_intv}") || raise


