&input_config
input_basename = "input_" ! input file basename (should be quoted, time step and suffix such as 0001.bin will be added to this) /
input_nc_var = "REF"
input_type = 5 ! 0: Cartesian, 1: PAWR QCed, 2: TOSHIBA PAWR, 3: JRC NetCDF, 4: TOSHIBA MP-PAWR, 5: 2D Cartesian NetCDF /
input_interval = 480.0d0 ! input file interval (seconds) /
output_interval = 300.0d0 ! output file interval (seconds) /
switching_input_missing = 1 ! 0:do not replace missing value, 1:replace missing value with input_missing_newval /
input_missing_newval = 0.0d0 ! substitute missing value with this /
expconv = 0 ! 0:no conversion, 1: convert from dBZ to Z, 2: convert from dBZ to rain rate /
read_topo_mask = 0 ! 0: internally generate topographic info, 1: read topographic info from file /
topo_max = 20 ! max layer number for topo filling /
use_precomputed_vector = 1 ! 0:compute motion vector, 1:read in motion vector from file /
output_vector = 4
output_history = 4 ! 0:no history output, 1:single file, 2:(preserved for MPI-IO), 3:files for each process, 4:NetCDF /
smooth_rain_field = 1
smooth_rain_field_ratio_to_frame_num = 10
ensemble_size = 20
ensemble_share_input = 1
/

&time_config
dt = 10.0d0 !Time step (seconds) /
calnt = 49 !maximum time steps /
start = 1 !initial run number /
iteration_all = 1 !number of runs /
nt = 2 !number of observation frames for each run /
/

&domain_config
ddx = 241 ! input data size (x) /
ddy = 241 ! input data size (y) /
ddz = 1 ! input data size (z) /
dx = 241 ! output data size (x) /
dy = 241 ! output data size (y) /
dz = 1 ! output data size (z) /
gridx = 2000 ! resolution (x) /
gridy = 2000 ! resolution (y) /
gridz = 2000 ! resolution (z) /
posix = 0 ! offset of origin for the output field (x) /
posiy = 0 ! offset of origin for the output field (y) /
posiz = 0 ! offset of origin for the output field (z) /
boundary_type = 0 ! 0:standard, 1:cyclic in x /
/

&search_config
frame_num = 25 ! search box size (xy)
frame_numz = 1 ! search box size (z)
use_variable_frame_num = 0
search_num = 5 ! max search area (xy)
search_numz = 0 ! max search area (z)
use_variable_search_num = 0
switching_vector = 5 ! 0:corration, 1:SSD, 2:fuzzy, 3:fuzzy2, 4:SSD_skip_noise, 5: correlation_skip_noise /
switching_vector_centroid = 1 ! 0:use single point, 1:use centroid, 2:use centroid (fixed variable range) /
vector_centroid_global = 0
vector_centroid_variable_range = 0.1
corr_qc_threshold = 0.6
corr_diff_qc_threshold = 0.8
corr_qc_normalize_diff_by_autocorr = 1
switching_vector_skip_missing = 0
vector_missing_threshold = 0.1d0 ! values smaller than this will be ignored if switching_vector_skip_missing == 1 /
min_valid_grid_ratio = 0.0125 ! minimum valid grid ratio within the matching box /
min_valid_direction_ratio = 0.99 !minimum valid direction ratio within the search area /
fill_missing = 2 ! 0:filling domain-average motions, 1:filling latitude-average motions, 2:filling by diffusion /
fill_missing_threshold = 0.0001 ! threshold value for filling by diffusion /
match_by_sphere = 1
search_within_sphere = 1
x_skip = 2
y_skip = 2
vector_align_skip = 1
vector_set_core_minval = 1
vector_core_minval = 1e-5
vector_core_radius = 20
/

&cotrec_config
swiching_cotrec = 0 ! 0:No cotrec, 1:2D cotrec, 2:3D cotrec /
threshold = 0.001 ! threshold for cotrec /
countermax = 100000 ! max iteration for cotrec /
sor_accel = 1.5 ! for cotrec Gauss-Seidel SOR (fallback) /
/

&driver_config
swiching_burgers = 0 ! advection of motion vectors 0:1st order upwind, 1:5th order WENO /
switching_growth = 0 ! growth rate 0:not consider 1:consider /
switching_growth_weno = 0 ! growth rate advection method 0:1st order upwind 1:5th order WENO /
growth_rate_max_it = 181 ! time step to turn off growth rate /
switching_boundary_motion = 0 ! boundary motion 0:fixed, 1:upwind_mean_downwind_Neumann /
switching_time_integration = 0 ! 0: 2nd-order Adams-Bashforth, 1: 3rd-order Adams-Bashfort /
switching_div_damper = 1
div_damper_param = 100000.0
/

&noise_config
rain_noise = 0.0 ! rain noise level /
fuzzy_sad_min = 0 ! fuzzy SAD min /
fuzzy_sad_max = 30 ! fuzzy SAD max /
switching_lanczos = 0 ! 0:no filter, 1:Lanczos filter /
lanczos_window = 40 ! Lanczos filter window (grids) /
lanczos_critical_wavelength = 40.0d0 ! Lanczos filter critical wavelength (grids) /
/

&da_config
use_data_assimilation = 0
variance_f_init = 10.0 ! initial forecast variance /
variance_f_inflation = 1.1 ! forecast variance inflation parameter /
variance_o = 5.0 ! observation variance /
da_start_from = 1 ! start assimilation from this cycle /
da_qc_threshold = 5.0 ! QC threshold /
da_assimilate_boundary = 0
/
