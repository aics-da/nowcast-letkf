require "fileutils"

### SET PATH TO YOUR MODEL ###
model = "../../../nowcast/src/nowcast.exe"
model = File.expand_path(model)

### SET PATH TO DATA ###
data = Dir.glob("../data/cappi*.nc").sort.map!{|m| File.expand_path(m)}

### COMPUTE MOTION VECTOR ###
trec_dir = "./trec"
trec_dir = File.expand_path(trec_dir)
pwd = Dir.pwd
FileUtils.mkdir_p(trec_dir)
Dir.chdir(trec_dir)
system("rm -rf ./*") || raise
File.symlink("../setting.namelist.trec", "./setting.namelist") ### CONFIG FOR MOTION ###
File.symlink(data[0], "./input_0001.nc")
File.symlink(data[1], "./input_0002.nc")
system(model) || raise
Dir.chdir(pwd)

### SETUP DATA FOR EACH ENSEMBLE MEMBER ###
20.times do |i|
  mem_dir = (i + 1).to_s ### STARTING FROM 1 ###
  FileUtils.mkdir_p(mem_dir) ### DIRECTORY FOR EACH MEMBER ###
  Dir.chdir(mem_dir)
  system("rm -rf ./*") || raise
  File.symlink("../setting.namelist.fcst", "./setting.namelist") ### CONFIG FOR FORECAST ###
  File.symlink(data[0], "./input_0001.nc")
  File.symlink(data[1], "./input_0002.nc")
  ### THIS IS JUST AN EXAMPLE: ALL MEMBERS ARE IDENTICAL ###
  %w( u v w ).each{|j|
    FileUtils.copy("#{trec_dir}/vector_#{j}_001.nc", "./", :preserve => true)
  }
  Dir.chdir(pwd)
end

### RUN ALL ENSEMBLE MEMBERS AT ONCE ###
system(model)
puts "done"
