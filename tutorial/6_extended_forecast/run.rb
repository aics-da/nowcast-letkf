require "fileutils"
require "time"

#data = "../data/cappi*.nc"
#var_name = "REF"
data = "/home/aarruti/data/cappis/RMA2/20191011/cappi*.nc"
data = Dir.glob(data).sort.map!{|m| File.expand_path(m)}
var_name = "precipitation"

def get_time_intv(path1, path2)
  p t1 = Time.parse(File.basename(path1).gsub("cappi.", "").gsub(".nc", ""))
  p t2 = Time.parse(File.basename(path2).gsub("cappi.", "").gsub(".nc", ""))
  p dt = t2 - t1
  dt.round(-1) ### ROUNDED TO MULTIPLES OF TEN ###
end

def get_dir_name(path)
  path = File.basename(path).gsub("cappi.", "").gsub(".nc", "")
  File.expand_path(path)
end

pwd = Dir.pwd

for i in 1..(data.length - 2) ### from the pair of t = (1, 2)
  next unless i % 10 == 0 ### EVERY 10 STEPS

  dat0 = data[i]
  dat1 = data[i + 1]
  p dir = get_dir_name(dat1)
  p time_intv = get_time_intv(dat0, dat1)

  FileUtils.mkdir_p(dir)
  Dir.chdir(dir)

  system("rm -f letkf")  || raise ### REMOVE PREVIOUS SYMLINK ###
  FileUtils.symlink(File.join("..", "..", "5_letkf_cycle_run", File.basename(dir), "letkf"), ".")

  
  ### RUN ENSEMBLE FORECAST FROM t = i + 1 -> i + 2 ###
  system("cp -pr ../template/extended_fcst .") || raise
  system("cd extended_fcst ; ruby run_ens.rb #{dat0} #{dat1} #{var_name} #{time_intv} > log 2>&1") || raise

  puts "#{dir} done"
  Dir.chdir(pwd)
end
