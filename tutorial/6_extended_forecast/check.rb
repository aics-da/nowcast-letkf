require "numru/ggraph"
include NumRu

g = GPhys::IO.open(Dir.glob(data = "/home/aarruti/data/cappis/RMA2/20191011/cappi*.nc")[0], "precipitation")
nx = g.shape[0]
ny = g.shape[1]
grid = g.grid_copy

DCL::swpset("iwidth", DCL::swpget("iwidth") * 2)
DCL::swpset("iheight", DCL::swpget("iheight") * 2)
DCL::swpset("ldump", true)
DCL::gropn(1)
DCL::sldiv("Y", 6, 4)
#DCL::uzfact(1.5)
DCL::glpset("lmiss", true)

umin = -40
umax = 40
incmin = -10
incmax = 10
p rmiss = DCL::glpget("rmiss")
levels   = [rmiss, 0,     0.1,   0.2,   0.3,   0.5,   1,     2,     3,     5,     10,    20,    30,    50,    100, rmiss]
patterns = [0,     15999, 20999, 25999, 30999, 35999, 40999, 45999, 50999, 55999, 60999, 65999, 70999, 75999, 80999]

exps = Dir.glob("20*_*[0-9]").sort

fignames = []

exps.each_with_index{|d, j|
  p d
  unless File.exist?(d + "_7.png")
    DCL::sgscmn(56)
    ### FIRST GUESS ###
    7.times do |it|
      ensmean = 0
      20.times do |i|
        p [it, i]
        dat = GPhys::IO.open(File.expand_path("#{d}/extended_fcst/#{i + 1}/raindata_001_#{format('%03d', it + 1)}.nc"), "rain")
        dat = GPhys.new(grid, dat.data[true, true, 0, 0])
        ensmean += dat
        GGraph.tone(dat, true, "levels" => levels, "patterns" => patterns, "title" => "mem#{i + 1} t=#{it * 5}min")
      end
      #DCLExt.color_bar("tickintv" => 0, "labelintv" => 1)

      DCL::grfrm ### DUMMY ###

      ensmean /= 20
      p ensmean.val
      GGraph.tone(ensmean, true, "levels" => levels, "patterns" => patterns, "title" => "#{d} ensmean t=#{it * 5}min")

      DCL::grfrm ### DUMMY ###
      DCL::grfrm ### DUMMY ###
      
      fignames.push d + "_#{it}.png"
    end
  end
}
DCL::grcls

fignames.length.times do |i|
  system("mv dcl_#{format("%04d", i + 1)}.png #{fignames[i]}")
end
