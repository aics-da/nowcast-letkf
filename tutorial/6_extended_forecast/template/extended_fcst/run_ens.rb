require "fileutils"

### SET PATH TO YOUR MODEL ###
model = "../../../../../nowcast/src/nowcast.exe"
model = File.expand_path(model)
raise "model not found" unless File.exist?(model)

### SET PATH TO DATA ###
data0 = ARGV[0]
data1 = ARGV[1]
raise unless File.exist?(data0) and File.exist?(data1)

### SET VARIABLE NAME ###
var_name = ARGV[2] || "REF"

### SET INPUT TIME INTERVAL ###
dt = (ARGV[3] || 480.0).to_f

dt_integ = 10.0 #[s]

### SET OUTPUT TIME INTERVAL AND LENGTH ###
dt_out = 300.0 #[s]
ft_out = 1800.0 #[s]
calnt = (ft_out / dt_integ).to_i + 1

### SETUP DATA FOR EACH ENSEMBLE MEMBER ###
pwd = Dir.pwd
20.times do |i|
  mem_dir = (i + 1).to_s ### STARTING FROM 1 ###
  FileUtils.mkdir_p(mem_dir) ### DIRECTORY FOR EACH MEMBER ###
  Dir.chdir(mem_dir)
  system("rm -rf ./*") || raise

  File.open("../setting.namelist.fcst"){|fin| ### CONFIG FOR FORECAST ###
    File.open("setting.namelist", "w"){|fout|
      dat = fin.read
      dat.gsub!(/\@\@INPUT_NC_VAR\@\@/, "'#{var_name}'") ### SET INPUT NC VAR NAME ###
      dat.gsub!(/\@\@INPUT_INTERVAL\@\@/, dt.to_s) ### SET INPUT TIME INTERVAL ###
      dat.gsub!(/\@\@OUTPUT_INTERVAL\@\@/, dt_out.to_s) ### SET OUTPUT TIME INTERVAL ###
      dat.gsub!(/\@\@DT\@\@/, dt_integ.to_s) ### TIME INTERVAL FOR TIME INTEGRATION ###
      dat.gsub!(/\@\@CALNT\@\@/, calnt.to_s) ### SET FORECAST LENGTH ###
      fout.puts dat
    }
  }
  File.symlink(data0, "./input_0001.nc")
  File.symlink(data1, "./input_0002.nc")
  %w( u v w ).each{|j|
    FileUtils.symlink("../../letkf/#{mem_dir}/vector_#{j}_001.bin", "./")
  }
  Dir.chdir(pwd)
end

### RUN ALL ENSEMBLE MEMBERS AT ONCE ###
system(model) || raise
puts "done"
