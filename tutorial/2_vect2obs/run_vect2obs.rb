### SET PATH TO YOUR MODEL ###
model = "../../../nowcast/src/nowcast.exe"
model = File.expand_path(model)

### MAKE VECT2OBS ###
system("make -f Makefile.gfortran")

### SET PATH TO DATA ###
data = Dir.glob("../data/cappi*.nc").sort.map!{|m| File.expand_path(m)}

### COMPUTE MOTION VECTOR ###
system("rm -f *.nc")
File.symlink(data[0], "./input_0001.nc")
File.symlink(data[1], "./input_0002.nc")
system(model) || raise

### RUN VECT2OBS ###
system("./vect2obs.exe #{data[0]} vector_u_001.nc vector_v_001.nc lon lat 5")

### RUN TEST ###
system("./test.exe")

