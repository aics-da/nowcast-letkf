program vect2obs
  use netcdf
  implicit none

  character(1024) :: filenames(4), lonlatnames(2)
  real(4), allocatable :: u(:, :), v(:, :), w(:, :), lon(:), lat(:)
  real(4), parameter :: rmiss = 1.0e4
  real(4) :: dx, dt, factor
  
  call get_info()
  call get_data()
  call write_letkf_gues()
  stop
  
contains

  subroutine get_info()
    integer :: n_arg, length, status, i
    character(1024), allocatable :: args(:)

    write(*, *) "usage: conv.exe rain_filename u_filename v_filename w_filename ", &
         & "lon_axis_name lat_axis_name dx_dy input_time_interval"
    n_arg = command_argument_count()
    if(n_arg < 8) then
       write(*, *) "number of options is less than required"
       stop 999
    end if

    allocate(args(n_arg))
    do i = 1, n_arg
       call get_command_argument(i, args(i), length, status)
    end do !i

    filenames(:) = args(1:4)
    lonlatnames(:) = args(5:6)
    read(args(7), *) dx !!! ASSUMING SAME GRID SPACING IN X, Y, Z !!!
    read(args(8), *) dt !!! INPUT TIME INTERVAL FOR THIS ANALYSIS STEP !!!
    factor = dt / dx
    write(*, *) "dx = ", dx
    write(*, *) "dt = ", dt
    write(*, *) "factor from physical space to grid number space = ", factor
  end subroutine get_info

  subroutine check_nc_err(stat)
    integer, intent(in) :: stat
    if(stat .ne. nf90_noerr) then
       write(*, *) trim(nf90_strerror(stat))
       stop 999
    end if
  end subroutine check_nc_err
  
  subroutine get_data()
    integer ncid, dimid, varid, xid, yid, nx, ny
    write(*, *) "reading lon,lat from ", trim(filenames(1)) 
    call check_nc_err(nf90_open(trim(filenames(1)), NF90_NOWRITE, ncid))
    call check_nc_err(nf90_inq_dimid(ncid, trim(lonlatnames(1)), dimid))
    call check_nc_err(nf90_inquire_dimension(ncid, dimid, len = nx))
    allocate(lon(nx))
    call check_nc_err(nf90_inq_varid(ncid, trim(lonlatnames(1)), varid))
    call check_nc_err(nf90_get_var(ncid, varid, lon(:)))

    call check_nc_err(nf90_inq_dimid(ncid, trim(lonlatnames(2)), dimid))
    call check_nc_err(nf90_inquire_dimension(ncid, dimid, len = ny))
    allocate(lat(ny))
    call check_nc_err(nf90_inq_varid(ncid, trim(lonlatnames(2)), varid))
    call check_nc_err(nf90_get_var(ncid, varid, lat(:)))
    call check_nc_err(nf90_close(ncid))
    write(*, *) nx, ny
    
    allocate(u(nx, ny), v(nx, ny), w(nx, ny))
    write(*, *) "reading u from ", trim(filenames(2)) 
    call check_nc_err(nf90_open(trim(filenames(2)), NF90_NOWRITE, ncid))
    call check_nc_err(nf90_inq_varid(ncid, "u", varid))
    call check_nc_err(nf90_get_var(ncid, varid, u(:, :)))
    call check_nc_err(nf90_close(ncid))
    
    write(*, *) "reading v from ", trim(filenames(3)) 
    call check_nc_err(nf90_open(trim(filenames(3)), NF90_NOWRITE, ncid))
    call check_nc_err(nf90_inq_varid(ncid, "v", varid))
    call check_nc_err(nf90_get_var(ncid, varid, v(:, :)))
    call check_nc_err(nf90_close(ncid))

    write(*, *) "reading w from ", trim(filenames(4)) !!! NOT USED, BUT REQUIRED !!!
    call check_nc_err(nf90_open(trim(filenames(4)), NF90_NOWRITE, ncid))
    call check_nc_err(nf90_inq_varid(ncid, "w", varid))
    call check_nc_err(nf90_get_var(ncid, varid, w(:, :)))
    call check_nc_err(nf90_close(ncid))
  end subroutine get_data

  subroutine write_letkf_gues()
    integer i
    integer, parameter :: iunit = 91
    
    write(*, *) "writing data to gues.grd"
    OPEN(iunit, FILE = "gues.grd", FORM = 'unformatted', ACCESS = 'stream')
    write(iunit) u * factor, v * factor, w * factor !!! CONVERT FROM PHYSICAL SPACE (m/s) TO GRID NUMBER SPACE (grid/intv)
    close(iunit)

    write(*, *) "writing data to lon.dat"
    OPEN(iunit, FILE = "lon.dat", FORM = 'formatted')
    write(iunit, *) lon
    close(iunit)
    
    write(*, *) "writing data to lat.dat"
    OPEN(iunit, FILE = "lat.dat", FORM = 'formatted')
    write(iunit, *) lat
    close(iunit)

    write(*, *) "writing data to lev.dat"
    OPEN(iunit, FILE = "lev.dat", FORM = 'formatted')
    write(iunit, *) 0.975 !!! DUMMY ELEVATION INFO !!!
    close(iunit)
  end subroutine write_letkf_gues
end program vect2obs
