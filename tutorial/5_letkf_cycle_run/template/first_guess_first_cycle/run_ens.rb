require "fileutils"

### SET PATH TO YOUR MODEL ###
model = "../../../../../nowcast/src/nowcast.exe"
model = File.expand_path(model)
raise "model not found" unless File.exist?(model)

### SET PATH TO DATA ###
data0 = ARGV[0]
data1 = ARGV[1]
raise unless File.exist?(data0) and File.exist?(data1)

### SET VARIABLE NAME ###
var_name = ARGV[2] || "REF"

### SET INPUT TIME INTERVAL ###
dt = (ARGV[3] || 480.0).to_f

dt_integ = 10.0 #[s]

### SET LAGGED ENSEMBLE INTERVAL ###
dt_laf = dt_integ * 1 ### MUST BE MULTIPLES OF dt_integ ###

pwd = Dir.pwd

base_frame_num = 40

### SETUP DATA FOR EACH ENSEMBLE MEMBER ###
20.times do |i|
  mem_dir = (i + 1).to_s ### STARTING FROM 1 ###
  FileUtils.mkdir_p(mem_dir) ### DIRECTORY FOR EACH MEMBER ###
  Dir.chdir(mem_dir)
  system("rm -rf ./*") || raise
  dt_fcst = dt - dt_laf * i ### LAGGED ENSEMBLE ###
  File.open("../setting.namelist.fcst"){|fin| ### CONFIG FOR FORECAST ###
    File.open("setting.namelist", "w"){|fout|
      dat = fin.read
      dat.gsub!(/\@\@INPUT_NC_VAR\@\@/, "'#{var_name}'") ### SET INPUT NC VAR NAME ###
      dat.gsub!(/\@\@INPUT_INTERVAL\@\@/, dt.to_s) ### SET INPUT TIME INTERVAL ###
      dat.gsub!(/\@\@OUTPUT_INTERVAL\@\@/, dt_fcst.to_s) ### SET OUTPUT TIME INTERVAL ###
      dat.gsub!(/\@\@DT\@\@/, dt_integ.to_s) ### TIME INTERVAL FOR TIME INTEGRATION ###
      dat.gsub!(/\@\@CALNT\@\@/, ((dt_fcst / dt_integ).to_i + 1).to_s) ### SET FORECAST LENGTH (LAGGED ENSEMBLE) ###
      dat.gsub!(/\@\@FRAME_NUM\@\@/, (base_frame_num + i).to_s) ### SET FRAME NUM (PARAMETER ENSEMBLE) ###
      fout.puts dat
    }
  }
  File.symlink(data0, "./input_0001.nc")
  File.symlink(data1, "./input_0002.nc")
  ### RUN ENSEMBLE MEMBERS ONE BY ONE ###
  system(model) || raise
  Dir.chdir(pwd)
end

puts "done"
