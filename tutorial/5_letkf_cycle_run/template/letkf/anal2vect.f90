program vect2obs
  use netcdf
  implicit none

  character(1024) :: filenames(4)
  real(4), allocatable :: dat(:, :)

  call get_info()
  call get_data()
  stop
  
contains

  subroutine get_info()
    integer :: n_arg, length, status, i
    character(1024), allocatable :: args(:)

    write(*, *) "usage: anal2vect.exe letkf_output_filename u_filename v_filename w_filename"
    n_arg = command_argument_count()
    if(n_arg < 4) then
       write(*, *) "number of options is less than required"
       stop 999
    end if

    allocate(args(n_arg))
    do i = 1, n_arg
       call get_command_argument(i, args(i), length, status)
    end do !i

    filenames(:) = args(1:4)
  end subroutine get_info

  subroutine check_nc_err(stat)
    integer, intent(in) :: stat
    if(stat .ne. nf90_noerr) then
       write(*, *) trim(nf90_strerror(stat))
       stop 999
    end if
  end subroutine check_nc_err
  
  subroutine get_data()
    integer ncid, dimid, varid, nx, ny
    integer, parameter :: iunit = 99
    write(*, *) "reading dimension from ", trim(filenames(1)) 
    call check_nc_err(nf90_open(trim(filenames(1)), NF90_NOWRITE, ncid))
    call check_nc_err(nf90_inq_dimid(ncid, "lon", dimid))
    call check_nc_err(nf90_inquire_dimension(ncid, dimid, len = nx))
    call check_nc_err(nf90_inq_dimid(ncid, "lat", dimid))
    call check_nc_err(nf90_inquire_dimension(ncid, dimid, len = ny))
    write(*, *) nx, ny
    allocate(dat(nx, ny))

    write(*, *) "reading u from ", trim(filenames(1)) 
    call check_nc_err(nf90_inq_varid(ncid, "u", varid))
    call check_nc_err(nf90_get_var(ncid, varid, dat(:, :)))
    write(*, *) "writing u to ", trim(filenames(2))
    open(iunit, file = trim(filenames(2)), form = "unformatted", access = "stream")
    write(iunit) dat
    close(iunit)
    write(*, *) "reading v from ", trim(filenames(1)) 
    call check_nc_err(nf90_inq_varid(ncid, "v", varid))
    call check_nc_err(nf90_get_var(ncid, varid, dat(:, :)))
    write(*, *) "writing v to ", trim(filenames(3))
    open(iunit, file = trim(filenames(3)), form = "unformatted", access = "stream")
    write(iunit) dat
    close(iunit)
    write(*, *) "reading w from ", trim(filenames(1)) 
    call check_nc_err(nf90_inq_varid(ncid, "w", varid))
    call check_nc_err(nf90_get_var(ncid, varid, dat(:, :)))
    write(*, *) "writing w to ", trim(filenames(4))
    open(iunit, file = trim(filenames(4)), form = "unformatted", access = "stream")
    write(iunit) dat
    close(iunit)
    call check_nc_err(nf90_close(ncid))
  end subroutine get_data
end program vect2obs
