target = ["*.grd",  "*.dat", "*.nc", "*.bin", "NOUT*", "*~"]

20.times do |i|
  target.push("#{i + 1}")
end

system("make -f Makefile.gfortran clean") || raise
system("rm -rfv #{target.join(' ')}") || raise
