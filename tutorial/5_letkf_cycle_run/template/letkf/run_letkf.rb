require "fileutils"

### SET PATH TO LETKF ###
letkf = "../../../../nowcast/letkf/letkf/letkf.exe"
letkf = File.expand_path(letkf)
raise "letkf not found" unless File.exist?(letkf)

### MAKE ANAL2VECT ###
system("make -f Makefile.gfortran") || raise

system("ln -s ../obsope/{*.grd,*.dat} .")

### RUN ###
system(letkf) || raise

system("rm gs*grd obs*dat")

### POST-PROCESSING ###
pwd = Dir.pwd
20.times do |i|
  mem_dir = (i + 1).to_s ### STARTING FROM 1 ###
  FileUtils.mkdir_p(mem_dir) ### DIRECTORY FOR EACH MEMBER ###
  Dir.chdir(mem_dir)
  memid = format('%03d', i + 1)
  system("mv ../anal#{memid}.nc .") || raise
  system("../anal2vect.exe anal#{memid}.nc vector_u_001.bin vector_v_001.bin vector_w_001.bin") || raise
  system("rm -f anal#{memid}.nc") || raise
  Dir.chdir(pwd)
end

system("./anal2vect.exe anal_me.nc vector_u_001.bin vector_v_001.bin vector_w_001.bin") || raise


