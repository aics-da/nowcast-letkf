program vect2obs
  use netcdf
  implicit none

  character(1024) :: filenames(3), lonlatnames(2)
  real(4), allocatable :: u(:, :), v(:, :), lon(:), lat(:)
  real(4) :: obserr
  integer :: flag_fill_missing = 0
  real(4) :: mean_u, mean_v, std_u, std_v, obserr_mean_uv
  real(4), parameter :: rmiss = 1.0e4
  real(4), parameter :: min_obs_ratio = 0.01
  integer, parameter :: iter_qc = 5
  real(4), parameter :: qc_std = 2.0
  real(4), parameter :: adap_obs_err_infl_factor = 2.0
  integer i

  call get_info()
  call get_data()
  do i = 1, iter_qc !!! ITERATE !!!
     call get_mean_vect()
     call qc_data()
  end do
  call get_mean_vect() !!! RECALCULATE AFTER QC !!!
  call adap_obs_err_infl()
  call write_letkf_obs()
  stop
  
contains

  subroutine get_info()
    integer :: n_arg, length, status, i
    character(1024), allocatable :: args(:)

    write(*, *) "usage: vect2obs.exe rain_filename u_filename v_filename lon_axis_name lat_axis_name observation_error_stddev" &
         & // "[flag_fill_missing observation_error_stddev_mean_uv]"
    n_arg = command_argument_count()
    if(n_arg < 6) then
       write(*, *) "number of options is less than required"
       stop 999
    end if

    allocate(args(n_arg))
    do i = 1, n_arg
       call get_command_argument(i, args(i), length, status)
    end do !i

    filenames(:) = args(1:3)
    lonlatnames(:) = args(4:5)
    read(args(6), *) obserr
    if(n_arg .ge. 7) then
       if(n_arg < 8) then
          write(*, *) "please specify observation_error_stddev_mean_uv"
          stop 999
       end if
       read(args(7), *) flag_fill_missing
       read(args(8), *) obserr_mean_uv
    end if
  end subroutine get_info

  subroutine check_nc_err(stat)
    integer, intent(in) :: stat
    if(stat .ne. nf90_noerr) then
       write(*, *) trim(nf90_strerror(stat))
       stop 999
    end if
  end subroutine check_nc_err
  
  subroutine get_data()
    integer ncid, dimid, varid, xid, yid, nx, ny
    write(*, *) "reading lon,lat from ", trim(filenames(1)) 
    call check_nc_err(nf90_open(trim(filenames(1)), NF90_NOWRITE, ncid))
    call check_nc_err(nf90_inq_dimid(ncid, trim(lonlatnames(1)), dimid))
    call check_nc_err(nf90_inquire_dimension(ncid, dimid, len = nx))
    allocate(lon(nx))
    call check_nc_err(nf90_inq_varid(ncid, trim(lonlatnames(1)), varid))
    call check_nc_err(nf90_get_var(ncid, varid, lon(:)))

    call check_nc_err(nf90_inq_dimid(ncid, trim(lonlatnames(2)), dimid))
    call check_nc_err(nf90_inquire_dimension(ncid, dimid, len = ny))
    allocate(lat(ny))
    call check_nc_err(nf90_inq_varid(ncid, trim(lonlatnames(2)), varid))
    call check_nc_err(nf90_get_var(ncid, varid, lat(:)))
    call check_nc_err(nf90_close(ncid))
    write(*, *) nx, ny
    
    allocate(u(nx, ny), v(nx, ny))
    write(*, *) "reading u from ", trim(filenames(2)) 
    call check_nc_err(nf90_open(trim(filenames(2)), NF90_NOWRITE, ncid))
    call check_nc_err(nf90_inq_varid(ncid, "u", varid))
    call check_nc_err(nf90_get_var(ncid, varid, u(:, :)))
    call check_nc_err(nf90_close(ncid))
    
    write(*, *) "reading v from ", trim(filenames(3)) 
    call check_nc_err(nf90_open(trim(filenames(3)), NF90_NOWRITE, ncid))
    call check_nc_err(nf90_inq_varid(ncid, "v", varid))
    call check_nc_err(nf90_get_var(ncid, varid, v(:, :)))
    call check_nc_err(nf90_close(ncid))
  end subroutine get_data

  subroutine write_letkf_obs()
    integer nobs, i, j
    real(4) :: letkf_obs(6)
    integer, parameter :: iunit = 91
    
    nobs = count(u .lt. rmiss) * 2
    write(*, *) nobs, "observations out of ",  size(u) * 2, "will be processed"
    write(*, *) "observation error is set to", obserr
    write(*, *) "writing data to obsin.dat"

    OPEN(iunit, FILE = "obsin.dat", FORM = 'unformatted', ACCESS = 'sequential')
    do j = 1, size(lat)
       do i = 1, size(lon)
          if(u(i, j) .ge. rmiss) cycle
          
          letkf_obs(1) = 2819 !!! CODE NUMBER FOR U WIND !!!
          letkf_obs(2) = lon(i)
          letkf_obs(3) = lat(j)
          letkf_obs(4) = 0.975 !!! DUMMY ELEVATION INFO !!!
          letkf_obs(5) = u(i, j)
          letkf_obs(6) = obserr
          write(iunit) letkf_obs

          letkf_obs(1) = 2820 !!! CODE NUMBER FOR V WIND !!!
          letkf_obs(2) = lon(i)
          letkf_obs(3) = lat(j)
          letkf_obs(4) = 0.975 !!! DUMMY ELEVATION INFO !!!
          letkf_obs(5) = v(i, j)
          letkf_obs(6) = obserr
          write(iunit) letkf_obs
       end do !i
    end do !j
    if(flag_fill_missing .ne. 0 .and. (nobs / 2) > min_obs_ratio * size(u)) then
       write(*, *) "missing values are filled with the mean vector"
       do j = 1, size(lat)
          do i = 1, size(lon)
             if(u(i, j) .lt. rmiss) cycle
          
             letkf_obs(1) = 2819 !!! CODE NUMBER FOR U WIND !!!
             letkf_obs(2) = lon(i)
             letkf_obs(3) = lat(j)
             letkf_obs(4) = 0.975 !!! DUMMY ELEVATION INFO !!!
             letkf_obs(5) = mean_u
             letkf_obs(6) = obserr_mean_uv
             write(iunit) letkf_obs

             letkf_obs(1) = 2820 !!! CODE NUMBER FOR V WIND !!!
             letkf_obs(2) = lon(i)
             letkf_obs(3) = lat(j)
             letkf_obs(4) = 0.975 !!! DUMMY ELEVATION INFO !!!
             letkf_obs(5) = mean_v
             letkf_obs(6) = obserr_mean_uv
             write(iunit) letkf_obs
          end do !i
       end do !j
    end if
    close(iunit)
  end subroutine write_letkf_obs

  subroutine get_mean_vect()
    integer i, j, count
    real(4) tmpu, tmpv

    tmpu = 0
    tmpv = 0
    count = 0
    do j = 1, size(lat)
       do i = 1, size(lon)
          if(u(i, j) .ge. rmiss) cycle
          tmpu = tmpu + u(i, j)
          tmpv = tmpv + v(i, j)
          count = count + 1
       end do !i
    end do !j
    mean_u = tmpu / count
    mean_v = tmpv / count

    tmpu = 0
    tmpv = 0
    count = 0
    do j = 1, size(lat)
       do i = 1, size(lon)
          if(u(i, j) .ge. rmiss) cycle
          tmpu = tmpu + (u(i, j) - mean_u) ** 2
          tmpv = tmpv + (v(i, j) - mean_v) ** 2
          count = count + 1
       end do !i
    end do !j
    std_u = sqrt(tmpu / count)
    std_v = sqrt(tmpv / count)
  end subroutine get_mean_vect

  subroutine qc_data()
    integer i, j, count
    count = 0
    do j = 1, size(lat)
       do i = 1, size(lon)
          if(u(i, j) .ge. rmiss) cycle
          if(abs(u(i, j) - mean_u) / std_u > qc_std .or. abs(v(i, j) - mean_v) / std_v > qc_std) then
             u(i, j) = rmiss
             v(i, j) = rmiss
             count = count + 1
          end if
       end do !i
    end do !j
    write(*, *) "reject:", count
  end subroutine qc_data

  subroutine adap_obs_err_infl()
    integer i
    real(4) tmperr
    do i = 1, 2
       if(i == 1) then
          tmperr = std_u * adap_obs_err_infl_factor
       else
          tmperr = std_v * adap_obs_err_infl_factor
       end if
       if(tmperr > obserr) then
          obserr_mean_uv = tmperr * (obserr_mean_uv / obserr)
          obserr = tmperr
       end if
    end do
    write(*, *) "std_u, std_v, actual obserr, obserr_mean_uv:", std_u, std_v, obserr, obserr_mean_uv
  end subroutine adap_obs_err_infl
end program vect2obs
