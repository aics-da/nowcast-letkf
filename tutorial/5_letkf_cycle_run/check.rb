require "numru/ggraph"
include NumRu

g = GPhys::IO.open(Dir.glob(data = "/home/aarruti/data/cappis/RMA2/20191011/cappi*.nc")[0], "precipitation")
nx = g.shape[0]
ny = g.shape[1]
grid = g.grid_copy

DCL::swpset("iwidth", DCL::swpget("iwidth") * 2)
DCL::swpset("iheight", DCL::swpget("iheight") * 2)
DCL::swpset("ldump", true)
DCL::gropn(1)
DCL::sldiv("Y", 6, 4)
#DCL::uzfact(1.5)
DCL::glpset("lmiss", true)

umin = -40
umax = 40
incmin = -10
incmax = 10
rmiss = DCL::glpget("rmiss")
levels   = [rmiss, 0,     0.1,   0.2,   0.3,   0.5,   1,     2,     3,     5,     10,    20,    30,    50,    100, rmiss]
patterns = [0,     15999, 20999, 25999, 30999, 35999, 40999, 45999, 50999, 55999, 60999, 65999, 70999, 75999, 80999]

exps = Dir.glob("20*_*[0-9]").sort
date = exps.map{|m| Time.parse(m)}
dt = []
(date.length - 1).times do |i|
  dt.push date[i + 1] - date[i] #[s]
end
dx = 2000.0 #[m]

fignames = []

d_old = exps[0]
#Dir.glob("20*_*").sort[1..1].each{|d|
exps[1..-1].each_with_index{|d, j|
  p d
  unless File.exist?(d + "_3.png")
    DCL::sgscmn(14)
    ### FIRST GUESS ###
    phys2grid = dt[j] / dx
    grid2phys = dx / dt[j]
    gues_ens = []
    20.times do |i|
      dat = GPhys::IO.open(File.expand_path("#{d_old}/first_guess/#{i + 1}/burgersU_001_002.nc"), "u")
      dat = GPhys.new(grid, dat.data[true, true, 0, 0])
      gues_ens.push dat
      GGraph.tone(dat, true, "min" => umin, "max" => umax, "nlev" => 80, "title" => "u guess #{i + 1}")
      DCL::uxsttl("T", "#{dat.mean}", 0)
    end
    DCLExt.color_bar("tickintv" => 0)
  
    f = File.expand_path("#{d}/trec/vector_u_001.nc")
    g = GPhys::IO.open("/home/aarruti/data/cappis/RMA2/20191011/cappi.#{d_old}.nc", "precipitation")
    h = GPhys::IO.open("/home/aarruti/data/cappis/RMA2/20191011/cappi.#{d}.nc", "precipitation")
    if File.exist?(f)
      dat = GPhys::IO.open(f, "u")
      dat = GPhys.new(grid, dat.data[true, true, 0, 0])
      mask = dat.ge(123456)
      dat.replace_val(NArrayMiss.to_nam_no_dup(dat.val, 1 - mask))
      mask = dat.copy.replace_val(mask.to_f)
      dat *= grid2phys
      GGraph.tone(dat, true, "min" => umin, "max" => umax, "nlev" => 80, "auto" => false, "tonc" => true, "title" => "TREC #{d}")
      DCLExt.color_bar("tickintv" => 0)
      GGraph.tone(mask, false, "levels" => [0.5, 1.5], "patterns" => [611], "auto" => false, "tonb" => true)
      DCL::uxsttl("T", "#{dat.mean}", 0)

      dat -= dat.mean
      std = dat.stddev
      dat /= std
      GGraph.tone(dat, true,\
                  "levels"   => [rmiss, -5,    -4,    -3,    -2,    -1,    0,     1,     2,     3,     4,     5, rmiss],\
                  "patterns" => [10999, 20999, 30999, 40999, 45999, 50999, 60999, 65999, 70999, 80999, 90999, 99999],\
                  "nlev" => 80, "auto" => false, "tonc" => true, "title" => "standardized")
      DCLExt.color_bar("tickintv" => 0)
      GGraph.tone(mask, false, "levels" => [0.5, 1.5], "patterns" => [611], "auto" => false, "tonb" => true)
      DCL::uxsttl("T", "#{std}", 0)
    else
      DCL::grfrm ### DUMMY ###
      DCL::grfrm ### DUMMY ###
    end

    DCL::sgscmn(54)
    GGraph.tone(g, true, "levels" => levels, "patterns" => patterns, "title" => "rain(-#{dt[j]}s)")
    GGraph.tone(h, true, "levels" => levels, "patterns" => patterns, "title" => "rain(now)")
    DCLExt.color_bar("tickintv" => 0, "constwidth" => true)

    
    fignames.push d + "_1.png"

    DCL::sgscmn(14)
    ### ANALYSIS ###
    anal_ens = []
    20.times do |i|
      f = File.expand_path("#{d}/letkf/#{i + 1}/vector_u_001.bin")
      if File.exist?(f)
        dat = NArray.to_na(File.read(f), NArray::SFLOAT, nx, ny)
        dat *= grid2phys
        dat = VArray.new(dat, {}, "u_#{i + 1}")
        dat = GPhys.new(grid, dat)
        anal_ens.push dat
        GGraph.tone(dat, true, "min" => umin, "max" => umax, "nlev" => 80)
        DCL::uxsttl("T", "#{dat.mean}", 0)
      else
        DCL::grfrm ### DUMMY ###
      end
    end
    f = File.expand_path("#{d}/letkf/vector_u_001.bin")
    if File.exist?(f)
      dat = NArray.to_na(File.read(f), NArray::SFLOAT, nx, ny)
      dat *= grid2phys
      dat = VArray.new(dat, {}, "u_mean #{d}")
      dat = GPhys.new(grid, dat)
      GGraph.tone(dat, true, "min" => umin, "max" => umax, "nlev" => 80)
      DCLExt.color_bar("tickintv" => 0)
      DCL::uxsttl("T", "#{dat.mean}", 0)
    else
      DCL::grfrm ### DUMMY ###
    end

    GGraph.contour(g, true, "levels" => [0.2],  "index" => [31], "title" => "rain(-#{dt[j]}s, now)")
    GGraph.contour(h, false, "levels" => [0.2], "index" => [21])


    DCL::sgscmn(54)
    f = File.expand_path("#{d}/letkf/anal_sp.nc")
    if File.exist?(f)
      sp = GPhys::IO.open(f, "u")
      sp = GPhys.new(grid, sp.data[true, true, 0])
      sp *= grid2phys
      GGraph.tone(sp, true, "min" => 1e-2, "max" => umax, "nlev" => 80, "title" => "spread", "log" => true)
      DCLExt.color_bar("tickintv" => 0, "constwidth" => true, "labelintv" => 1)
    else
      DCL::grfrm ### DUMMY ###
    end

    DCL::grfrm ### DUMMY ###
    fignames.push d + "_2.png"
    ###

    DCL::sgscmn(14)
    ### INCREMENT ###
    mean = 0
    20.times do |i|
      if gues_ens[i] and anal_ens[i]
        dat = anal_ens[i] - gues_ens[i]
        GGraph.tone(dat, true, "min" => incmin, "max" => incmax, "nlev" => 80, "title" => "mem#{i + 1} increment")
        mean += dat
      else
        DCL::grfrm ### DUMMY ###
      end
    end
    mean /= 20

    GGraph.tone(mean, true, "min" => incmin, "max" => incmax, "nlev" => 80, "title" => "mean increment")
    DCLExt.color_bar("tickintv" => 0)

    DCL::grfrm ### DUMMY ###
    DCL::grfrm ### DUMMY ###
    DCL::grfrm ### DUMMY ###
    fignames.push d + "_3.png"
  end

  d_old = d
}
DCL::grcls

fignames.length.times do |i|
  system("mv dcl_#{format("%04d", i + 1)}.png #{fignames[i]}")
end
