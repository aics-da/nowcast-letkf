require "fileutils"
require "time"

#data = "../data/cappi*.nc"
#var_name = "REF"
data = "/home/aarruti/data/cappis/RMA2/20191011/cappi*.nc"
data = Dir.glob(data).sort.map!{|m| File.expand_path(m)}
var_name = "precipitation"

### MAKE ###
system("cd template/trec ; make -f Makefile.gfortran") || raise
system("cd template/obsope ; make -f Makefile.gfortran") || raise
system("cd template/letkf ; make -f Makefile.gfortran") || raise

def get_time_intv(path1, path2)
  p t1 = Time.parse(File.basename(path1).gsub("cappi.", "").gsub(".nc", ""))
  p t2 = Time.parse(File.basename(path2).gsub("cappi.", "").gsub(".nc", ""))
  p dt = t2 - t1
  dt.round(-1) ### ROUNDED TO MULTIPLES OF TEN ###
end

def get_dir_name(path)
  path = File.basename(path).gsub("cappi.", "").gsub(".nc", "")
  File.expand_path(path)
end

MAX_INPUT_DT = 600.0 # [seconds]

pwd = Dir.pwd

dat0 = data[0] ### t = 0
dat1 = data[1] ### t = 1
p time_intv = get_time_intv(dat0, dat1)
p dir = get_dir_name(dat1)
FileUtils.mkdir_p(dir)
Dir.chdir(dir)
system("cp -pr ../template/first_guess_first_cycle ./first_guess") || raise
### RUN ENSEMBLE FORECAST FROM t = 1 -> 2 ###
system("cd first_guess ; ruby run_ens.rb #{dat0} #{dat1} #{var_name} #{time_intv} > log 2>&1") || raise
puts "#{dir} done"
Dir.chdir(pwd)

for i in 1..(data.length - 2) ### from the pair of t = (1, 2)
  dat0 = data[i]
  dat1 = data[i + 1]

  dir_prev = dir
  p dir = get_dir_name(dat1)
  FileUtils.mkdir_p(dir)
  Dir.chdir(dir)

  FileUtils.symlink(File.join(dir_prev, "first_guess"), ".")

  p time_intv = get_time_intv(dat0, dat1)
  run_trec = (time_intv < MAX_INPUT_DT)
  if run_trec
    ### OBTAIN NEW MOTION VECTORS AT t = i + 1 AND CONVERT THEM ###
    system("cp -pr ../template/trec .") || raise
    system("cd trec ; ruby run_vect2obs.rb #{dat0} #{dat1} #{var_name} #{time_intv} > log 2>&1") || raise
  else
    puts "TREC skipped: time_intv #{time_intv} < MAX_INPUT_DT #{MAX_INPUT_DT}"
  end

  ### RUN OBSOPE AT t = i + 1 ###
  system("cp -pr ../template/obsope .") || raise
  system("cd obsope ; ruby run_obsope.rb #{dat0} #{time_intv} #{run_trec ? '' : 1} > log 2>&1") || raise

  ### RUN LETKF AT t = i + 1 ###
  system("cp -pr ../template/letkf .") || raise
  system("cd letkf ; ruby run_letkf.rb > log 2>&1") || raise

  ### RUN ENSEMBLE FORECAST FROM t = i + 1 -> i + 2 ###
  system("rm -f first_guess")  || raise ### REMOVE PREVIOUS SYMLINK ###
  system("cp -pr ../template/first_guess .") || raise
  system("cd first_guess ; ruby run_ens.rb #{dat0} #{dat1} #{var_name} #{time_intv} > log 2>&1") || raise

  puts "#{dir} done"
  Dir.chdir(pwd)
end
