MODULE common_obs_nowcast
!=======================================================================
!
! [PURPOSE:] Observational procedures
!
! [HISTORY:]
!   01/23/2009 Takemasa MIYOSHI  created
!
!=======================================================================
!$USE OMP_LIB
  USE common
  USE common_nowcast

  IMPLICIT NONE
  PUBLIC

  INTEGER,PARAMETER :: nid_obs=3
  INTEGER,PARAMETER :: id_u_obs=2819
  INTEGER,PARAMETER :: id_v_obs=2820
  INTEGER,PARAMETER :: id_w_obs=2821 ! ???
  !INTEGER,PARAMETER :: id_t_obs=3073
  !INTEGER,PARAMETER :: id_q_obs=3330
  !INTEGER,PARAMETER :: id_rh_obs=3331
  !INTEGER,PARAMETER :: id_ps_obs=14593
  !INTEGER,PARAMETER :: id_rain_obs=9999

  REAL(r_size), allocatable :: rh(:, :, :)
  REAL(r_size), allocatable :: lnps(:, :)
  REAL(r_size), allocatable :: plev(:)
  private rh, lnps, plev

CONTAINS

subroutine set_common_obs_nowcast
  allocate(rh(nlon, nlat, nlev))
  allocate(lnps(nlon, nlat))
  allocate(plev(nlev))
end subroutine set_common_obs_nowcast

subroutine unset_common_obs_nowcast
  deallocate(rh, lnps, plev)
end subroutine unset_common_obs_nowcast

!-----------------------------------------------------------------------
! Customized ceiling
!-----------------------------------------------------------------------
function ceiling_safe(val, nmax)
  integer :: ceiling_safe
  real(r_size), intent(in) :: val
  integer, intent(in) :: nmax
  integer cc
  cc = ceiling(val)
  if(cc == floor(val)) then
     if(cc == nmax) then
        ceiling_safe = nmax
     else
        ceiling_safe = cc + 1
     end if
  else
     ceiling_safe = cc
  end if
end function ceiling_safe
!-----------------------------------------------------------------------
! Transformation from model variables to an observation
!-----------------------------------------------------------------------
!SUBROUTINE Trans_XtoY(elm,ri,rj,rk,v3d,v2d,p_full,yobs)
!DEC$ ATTRIBUTES FORCEINLINE :: Trans_XtoY
SUBROUTINE Trans_XtoY(elm, ri, rj, rk, v3d, v2d, yobs)
  IMPLICIT NONE
  REAL(r_size),INTENT(IN) :: elm
  REAL(r_size),INTENT(IN) :: ri, rj, rk
  REAL(r_size),INTENT(IN) :: v3d(nlon, nlat, nlev, nv3d)
  REAL(r_size),INTENT(IN) :: v2d(nlon, nlat, nv2d)
  !REAL(r_size),INTENT(IN) :: p_full(nlon,nlat,nlev)
  REAL(r_size),INTENT(OUT) :: yobs
  !REAL(r_size) :: rh(nlon,nlat,nlev)
  INTEGER :: i, j, k
  INTEGER :: is, ie, js, je, ks, ke
  ie = ceiling_safe(ri, nlon + 1)
  is = ie - 1
  je = ceiling_safe(rj, nlat)
  js = je - 1
  ke = ceiling_safe(rk, nlev)
  ks = ke - 1

  SELECT CASE (NINT(elm))
  CASE(id_u_obs)  ! U
    CALL itpl_3d(v3d(:, :, :, iv3d_u), ri, rj, rk, yobs)
  CASE(id_v_obs)  ! V
    CALL itpl_3d(v3d(:, :, :, iv3d_v), ri, rj, rk, yobs)
  CASE(id_w_obs)  ! W
    CALL itpl_3d(v3d(:, :, :, iv3d_w), ri, rj, rk, yobs)
  END SELECT

  RETURN
END SUBROUTINE Trans_XtoY
!-----------------------------------------------------------------------
! Compute relative humidity (RH)
!-----------------------------------------------------------------------
SUBROUTINE calc_rh(t,q,p,rh)
  IMPLICIT NONE
  REAL(r_size),PARAMETER :: t0=273.15d0
  REAL(r_size),PARAMETER :: e0c=6.11d0
  REAL(r_size),PARAMETER :: al=17.3d0
  REAL(r_size),PARAMETER :: bl=237.3d0
  REAL(r_size),PARAMETER :: e0i=6.1121d0
  REAL(r_size),PARAMETER :: ai=22.587d0
  REAL(r_size),PARAMETER :: bi=273.86d0
  REAL(r_size),INTENT(IN) :: t,q,p
  REAL(r_size),INTENT(OUT) :: rh
  REAL(r_size) :: e,es,tc

  e = q * p * 0.01d0 / (0.378d0 * q + 0.622d0)

  tc = t-t0
  IF(tc >= 0.0d0) THEN
    es = e0c * exp(al*tc/(bl+tc))
  ELSE IF(tc <= -15.d0) THEN
    es = e0i * exp(ai*tc/(bi+tc))
  ELSE
    es = e0c * exp(al*tc/(bl+tc)) * (15.0d0+tc)/15.0d0 &
       + e0i * exp(ai*tc/(bi+tc)) * (-tc) / 15.0d0
  END IF

  rh = e/es

  RETURN
END SUBROUTINE calc_rh
!-----------------------------------------------------------------------
! Pressure adjustment for a different height level
!-----------------------------------------------------------------------
SUBROUTINE prsadj(p,dz,t,q)
  IMPLICIT NONE
  REAL(r_size),INTENT(INOUT) :: p
  REAL(r_size),INTENT(IN) :: dz ! height difference (target - original) [m]
  REAL(r_size),INTENT(IN) :: t  ! temperature [K] at target level
  REAL(r_size),INTENT(IN) :: q  ! humidity [kg/kg] at target level
  REAL(r_size),PARAMETER :: gamma=5.0d-3 ! lapse rate [K/m]
  REAL(r_size) :: tv

  tv = t * (1.0d0 + 0.608d0 * q)
  IF(dz /= 0) THEN
!    p = p * ((-gamma*dz+tv)/tv)**(gg/(gamma*rd)) !tv is at original level
    p = p * (tv/(tv+gamma*dz))**(gg/(gamma*rd)) !tv is at target level
  END IF

  RETURN
END SUBROUTINE prsadj
!-----------------------------------------------------------------------
! Coordinate conversion
!-----------------------------------------------------------------------
!SUBROUTINE phys2ijk(p_full,elem,rlon,rlat,rlev,ri,rj,rk)
!DEC$ ATTRIBUTES FORCEINLINE :: phys2ijk
SUBROUTINE phys2ijk(elem, rlon, rlat, rlev, ri, rj, rk)
  IMPLICIT NONE
  !REAL(r_size),INTENT(IN) :: p_full(nlon,nlat,nlev)
  REAL(r_size),INTENT(IN) :: elem
  REAL(r_size),INTENT(IN) :: rlon
  REAL(r_size),INTENT(IN) :: rlat
  REAL(r_size),INTENT(IN) :: rlev ! pressure levels
  REAL(r_size),INTENT(OUT) :: ri
  REAL(r_size),INTENT(OUT) :: rj
  REAL(r_size),INTENT(OUT) :: rk
  REAL(r_size) :: aj,ak
  !REAL(r_size) :: lnps(nlon,nlat)
  !REAL(r_size) :: plev(nlev)
  INTEGER :: i,j,k
!
! rlon -> ri
!
  !IF(rlon == 0.0 .OR. rlon == 360.0) THEN
  !  ri = REAL(nlon+1,r_size)
  !ELSE
  !  ri = rlon / 360.0d0 * REAL(nlon,r_size) + 1.0d0
  !END IF
  if(rlon .le. lon(1) .and. rlon + 360.0d0 .ge. lon(nlon)) then
     ri = nlon + (rlon + 360.0d0 - lon(nlon)) / (lon(1) + 360.0d0 - lon(nlon))
  else if(rlon .ge. lon(nlon) .and. rlon - 360.0d0 .ge. lon(1)) then
     ri = nlon + (rlon - lon(nlon)) / (lon(1) + 360.0d0 - lon(nlon))
  else
     i = com_binary_search_real(nlon, lon, rlon)
     ri = i + (rlon - lon(i)) / (lon(i + 1) - lon(i))
  end if
  IF(ceiling_safe(ri, nlon + 1) < 2 .OR. nlon + 1 < ceiling_safe(ri, nlon + 1)) RETURN
!
! rlat -> rj
!
  if(lat(1) < lat(2)) then
     if(rlat > lat(nlat)) then
        j = nlat + 1
     else if(rlat < lat(1)) then
        j = 1
     else
        j = com_binary_search_real(nlat, lat, rlat) + 1
     end if
  else
     if(rlat < lat(nlat)) then
        j = nlat + 1
     else if(rlat > lat(1)) then
        j = 1
     else
        j = com_binary_search_real(nlat, lat, rlat) + 1
     end if
  end if
  IF(j == 1) THEN
  !IF(j == 0) THEN
    rj = (rlat + 90.0d0) / (lat(1) + 90.0d0)
  ELSE IF(j == nlat+1) THEN
    aj = (rlat - lat(nlat)) / (90.0d0 - lat(nlat))
    rj = REAL(nlat,r_size) + aj
  ELSE
    aj = (rlat - lat(j-1)) / (lat(j) - lat(j-1))
    rj = REAL(j-1,r_size) + aj
  END IF
  IF(ceiling_safe(rj, nlat) < 2 .OR. nlat < ceiling_safe(rj, nlat)) RETURN
!
! rlev -> rk
!
  !IF(NINT(elem) == id_ps_obs) THEN ! surface pressure observation
  !  rk = 0.0d0
  !ELSE
    !
    ! horizontal interpolation
    !
    !i = CEILING(ri)
    !j = CEILING(rj)
    !DO k=1,nlev
    !  IF(i <= nlon) THEN
    !    lnps(i-1:i,j-1:j) = LOG(p_full(i-1:i,j-1:j,k))
    !  ELSE
    !    lnps(i-1,j-1:j) = LOG(p_full(i-1,j-1:j,k))
    !    lnps(1,j-1:j) = LOG(p_full(1,j-1:j,k))
    !  END IF
    !  CALL itpl_2d(lnps,ri,rj,plev(k))
    !END DO

  !
  ! INTERPOLATION OF GEOMETRICAL HEIGHT MUST BE IMPLEMENTED HERE FOR 3D NOWCASTING !!!
  !
    !
    ! Log pressure
    !
    !rk = LOG(rlev)
    !
    ! find rk
    !
    !if(nlev > 1) then
    !   DO k=2,nlev-1
    !      IF(plev(k) < rk) EXIT ! assuming descending order of plev
    !   END DO
    !   ak = (rk - plev(k-1)) / (plev(k) - plev(k-1))
    !   rk = REAL(k-1,r_size) + ak
    !else
    !   rk = 0.0d0
    !end if

  ! for equally-spaced cartesian coordinate system (3D nowcasting)
  rk = rlev

  !END IF

  RETURN
END SUBROUTINE phys2ijk
!-----------------------------------------------------------------------
! Interpolation
!-----------------------------------------------------------------------
SUBROUTINE itpl_2d(var,ri,rj,var5)
  IMPLICIT NONE
  REAL(r_size),INTENT(IN) :: var(nlon,nlat)
  REAL(r_size),INTENT(IN) :: ri
  REAL(r_size),INTENT(IN) :: rj
  REAL(r_size),INTENT(OUT) :: var5
  REAL(r_size) :: ai,aj
  INTEGER :: i,j

  i = ceiling_safe(ri, nlon + 1)
  ai = ri - REAL(i - 1, r_size)
  j = ceiling_safe(rj, nlat)
  aj = rj - REAL(j - 1, r_size)

  IF(i <= nlon) THEN
    var5 = var(i-1,j-1) * (1-ai) * (1-aj) &
       & + var(i  ,j-1) *    ai  * (1-aj) &
       & + var(i-1,j  ) * (1-ai) *    aj  &
       & + var(i  ,j  ) *    ai  *    aj
  ELSE
    var5 = var(i-1,j-1) * (1-ai) * (1-aj) &
       & + var(1  ,j-1) *    ai  * (1-aj) &
       & + var(i-1,j  ) * (1-ai) *    aj  &
       & + var(1  ,j  ) *    ai  *    aj
  END IF

  RETURN
END SUBROUTINE itpl_2d

!DEC$ ATTRIBUTES FORCEINLINE :: itpl_3d
SUBROUTINE itpl_3d(var,ri,rj,rk,var5)
  IMPLICIT NONE
  REAL(r_size),INTENT(IN) :: var(nlon,nlat,nlev)
  REAL(r_size),INTENT(IN) :: ri
  REAL(r_size),INTENT(IN) :: rj
  REAL(r_size),INTENT(IN) :: rk
  REAL(r_size),INTENT(OUT) :: var5
  REAL(r_size) :: ai,aj,ak
  INTEGER :: i,j,k

  i = ceiling_safe(ri, nlon + 1)
  ai = ri - REAL(i - 1, r_size)
  j = ceiling_safe(rj, nlat)
  aj = rj - REAL(j - 1, r_size)
  k = ceiling_safe(rk, nlev)
  ak = rk - REAL(k - 1, r_size)

  if(nlev > 1) then
     IF(i <= nlon) THEN
        var5 = var(i-1,j-1,k-1) * (1-ai) * (1-aj) * (1-ak) &
           & + var(i  ,j-1,k-1) *    ai  * (1-aj) * (1-ak) &
           & + var(i-1,j  ,k-1) * (1-ai) *    aj  * (1-ak) &
           & + var(i  ,j  ,k-1) *    ai  *    aj  * (1-ak) &
           & + var(i-1,j-1,k  ) * (1-ai) * (1-aj) *    ak  &
           & + var(i  ,j-1,k  ) *    ai  * (1-aj) *    ak  &
           & + var(i-1,j  ,k  ) * (1-ai) *    aj  *    ak  &
           & + var(i  ,j  ,k  ) *    ai  *    aj  *    ak
     ELSE !cyclic boundary condition
        var5 = var(i-1,j-1,k-1) * (1-ai) * (1-aj) * (1-ak) &
           & + var(1  ,j-1,k-1) *    ai  * (1-aj) * (1-ak) &
           & + var(i-1,j  ,k-1) * (1-ai) *    aj  * (1-ak) &
           & + var(1  ,j  ,k-1) *    ai  *    aj  * (1-ak) &
           & + var(i-1,j-1,k  ) * (1-ai) * (1-aj) *    ak  &
           & + var(1  ,j-1,k  ) *    ai  * (1-aj) *    ak  &
           & + var(i-1,j  ,k  ) * (1-ai) *    aj  *    ak  &
           & + var(1  ,j  ,k  ) *    ai  *    aj  *    ak
     END IF
  else
     IF(i <= nlon) THEN
        var5 =   var(i - 1, j - 1, 1) * (1 - ai) * (1 - aj) &
             & + var(i    , j - 1, 1) *      ai  * (1 - aj) &
             & + var(i - 1, j    , 1) * (1 - ai) *      aj  &
             & + var(i    , j    , 1) *      ai    *    aj
     ELSE
        var5 =   var(i - 1, j - 1, 1) * (1 - ai) * (1 - aj) &
             & + var(1    , j - 1, 1) *      ai  * (1 - aj) &
             & + var(i - 1, j    , 1) * (1 - ai) *      aj  &
             & + var(1    , j    , 1) *      ai  *      aj
     END IF
  end if

  RETURN
END SUBROUTINE itpl_3d
!-----------------------------------------------------------------------
! Monitor departure
!-----------------------------------------------------------------------
SUBROUTINE monit_dep(nn, elm, dep, qc)
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: nn
  REAL(r_size), INTENT(IN) :: elm(nn)
  REAL(r_size), INTENT(IN) :: dep(nn)
  INTEGER, INTENT(IN) :: qc(nn)
  REAL(r_size) :: rmse_u, rmse_v, rmse_w
  REAL(r_size) :: bias_u, bias_v, bias_w
  INTEGER :: n, iu, iv, iw

  rmse_u = 0.0d0
  rmse_v = 0.0d0
  rmse_w = 0.0d0
  bias_u = 0.0d0
  bias_v = 0.0d0
  bias_w = 0.0d0
  iu = 0
  iv = 0
  iw = 0
!$omp parallel do private(n) reduction(+: rmse_u, rmse_v, rmse_w, bias_u, bias_v, bias_w, iu, iv, iw)
  DO n = 1, nn
    IF(qc(n) /= 1) CYCLE
    SELECT CASE(NINT(elm(n)))
    CASE(id_u_obs)
      rmse_u = rmse_u + dep(n) ** 2
      bias_u = bias_u + dep(n)
      iu = iu + 1
    CASE(id_v_obs)
      rmse_v = rmse_v + dep(n) ** 2
      bias_v = bias_v + dep(n)
      iv = iv + 1
    CASE(id_w_obs)
      rmse_w = rmse_w + dep(n) ** 2
      bias_w = bias_w + dep(n)
      iw = iw + 1
    END SELECT
  END DO !n
!$omp end parallel do
  IF(iu == 0) THEN
    rmse_u = undef
    bias_u = undef
  ELSE
    rmse_u = SQRT(rmse_u / REAL(iu, r_size))
    bias_u = bias_u / REAL(iu, r_size)
  END IF
  IF(iv == 0) THEN
    rmse_v = undef
    bias_v = undef
  ELSE
    rmse_v = SQRT(rmse_v / REAL(iv, r_size))
    bias_v = bias_v / REAL(iv, r_size)
  END IF
  IF(iw == 0) THEN
    rmse_w = undef
    bias_w = undef
  ELSE
    rmse_w = SQRT(rmse_w / REAL(iw, r_size))
    bias_w = bias_w / REAL(iw, r_size)
  END IF

  WRITE(6,'(A)') '== OBSERVATIONAL DEPARTURE BIAS ========================================'
  WRITE(6,'(3A12)') 'U','V','W'
  WRITE(6,'(3ES12.3)') bias_u, bias_v, bias_w
  WRITE(6,'(A)') '== OBSERVATIONAL DEPARTURE RMSE ========================================'
  WRITE(6,'(3A12)') 'U','V','W'
  WRITE(6,'(3ES12.3)') rmse_u, rmse_v, rmse_w
  WRITE(6,'(A)') '== NUMBER OF OBSERVATIONS TO BE ASSIMILATED ============================'
  WRITE(6,'(3A12)') 'U','V','W'
  WRITE(6,'(3I12)') iu, iv, iw
  WRITE(6,'(A)') '========================================================================'

  if(rmse_u * 0 .ne. 0) then
     write(6, *) "Error: input has NaN"
     stop 99
  end if
  if(rmse_v * 0 .ne. 0) then
     write(6, *) "Error: input has NaN"
     stop 99
  end if
  if(rmse_w * 0 .ne. 0) then
     write(6, *) "Error: input has NaN"
     stop 99
  end if

  RETURN
END SUBROUTINE monit_dep
!-----------------------------------------------------------------------
! Basic modules for observation input
!-----------------------------------------------------------------------
SUBROUTINE get_nobs(cfile, nrec, nn)
  IMPLICIT NONE
  CHARACTER(*), INTENT(IN) :: cfile
  INTEGER, INTENT(IN) :: nrec
  INTEGER, INTENT(OUT) :: nn
  REAL(r_sngl), ALLOCATABLE :: wk(:)
  INTEGER :: ios
  INTEGER :: iu, iv, iw
  INTEGER :: iunit
  LOGICAL :: ex

  ALLOCATE(wk(nrec))
  nn = 0
  iu = 0
  iv = 0
  iw = 0
  iunit=91
  INQUIRE(FILE = cfile, EXIST = ex)
  IF(ex) THEN
    OPEN(iunit, FILE = cfile, FORM = 'unformatted', ACCESS = 'sequential')
    DO
      READ(iunit, IOSTAT = ios) wk
      IF(ios /= 0) EXIT
      SELECT CASE(NINT(wk(1)))
      CASE(id_u_obs)
        iu = iu + 1
      CASE(id_v_obs)
        iv = iv + 1
      CASE(id_w_obs)
        iw = iw + 1
      END SELECT
      nn = nn + 1
    END DO
    WRITE(6, '(I10, A)') nn,' OBSERVATIONS INPUT'
    WRITE(6, '(A12, I10)') '          U:', iu
    WRITE(6, '(A12, I10)') '          V:', iv
    WRITE(6, '(A12, I10)') '          W:', iw
    CLOSE(iunit)
  ELSE
    WRITE(6, '(2A)') cfile, ' does not exist -- skipped'
  END IF
  DEALLOCATE(wk)

  RETURN
END SUBROUTINE get_nobs

SUBROUTINE read_obs(cfile,nn,elem,rlon,rlat,rlev,odat,oerr)
  IMPLICIT NONE
  CHARACTER(*),INTENT(IN) :: cfile
  INTEGER,INTENT(IN) :: nn
  REAL(r_size),INTENT(OUT) :: elem(nn) ! element number
  REAL(r_size),INTENT(OUT) :: rlon(nn)
  REAL(r_size),INTENT(OUT) :: rlat(nn)
  REAL(r_size),INTENT(OUT) :: rlev(nn)
  REAL(r_size),INTENT(OUT) :: odat(nn)
  REAL(r_size),INTENT(OUT) :: oerr(nn)
  integer, parameter :: nread = 10000
  REAL(r_sngl) :: wk(6, 0:(nread - 1))
  INTEGER :: n, iunit
  integer m, mmax, n0, n1

  mmax = nn / nread
  if(mod(nn, nread) > 0) mmax = mmax + 1

  iunit=91
  OPEN(iunit,FILE=cfile,FORM='unformatted',ACCESS='sequential')
  do m = 1, mmax
     n0 = (m - 1) * nread + 1
     n1 = m * nread
     if(n1 > nn) n1 = nn
     do n = n0, n1
        read(iunit) wk(:, n - n0)
     end do
!$omp parallel do private(n)
     do n = n0, n1
        !SELECT CASE(NINT(wk(1)))
        !CASE(id_u_obs)
        !  wk(4) = wk(4) * 100.0 ! hPa -> Pa
        !CASE(id_v_obs)
        !  wk(4) = wk(4) * 100.0 ! hPa -> Pa
        !CASE(id_w_obs)
        !  wk(4) = wk(4) * 100.0 ! hPa -> Pa
        !CASE(id_t_obs)
        !  wk(4) = wk(4) * 100.0 ! hPa -> Pa
        !CASE(id_q_obs)
        !  wk(4) = wk(4) * 100.0 ! hPa -> Pa
        !CASE(id_ps_obs)
        !  wk(5) = wk(5) * 100.0 ! hPa -> Pa
        !  wk(6) = wk(6) * 100.0 ! hPa -> Pa
        !CASE(id_rh_obs)
        !  wk(4) = wk(4) * 100.0 ! hPa -> Pa
        !  wk(5) = wk(5) * 0.01 ! percent input
        !  wk(6) = wk(6) * 0.01 ! percent input
        !END SELECT
        elem(n) = real(wk(1, n - n0), r_size)
        rlon(n) = real(wk(2, n - n0), r_size)
        rlat(n) = real(wk(3, n - n0), r_size)
        rlev(n) = real(wk(4, n - n0), r_size)
        odat(n) = real(wk(5, n - n0), r_size)
        oerr(n) = real(wk(6, n - n0), r_size)
     end do
!$omp end parallel do
  end do
  CLOSE(iunit)

  RETURN
END SUBROUTINE read_obs

SUBROUTINE read_obs2(cfile, nn, elem, rlon, rlat, rlev, odat, oerr, ohx, oqc)
  IMPLICIT NONE
  CHARACTER(*),INTENT(IN) :: cfile
  INTEGER,INTENT(IN) :: nn
  REAL(r_size),INTENT(OUT) :: elem(nn) ! element number
  REAL(r_size),INTENT(OUT) :: rlon(nn)
  REAL(r_size),INTENT(OUT) :: rlat(nn)
  REAL(r_size),INTENT(OUT) :: rlev(nn)
  REAL(r_size),INTENT(OUT) :: odat(nn)
  REAL(r_size),INTENT(OUT) :: oerr(nn)
  REAL(r_size),INTENT(OUT) :: ohx(nn)
  INTEGER,INTENT(OUT) :: oqc(nn)
  integer, parameter :: nread = 10000
  REAL(r_sngl) :: wk(8, 0:nread)
  INTEGER :: n,iunit
  integer m, mmax, n0, n1

  mmax = nn / nread
  if(mod(nn, nread) > 0) mmax = mmax + 1

  iunit=91
  OPEN(iunit,FILE=cfile,FORM='unformatted',ACCESS='sequential')
  do m = 1, mmax
     n0 = (m - 1) * nread + 1
     n1 = m * nread
     if(n1 > nn) n1 = nn
     do n = n0, n1
        read(iunit) wk(:, n - n0)
     end do
!$omp parallel do private(n)
     do n = n0, n1
        !SELECT CASE(NINT(wk(1)))
        !CASE(id_u_obs)
        !  wk(4) = wk(4) * 100.0 ! hPa -> Pa
        !CASE(id_v_obs)
        !  wk(4) = wk(4) * 100.0 ! hPa -> Pa
        !CASE(id_w_obs)
        !  wk(4) = wk(4) * 100.0 ! hPa -> Pa
        !CASE(id_t_obs)
        !  wk(4) = wk(4) * 100.0 ! hPa -> Pa
        !CASE(id_q_obs)
        !  wk(4) = wk(4) * 100.0 ! hPa -> Pa
        !CASE(id_ps_obs)
        !  wk(5) = wk(5) * 100.0 ! hPa -> Pa
        !  wk(6) = wk(6) * 100.0 ! hPa -> Pa
        !CASE(id_rh_obs)
        !  wk(4) = wk(4) * 100.0 ! hPa -> Pa
        !  wk(5) = wk(5) * 0.01 ! percent input
        !  wk(6) = wk(6) * 0.01 ! percent input
        !END SELECT
        elem(n) = REAL(wk(1, n - n0), r_size)
        rlon(n) = REAL(wk(2, n - n0), r_size)
        rlat(n) = REAL(wk(3, n - n0), r_size)
        rlev(n) = REAL(wk(4, n - n0), r_size)
        odat(n) = REAL(wk(5, n - n0), r_size)
        oerr(n) = REAL(wk(6, n - n0), r_size)
        ohx(n)  = REAL(wk(7, n - n0), r_size)
        oqc(n)  = NINT(wk(8, n - n0))
     end do
!$omp end parallel do
  end do
  CLOSE(iunit)

  RETURN
END SUBROUTINE read_obs2

SUBROUTINE write_obs2(cfile,nn,elem,rlon,rlat,rlev,odat,oerr,ohx,oqc)
  IMPLICIT NONE
  CHARACTER(*),INTENT(IN) :: cfile
  INTEGER,INTENT(IN) :: nn
  REAL(r_size),INTENT(IN) :: elem(nn) ! element number
  REAL(r_size),INTENT(IN) :: rlon(nn)
  REAL(r_size),INTENT(IN) :: rlat(nn)
  REAL(r_size),INTENT(IN) :: rlev(nn)
  REAL(r_size),INTENT(IN) :: odat(nn)
  REAL(r_size),INTENT(IN) :: oerr(nn)
  REAL(r_size),INTENT(IN) :: ohx(nn)
  INTEGER,INTENT(IN) :: oqc(nn)
  integer, parameter :: nwrite = 10000
  REAL(r_sngl) :: wk(8, 0:nwrite)
  INTEGER :: n,iunit
  integer m, mmax, n0, n1

  mmax = nn / nwrite
  if(mod(nn, nwrite) > 0) mmax = mmax + 1

  iunit=92
  OPEN(iunit,FILE=cfile,FORM='unformatted',ACCESS='sequential')
  do m = 1, mmax
     n0 = (m - 1) * nwrite + 1
     n1 = m * nwrite
     if(n1 > nn) n1 = nn
!$omp parallel do private(n)
     do n = n0, n1
        wk(1, n - n0) = REAL(elem(n),r_sngl)
        wk(2, n - n0) = REAL(rlon(n),r_sngl)
        wk(3, n - n0) = REAL(rlat(n),r_sngl)
        wk(4, n - n0) = REAL(rlev(n),r_sngl)
        wk(5, n - n0) = REAL(odat(n),r_sngl)
        wk(6, n - n0) = REAL(oerr(n),r_sngl)
        wk(7, n - n0) = REAL(ohx(n),r_sngl)
        wk(8, n - n0) = REAL(oqc(n),r_sngl)
        !SELECT CASE(NINT(wk(1)))
        !CASE(id_u_obs)
        !  wk(4) = wk(4) * 0.01 ! Pa -> hPa
        !CASE(id_v_obs)
        !  wk(4) = wk(4) * 0.01
        !CASE(id_w_obs)
        !  wk(4) = wk(4) * 0.01
        !CASE(id_t_obs)
        !  wk(4) = wk(4) * 0.01
        !CASE(id_q_obs)
        !  wk(4) = wk(4) * 0.01
        !CASE(id_ps_obs)
        !  wk(5) = wk(5) * 0.01
        !  wk(6) = wk(6) * 0.01
        !CASE(id_rh_obs)
        !  wk(4) = wk(4) * 0.01
        !  wk(5) = wk(5) * 100.0
        !  wk(6) = wk(6) * 100.0
        !END SELECT
     end do
!$omp end parallel do
     do n = n0, n1
        write(iunit) wk(:, n - n0)
     end do
  end do
  CLOSE(iunit)

  RETURN
END SUBROUTINE write_obs2

END MODULE common_obs_nowcast
