MODULE letkf_tools
!=======================================================================
!
! [PURPOSE:] Module for LETKF with nowcast
!
! [HISTORY:]
!   01/26/2009 Takemasa Miyoshi  created
!
!=======================================================================
  USE common
  USE common_mpi
  USE common_nowcast
  USE common_mpi_nowcast
  USE common_letkf
  USE letkf_obs

  IMPLICIT NONE

  PRIVATE
  PUBLIC :: das_letkf, cov_infl_mul, sp_infl_add, letkf_tools_config
  PUBLIC :: rtps_infl_alpha, rtcs_infl_const, rtcs_infl_alpha, rtcs_allow_deflation, num_obs_limit

  INTEGER,SAVE :: nobstotal

  !REAL(r_size),PARAMETER :: cov_infl_mul = -1.01d0 !multiplicative inflation
  REAL(r_size) :: cov_infl_mul
! > 0: globally constant covariance inflation
! < 0: 3D inflation values input from a GPV file "infl_mul.grd"
  !REAL(r_size),PARAMETER :: sp_infl_add = 0.d0 !additive inflation
  REAL(r_size) :: sp_infl_add
!TVS  LOGICAL,PARAMETER :: msw_vbc = .FALSE.
  REAL(r_size) :: rtps_infl_alpha = 0.0d0 ! Relaxation To Prior Spread, parameter alpha, by S.O.
  REAL(r_size) :: rtcs_infl_const(nv3d) = (/ 0.0d0, 0.0d0, 0.0d0 /) ! Relaxation To Constant Spread, by S.O.
  REAL(r_size) :: rtcs_infl_alpha = 0.0d0 ! Relaxation To Constant Spread, parameter alpha, by S.O.
  logical :: rtcs_allow_deflation = .true. ! Relaxation To Constant Spread, by S.O.
  integer :: num_obs_limit = 0 ! Obs Num Limit, by S.O.
  REAL(r_size),PARAMETER :: var_local(nv3d+nv2d,nid_obs) = RESHAPE( &
!!           U      V      T      Q     PS   RAIN
!   & (/ 1.0d0, 1.0d0, 1.0d0, 1.0d0, 1.0d0, 1.0d0,  & ! U
!   &    1.0d0, 1.0d0, 1.0d0, 1.0d0, 1.0d0, 1.0d0,  & ! V
!   &    1.0d0, 1.0d0, 1.0d0, 1.0d0, 1.0d0, 1.0d0,  & ! T
!   &    1.0d0, 1.0d0, 1.0d0, 1.0d0, 1.0d0, 1.0d0,  & ! Q
!   &    1.0d0, 1.0d0, 1.0d0, 1.0d0, 1.0d0, 1.0d0,  & ! RH
!   &    1.0d0, 1.0d0, 1.0d0, 1.0d0, 1.0d0, 1.0d0,  & ! PS
!   &    1.0d0, 1.0d0, 1.0d0, 1.0d0, 1.0d0, 1.0d0 /)& ! RAIN
!   & ,(/nv3d+nv2d,nid_obs/))
!           U      V      W
   & (/ 1.0d0, 1.0d0, 1.0d0,  & ! U
   &    1.0d0, 1.0d0, 1.0d0,  & ! V
   &    1.0d0, 1.0d0, 1.0d0 /)& ! W
   & ,(/nv3d+nv2d,nid_obs/))
  INTEGER,SAVE :: var_local_n2n(nv3d+nv2d)

  namelist /letkf_tools_config/ cov_infl_mul, sp_infl_add, &
       & rtps_infl_alpha, rtcs_infl_const, rtcs_infl_alpha, rtcs_allow_deflation, num_obs_limit

CONTAINS
!-----------------------------------------------------------------------
! Data Assimilation
!-----------------------------------------------------------------------
SUBROUTINE das_letkf(gues3d, gues2d, anal3d, anal2d)
  IMPLICIT NONE
  CHARACTER(12) :: inflfile='infl_mul.grd'
  REAL(r_size),INTENT(INOUT) :: gues3d(nij1, nlev, nbv, nv3d) ! background ensemble
  REAL(r_size),INTENT(INOUT) :: gues2d(nij1, nbv, nv2d)      !  output: destroyed
  REAL(r_size),INTENT(OUT) :: anal3d(nij1, nlev, nbv, nv3d) ! analysis ensemble
  REAL(r_size),INTENT(OUT) :: anal2d(nij1, nbv, nv2d)
  REAL(r_size),ALLOCATABLE :: mean3d(:, :, :)
  REAL(r_size),ALLOCATABLE :: mean2d(:, :)
  REAL(r_size),ALLOCATABLE :: hdxf(:, :)
  REAL(r_size),ALLOCATABLE :: rdiag(:)
  REAL(r_size),ALLOCATABLE :: rloc(:)
  REAL(r_size),ALLOCATABLE :: dep(:)
  REAL(r_size),ALLOCATABLE :: work3d(:, :, :)
  REAL(r_size),ALLOCATABLE :: work2d(:, :)
  REAL(r_sngl),ALLOCATABLE :: work3dg(:, :, :, :)
  REAL(r_sngl),ALLOCATABLE :: work2dg(:, :, :)
  REAL(r_size),ALLOCATABLE :: logpfm(:, :)
  REAL(r_size) :: parm
  !REAL(r_size) :: trans(nbv, nbv, nv3d + nv2d)
  REAL(r_size), allocatable :: trans(:, :, :)
  LOGICAL :: ex
  INTEGER :: ij, ilev, n, m, i, j, k, nobsl, ierr
  !+++for RTPS, by S.O.+++
  real(r_size) anal_sp, gues_sp, rtps_parm, rtcs_parm
  real(r_size), allocatable :: anal_mean(:), anal_ptb(:, :)
  !+++++++++++++++++++++++

  WRITE(6,'(A)') 'Hello from das_letkf'
  nobstotal = nobs !+ ntvs
  WRITE(6,'(A,I8)') 'Target observation numbers : NOBS=',nobs!,', NTVS=',ntvs
  !
  ! In case of no obs
  !
  IF(nobstotal == 0) THEN
    WRITE(6,'(A)') 'No observation assimilated'
!$omp parallel workshare
    anal3d = gues3d
    anal2d = gues2d
!$omp end parallel workshare
    RETURN
  END IF
  !
  ! Variable localization
  !
  var_local_n2n(1) = 1
  DO n = 2, nv3d + nv2d
    DO i = 1, n
      var_local_n2n(n) = i
      IF(MAXVAL(ABS(var_local(i,:)-var_local(n,:))) < TINY(var_local)) EXIT
    END DO
  END DO
!print *,var_local_n2n
  !
  ! FCST PERTURBATIONS
  !
  ALLOCATE(mean3d(nij1, nlev, nv3d))
  ALLOCATE(mean2d(nij1, nv2d))
  CALL ensmean_grd(nbv, nij1, gues3d, gues2d, mean3d, mean2d)
!$omp parallel private(i, k, m, n)
!$omp do collapse(4)
  DO n = 1, nv3d
    DO m = 1, nbv
      DO k = 1, nlev
        DO i = 1, nij1
          gues3d(i, k, m, n) = gues3d(i, k, m, n) - mean3d(i, k, n)
        END DO !i
      END DO !k
    END DO !m
  END DO !n
!$omp end do
!$omp do collapse(3)
  DO n = 1, nv2d
    DO m = 1, nbv
      DO i = 1, nij1
        gues2d(i, m, n) = gues2d(i, m, n) - mean2d(i, n)
      END DO !i
    END DO !m
  END DO !n
!$omp end do
!$omp end parallel
  !
  ! multiplicative inflation
  !
  ALLOCATE( work3d(nij1, nlev, nv3d) )
  ALLOCATE( work2d(nij1, nv2d) )
  IF(cov_infl_mul > 0.0d0) THEN ! fixed multiplicative inflation parameter
!$omp parallel
!$omp workshare
     work3d = cov_infl_mul
     work2d = cov_infl_mul
!$omp end workshare
!$omp do private(i, n)
     do n = 1, nv3d
        do i = 1, nij1
           work3d(i, nlev, n) = 1.01d0 !!! MUST BE REMOVED IN FUTURE !!!
        end do !i
     end do !n
!$omp end do
!$omp end parallel
  else ! 3D parameter values are read-in
     ALLOCATE( work3dg(nlon, nlat, nlev, nv3d) )
     ALLOCATE( work2dg(nlon, nlat, nv2d) )
     INQUIRE(FILE=inflfile,EXIST=ex)
     IF(ex) THEN
        IF(myrank == 0) THEN
           WRITE(6,'(A,I3.3,2A)') 'MYRANK ',myrank,' is reading.. ',inflfile
           CALL read_grd4(inflfile,work3dg,work2dg)
        END IF
        CALL scatter_grd_mpi(0, work3dg, work2dg, work3d, work2d, nv3d, nv2d)
     ELSE
        WRITE(6,'(2A)') '!!WARNING: no such file exist: ',inflfile
!$omp parallel
!$omp workshare
        work3d = -1.0d0 * cov_infl_mul
        work2d = -1.0d0 * cov_infl_mul
!$omp end workshare
!$omp end parallel
     END IF
  END IF
  !
  ! p_full for background ensemble mean
  !
  !ALLOCATE(logpfm(nij1,nlev))
  !CALL calc_pfull(nij1,1,mean2d(:,iv2d_ps),logpfm)
  !logpfm = DLOG(logpfm)
  !
  ! MAIN ASSIMILATION LOOP
  !
  !ALLOCATE( hdxf(nbv, nobstotal), rdiag(nobstotal), rloc(nobstotal), dep(nobstotal) ) !hdxf is transposed from original code
  !IF( nobs > 0 ) ALLOCATE(nobs_use(nobs)) !S.O.
  call set_common_obs_nowcast
  if(rtps_infl_alpha .gt. 0 .or. rtcs_infl_alpha .gt. 0) then
     write(6, '(A,F5.2)') "Relaxation to Prior Spread, alpha = ", rtps_infl_alpha
     write(6, '(A,F5.2)') "Relaxation to Constant Spread, alpha = ", rtcs_infl_alpha
     write(6, '(A,F5.2)') "Relaxation to Constant Spread, const = ", rtcs_infl_const
     !allocate(anal_mean(nv3d + nv2d), anal_ptb(nbv, nv3d + nv2d)) !S.O.
  end if
  DO ilev = 1, nlev
    WRITE(6,'(A,I3)') 'ilev = ',ilev
!$omp parallel do private(ij, i, j, n, m, trans, parm, gues_sp, anal_mean, anal_ptb, anal_sp) &
!$omp & private(rtps_parm, rtcs_parm, hdxf, rdiag, rloc, dep, nobsl) &
!$omp & shared(nij1, nbv, nobstotal, var_local_n2n, ilev, work3d, work2d, gues3d, gues2d, anal3d, anal2d) &
!$omp & shared(mean3d, mean2d, rtps_infl_alpha, rtcs_infl_alpha, rtcs_infl_const, rtcs_allow_deflation) &
!$omp & default(none)
    DO ij = 1, nij1
       allocate(trans(nbv, nbv, nv3d + nv2d))
       ALLOCATE( hdxf(nbv, nobstotal), rdiag(nobstotal), rloc(nobstotal), dep(nobstotal) ) !hdxf is transposed from original code
       allocate(anal_mean(nv3d + nv2d), anal_ptb(nbv, nv3d + nv2d)) !S.O.

      DO n = 1, nv3d
        IF(var_local_n2n(n) < n) THEN
           do j = 1, nbv
              do i = 1, nbv
                 trans(i, j, n) = trans(i, j, var_local_n2n(n))
              end do !i
           end do !j
           work3d(ij, ilev, n) = work3d(ij, ilev, var_local_n2n(n))
        ELSE
          !CALL obs_local(ij,ilev,n,hdxf,rdiag,rloc,dep,nobsl,logpfm)
          CALL obs_local(ij, ilev, n, hdxf, rdiag, rloc, dep, nobsl)
          parm = work3d(ij, ilev, n)
          CALL letkf_core(nobstotal, nobsl, hdxf, rdiag, rloc, dep, parm, trans(:, :, n))
          work3d(ij, ilev, n) = parm
        END IF
        DO m = 1, nbv
          anal3d(ij, ilev, m, n) = mean3d(ij, ilev, n) &
               & + sum(gues3d(ij, ilev, 1:nbv, n) * trans(1:nbv, m, n))
        END DO !m

        if(rtps_infl_alpha .gt. 0.0d0 .or. rtcs_infl_alpha .gt. 0.0d0) then !Relaxation to Prior Spread / constant spread, by S.O.
           !compute prior spread
           gues_sp = sum(gues3d(ij, ilev, 1:nbv, n) * gues3d(ij, ilev, 1:nbv, n))
           gues_sp = sqrt(gues_sp / (nbv - 1)) ! spread of each variable
           if(gues_sp > 0.0d0) then
              !compute analysis mean
              anal_mean(n) = sum(anal3d(ij, ilev, 1:nbv, n)) / nbv
              !compute analysis spread
              anal_ptb(1:nbv, n) = anal3d(ij, ilev, 1:nbv, n) - anal_mean(n)
              anal_sp = sum(anal_ptb(1:nbv, n) * anal_ptb(1:nbv, n))
              anal_sp = sqrt(anal_sp / (nbv - 1)) ! spread of each variable
              if(anal_sp > 0.0d0) then
                 !inflation
                 ! rtcs_infl_const must be different for different variables
                 ! but the same value is used because there are only U and V for the moment
                 rtps_parm = rtps_infl_alpha * (gues_sp - anal_sp) / anal_sp + real(1.0d0, r_size)
                 if(rtcs_allow_deflation .or. (rtcs_infl_const(n) .gt. (rtps_parm * anal_sp))) then
                    rtcs_parm = rtcs_infl_alpha * (rtcs_infl_const(n) &
                         & - (rtps_parm * anal_sp)) / (rtps_parm * anal_sp) + real(1.0d0, r_size)
                 else
                    rtcs_parm = real(1.0d0, r_size)
                 end if
                 anal3d(ij, ilev, 1:nbv, n) = anal_mean(n) + anal_ptb(1:nbv, n) * rtps_parm * rtcs_parm
              end if
           end if
        end if
      END DO ! n
      !if(rtps_infl_alpha .gt. 0 .or. rtcs_infl_alpha .gt. 0) then !Relaxation to Prior Spread / constant spread, by S.O.
      !   !compute prior spread
      !   gues_sp = sum(gues3d(ij, ilev, 1:nbv, 1:nv3d) * gues3d(ij, ilev, 1:nbv, 1:nv3d)) ! u^2 + v^2 + w^2
      !   gues_sp = sqrt(gues_sp / (nbv - 1))
      !   if(gues_sp > 0) then
      !      do n = 1, nv3d
      !         !compute analysis mean
      !         anal_mean(n) = sum(anal3d(ij, ilev, 1:nbv, n)) / nbv
      !         !compute analysis spread
      !         anal_ptb(1:nbv, n) = anal3d(ij, ilev, 1:nbv, n) - anal_mean(n)
      !      end do
      !      anal_sp = sum(anal_ptb(1:nbv, 1:nv3d) * anal_ptb(1:nbv, 1:nv3d)) ! u^2 + v^2 + w^2
      !      anal_sp = sqrt(anal_sp / (nbv - 1))
      !      if(anal_sp > 0) then
      !         !inflation
      !         rtps_parm = rtps_infl_alpha * (gues_sp - anal_sp) / anal_sp + 1
      !         rtcs_parm = rtcs_infl_alpha * (rtcs_infl_const - (rtps_parm * anal_sp)) / (rtps_parm * anal_sp) + 1
      !         do n = 1, nv3d
      !            anal3d(ij, ilev, 1:nbv, n) = anal_mean(n) + anal_ptb(1:nbv, n) * rtps_parm * rtcs_parm
      !         end do
      !      end if
      !   end if
      !end if !RTPS

      !IF(ilev >= 5) THEN !no analysis for upper-level Q
      !  DO m=1,nbv
      !    anal3d(ij,ilev,m,iv3d_q) = mean3d(ij,ilev,iv3d_q) &
      !                           & + gues3d(ij,ilev,m,iv3d_q)
      !  END DO
      !END IF
      IF(ilev == 1) THEN !update 2d variable at ilev=1
        DO n = 1, nv2d
          IF(var_local_n2n(nv3d + n) <= nv3d) THEN
            trans(:, :, nv3d + n) = trans(:, :, var_local_n2n(nv3d + n))
            work2d(ij, n) = work2d(ij, var_local_n2n(nv3d + n))
          ELSE IF(var_local_n2n(nv3d + n) < nv3d + n) THEN
            trans(:, :, nv3d + n) = trans(:, :, var_local_n2n(nv3d + n))
            work2d(ij, n) = work2d(ij, var_local_n2n(nv3d + n) - nv3d)
          ELSE
            !CALL obs_local(ij,ilev,nv3d+n,hdxf,rdiag,rloc,dep,nobsl,logpfm)
            CALL obs_local(ij, ilev, nv3d + n, hdxf, rdiag, rloc, dep, nobsl)
            parm = work2d(ij, n)
            CALL letkf_core(nobstotal, nobsl, hdxf, rdiag, rloc, dep, parm, trans(:, :, nv3d + n))
            work2d(ij, n) = parm
          END IF
          DO m = 1, nbv
            anal2d(ij, m, n) = mean2d(ij, n) &
                 & + sum(gues2d(ij, 1:nbv, n) * trans(1:nbv, m, nv3d + n))
          END DO !m
          if(rtps_infl_alpha .gt. 0) then !Relaxation to Prior Spread, by S.O.
             !compute prior spread
             gues_sp = sum(gues2d(ij, 1:nbv, n) * gues2d(ij, 1:nbv, n))
             gues_sp = sqrt(gues_sp / (nbv - 1)) ! spread of each variable
             if(gues_sp > 0) then
                !compute analysis mean
                anal_mean(nv3d + n) = sum(anal2d(ij, 1:nbv, n)) / nbv
                !compute analysis spread
                anal_ptb(1:nbv, nv3d + n) = anal2d(ij, 1:nbv, n) - anal_mean(nv3d + n)
                anal_sp = sum(anal_ptb(1:nbv, nv3d + n) * anal_ptb(1:nbv, nv3d + n))
                anal_sp = sqrt(anal_sp / (nbv - 1))
                if(anal_sp > 0) then
                   !inflation
                   ! use same rtps_parm as 3D variables
                   anal2d(ij, 1:nbv, n) = anal_mean(nv3d + n) + anal_ptb(1:nbv, nv3d + n) * rtps_parm
                end if
             end if
          end if !RTPS
        END DO ! n
      END IF

      deallocate(trans)
      DEALLOCATE(hdxf,rdiag,rloc,dep)
      if(rtps_infl_alpha .gt. 0 .or. rtcs_infl_alpha .gt. 0) deallocate(anal_mean, anal_ptb) !S.O.

    END DO ! ij
!$omp end parallel do

  END DO ! ilev

  !DEALLOCATE(hdxf,rdiag,rloc,dep)
  !IF( nobs > 0 ) DEALLOCATE(nobs_use) !S.O.
  call unset_common_obs_nowcast

  !if(rtps_infl_alpha .gt. 0 .or. rtcs_infl_alpha .gt. 0) deallocate(anal_mean, anal_ptb) !S.O.
  IF(cov_infl_mul < 0.0d0) THEN
    CALL gather_grd_mpi(0, work3d, work2d, work3dg, work2dg, nv3d, nv2d)
    IF(myrank == 0) THEN
      WRITE(6,'(A,I3.3,2A)') 'MYRANK ',myrank,' is writing.. ',inflfile
      CALL write_grd4(inflfile,work3dg,work2dg)
    END IF
    DEALLOCATE(work3dg,work2dg,work3d,work2d)
  END IF
  !
  ! Additive inflation
  !
  IF(sp_infl_add > 0.0d0) THEN
    CALL read_ens_mpi('addi',nbv,gues3d,gues2d)
    ALLOCATE( work3d(nij1,nlev,nv3d) )
    ALLOCATE( work2d(nij1,nv2d) )
    CALL ensmean_grd(nbv,nij1,gues3d,gues2d,work3d,work2d)
    DO n=1,nv3d
      DO m=1,nbv
        DO k=1,nlev
          DO i=1,nij1
            gues3d(i,k,m,n) = gues3d(i,k,m,n) - work3d(i,k,n)
          END DO
        END DO
      END DO
    END DO
    DO n=1,nv2d
      DO m=1,nbv
        DO i=1,nij1
          gues2d(i,m,n) = gues2d(i,m,n) - work2d(i,n)
        END DO
      END DO
    END DO

    DEALLOCATE(work3d,work2d)
    WRITE(6,'(A)') '===== Additive covariance inflation ====='
    WRITE(6,'(A,F10.4)') '  parameter:',sp_infl_add
    WRITE(6,'(A)') '========================================='
!    parm = 0.7d0
!    DO ilev=1,nlev
!      parm_infl_damp(ilev) = 1.0d0 + parm &
!        & + parm * REAL(1-ilev,r_size)/REAL(nlev_dampinfl,r_size)
!      parm_infl_damp(ilev) = MAX(parm_infl_damp(ilev),1.0d0)
!    END DO
    DO n=1,nv3d
      DO m=1,nbv
        DO ilev=1,nlev
          DO ij=1,nij1
            anal3d(ij,ilev,m,n) = anal3d(ij,ilev,m,n) &
              & + gues3d(ij,ilev,m,n) * sp_infl_add
          END DO
        END DO
      END DO
    END DO
    DO n=1,nv2d
      DO m=1,nbv
        DO ij=1,nij1
          anal2d(ij,m,n) = anal2d(ij,m,n) + gues2d(ij,m,n) * sp_infl_add
        END DO
      END DO
    END DO
  END IF

  !DEALLOCATE(logpfm,mean3d,mean2d)
  DEALLOCATE(mean3d, mean2d)
  RETURN
END SUBROUTINE das_letkf
!-----------------------------------------------------------------------
! Project global observations to local
!     (hdxf_g,dep_g,rdiag_g) -> (hdxf,dep,rdiag)
!-----------------------------------------------------------------------
!SUBROUTINE obs_local(ij,ilev,nvar,hdxf,rdiag,rloc,dep,nobsl,logpfm)
SUBROUTINE obs_local(ij,ilev,nvar,hdxf,rdiag,rloc,dep,nobsl)
  IMPLICIT NONE
  INTEGER,INTENT(IN) :: ij,ilev,nvar
  !REAL(r_size),INTENT(IN) :: logpfm(nij1,nlev)
  REAL(r_size),INTENT(OUT) :: hdxf(nbv, nobstotal) !transposed from original code
  REAL(r_size),INTENT(OUT) :: rdiag(nobstotal)
  REAL(r_size),INTENT(OUT) :: rloc(nobstotal)
  REAL(r_size),INTENT(OUT) :: dep(nobstotal)
  INTEGER,INTENT(OUT) :: nobsl
  REAL(r_size) :: minlon,maxlon,minlat,maxlat
  real(r_size) :: dist ,dlev
  REAL(r_size) :: tmplon,tmplat,tmperr!,tmpwgt(nlev)
  INTEGER :: tmpqc
  INTEGER, ALLOCATABLE :: nobs_use(:)
!TVS  INTEGER,ALLOCATABLE:: ntvs_use_prof(:),ntvs_use_inst(:),ntvs_use_slot(:)
  INTEGER :: imin,imax,jmin,jmax,im,ichan
  INTEGER :: n,nn,tvnn,iobs
  !for obsrvation number limit ---
  integer, allocatable :: iobsl(:)
  integer :: tmp_nobsl(nid_obs), count, tmp_nobsl_max
  real(r_size), allocatable :: tmp_hdxf(:, :, :), tmp_rdiag(:, :), tmp_rloc(:, :), tmp_dep(:, :)
  integer time1, time2, timerate, timemax
  integer :: id_obs
  real(r_size) :: sinlon0, coslon0, sinlat0, coslat0
  !-------------------------------
!
! INITIALIZE
!
  IF( nobs > 0 ) ALLOCATE(nobs_use(nobs))
!TVS  IF( ntvs > 0 ) THEN
!TVS    ALLOCATE(ntvs_use_prof(ntvs))
!TVS    ALLOCATE(ntvs_use_inst(ntvs))
!TVS    ALLOCATE(ntvs_use_slot(ntvs))
!TVS  END IF
!
! data search
!
  minlon = lon1(ij) - dlon_zero(ij)
  maxlon = lon1(ij) + dlon_zero(ij)
  minlat = lat1(ij) - dlat_zero
  maxlat = lat1(ij) + dlat_zero
  IF(maxlon - minlon >= 360.0d0) THEN
    minlon = 0.0d0
    maxlon = 360.0d0
  END IF
  jmin = com_binary_search_real(nlat, lat, minlat)
  if(jmin .ge. nlat - 1) jmin = nlat - 2
  jmax = com_binary_search_real(nlat, lat, maxlat)
  if(jmax .ge. nlat - 1) jmax = nlat - 2
  nn = 1
!TVS  tvnn = 1
  IF(minlon >= 0 .AND. maxlon <= 360.0) THEN
    imin = com_binary_search_real(nlon, lon, minlon)
    imax = com_binary_search_real(nlon, lon, maxlon)
    IF( nobs > 0 ) &
    & CALL obs_local_sub(imin,imax,jmin,jmax,nn, nobs_use)
!TVS    IF( ntvs > 0 ) &
!TVS    & CALL tvs_local_sub(imin,imax,jmin,jmax,tvnn, &
!TVS    &                    ntvs_use_prof,ntvs_use_inst,ntvs_use_slot)
  ELSE IF(minlon >= 0 .AND. maxlon > 360.0) THEN
    imin = com_binary_search_real(nlon, lon, minlon)
    maxlon = maxlon - 360.0d0
    IF(maxlon > 360.0d0) THEN
      imin = 1
      imax = nlon
      IF( nobs > 0 ) &
      & CALL obs_local_sub(imin,imax,jmin,jmax,nn, nobs_use)
!TVS      IF( ntvs > 0 ) &
!TVS      & CALL tvs_local_sub(imin,imax,jmin,jmax,tvnn, &
!TVS      &                    ntvs_use_prof,ntvs_use_inst,ntvs_use_slot)
    ELSE
      imax = com_binary_search_real(nlon, lon, maxlon)
      IF(imax > imin) THEN
        imin = 1
        imax = nlon
        IF( nobs > 0 ) &
        & CALL obs_local_sub(imin,imax,jmin,jmax,nn, nobs_use)
!TVS        IF( ntvs > 0 ) &
!TVS        & CALL tvs_local_sub(imin,imax,jmin,jmax,tvnn, &
!TVS        &                    ntvs_use_prof,ntvs_use_inst,ntvs_use_slot)
      ELSE
        imin = 1
        IF( nobs > 0 ) &
        & CALL obs_local_sub(imin,imax,jmin,jmax,nn, nobs_use)
!TVS        IF( ntvs > 0 ) &
!TVS        & CALL tvs_local_sub(imin,imax,jmin,jmax,tvnn, &
!TVS        &                    ntvs_use_prof,ntvs_use_inst,ntvs_use_slot)
        imin = com_binary_search_real(nlon, lon, minlon)
        imax = nlon
        IF( nobs > 0 ) &
        & CALL obs_local_sub(imin,imax,jmin,jmax,nn, nobs_use)
!TVS        IF( ntvs > 0 ) &
!TVS        & CALL tvs_local_sub(imin,imax,jmin,jmax,tvnn, &
!TVS        &                    ntvs_use_prof,ntvs_use_inst,ntvs_use_slot)
      END IF
    END IF
  ELSE IF(minlon < 0 .AND. maxlon <= 360.0d0) THEN
    imax = com_binary_search_real(nlon, lon, maxlon)
    minlon = minlon + 360.0d0
    IF(minlon < 0) THEN
      imin = 1
      imax = nlon
      IF( nobs > 0 ) &
      & CALL obs_local_sub(imin,imax,jmin,jmax,nn, nobs_use)
!TVS      IF( ntvs > 0 ) &
!TVS      & CALL tvs_local_sub(imin,imax,jmin,jmax,tvnn, &
!TVS      &                    ntvs_use_prof,ntvs_use_inst,ntvs_use_slot)
    ELSE
      imin = com_binary_search_real(nlon, lon, minlon)
      IF(imin < imax) THEN
        imin = 1
        imax = nlon
        IF( nobs > 0 ) &
        & CALL obs_local_sub(imin,imax,jmin,jmax,nn, nobs_use)
!TVS        IF( ntvs > 0 ) &
!TVS        & CALL tvs_local_sub(imin,imax,jmin,jmax,tvnn, &
!TVS        &                    ntvs_use_prof,ntvs_use_inst,ntvs_use_slot)
      ELSE
        imin = 1
        IF( nobs > 0 ) &
        & CALL obs_local_sub(imin,imax,jmin,jmax,nn, nobs_use)
!TVS        IF( ntvs > 0 ) &
!TVS        & CALL tvs_local_sub(imin,imax,jmin,jmax,tvnn, &
!TVS        &                    ntvs_use_prof,ntvs_use_inst,ntvs_use_slot)
        imin = com_binary_search_real(nlon, lon, minlon)
        imax = nlon
        IF( nobs > 0 ) &
        & CALL obs_local_sub(imin,imax,jmin,jmax,nn, nobs_use)
!TVS        IF( ntvs > 0 ) &
!TVS        & CALL tvs_local_sub(imin,imax,jmin,jmax,tvnn, &
!TVS        &                    ntvs_use_prof,ntvs_use_inst,ntvs_use_slot)
      END IF
    END IF
  ELSE
    maxlon = maxlon - 360.0d0
    minlon = minlon + 360.0d0
    IF(maxlon > 360.0 .OR. minlon < 0) THEN
      imin = 1
      imax = nlon
      IF( nobs > 0 ) &
      & CALL obs_local_sub(imin,imax,jmin,jmax,nn, nobs_use)
!TVS      IF( ntvs > 0 ) &
!TVS      & CALL tvs_local_sub(imin,imax,jmin,jmax,tvnn, &
!TVS      &                    ntvs_use_prof,ntvs_use_inst,ntvs_use_slot)
    ELSE
      imin = com_binary_search_real(nlon, lon, minlon)
      imax = com_binary_search_real(nlon, lon, maxlon)
      IF(imin > imax) THEN
        imin = 1
        imax = nlon
        IF( nobs > 0 ) &
        & CALL obs_local_sub(imin,imax,jmin,jmax,nn, nobs_use)
!TVS        IF( ntvs > 0 ) &
!TVS        & CALL tvs_local_sub(imin,imax,jmin,jmax,tvnn, &
!TVS        &                    ntvs_use_prof,ntvs_use_inst,ntvs_use_slot)
      ELSE
        IF( nobs > 0 ) &
        & CALL obs_local_sub(imin,imax,jmin,jmax,nn, nobs_use)
!TVS        IF( ntvs > 0 ) &
!TVS        & CALL tvs_local_sub(imin,imax,jmin,jmax,tvnn, &
!TVS        &                    ntvs_use_prof,ntvs_use_inst,ntvs_use_slot)
      END IF
    END IF
  END IF
  nn = nn-1
!TVS  tvnn = tvnn -1
!TVS  IF( nn < 1 .AND. tvnn < 1 ) THEN
  IF(nn < 1) THEN
    nobsl = 0
    RETURN
  END IF
  if(num_obs_limit > 0) tmp_nobsl = 0 !for obsrevation number limit
!
! CONVENTIONAL
!
  CALL com_distll_1_efficient_set2(lon1(ij), lat1(ij), sinlon0, coslon0, sinlat0, coslat0)
  nobsl = 0
  IF(nn > 0) THEN
     if(num_obs_limit > 0) allocate(iobsl(nn))
! !$omp parallel do private(n)
    DO n = 1, nn
      !
      ! vertical localization
      !
      !IF(NINT(obselm(nobs_use(n))) == id_ps_obs .AND. ilev > 1) THEN
      !  dlev = ABS(LOG(obsdat(nobs_use(n))) - logpfm(ij,ilev))
      !ELSE IF(NINT(obselm(nobs_use(n))) /= id_ps_obs) THEN
      !  dlev = ABS(LOG(obslev(nobs_use(n))) - logpfm(ij,ilev))
      !ELSE
      !  dlev = 0.0d0
      !END IF
      !
      ! for equally-spaced cartesian coordinate system (3D nowcasting)
      dlev = abs(obslev(nobs_use(n)) - ilev)
      IF(dlev > dist_zerov) CYCLE
      !
      ! horizontal localization
      !
      CALL com_distll_1_efficient2(coslon0, sinlon0, coslat0, sinlat0, &
           & obscoslon(nobs_use(n)), obssinlon(nobs_use(n)), obscoslat(nobs_use(n)), obssinlat(nobs_use(n)), dist) !added by Otsuka
      IF(dist > dist_zero ) CYCLE
      !
      ! variable localization
      !
      SELECT CASE(NINT(obselm(nobs_use(n))))
      CASE(id_u_obs)
        id_obs = 1
      CASE(id_v_obs)
        id_obs = 2
      CASE(id_w_obs)
        id_obs = 3
      !CASE(id_t_obs)
      !  id_obs=3
      !CASE(id_q_obs)
      !  id_obs=4
      !CASE(id_rh_obs)
      !  id_obs=5
      !CASE(id_ps_obs)
      !  id_obs=6
      !CASE(id_rain_obs)
      !  id_obs=7
      END SELECT
      IF(var_local(nvar, id_obs) < TINY(var_local)) CYCLE

      nobsl = nobsl + 1
      hdxf(:, nobsl) = obshdxf(:, nobs_use(n)) !transposed from original code
      dep(nobsl)     = obsdep(nobs_use(n))
      if(num_obs_limit > 0) then
         !Temporary array for observation number limit
         tmp_nobsl(id_obs) = tmp_nobsl(id_obs) + 1
         iobsl(nobsl) = id_obs
      end if
      !
      ! Observational localization
      !
      tmperr = obserr(nobs_use(n))
      rdiag(nobsl) = tmperr * tmperr
      rloc(nobsl) = EXP(-0.5d0 * ((dist / sigma_obs)**2 + (dlev / sigma_obsv)**2)) &
           &             * var_local(nvar, id_obs)
    END DO !n
  END IF
!TVS!
!TVS! ATOVS
!TVS!
!TVS  IF(tvnn > 0) THEN
!TVS    DO n=1,tvnn
!TVS      tmplon=tvslon(ntvs_use_prof(n),ntvs_use_inst(n),ntvs_use_slot(n))
!TVS      tmplat=tvslat(ntvs_use_prof(n),ntvs_use_inst(n),ntvs_use_slot(n))
!TVS      CALL com_distll_1( tmplon, tmplat, lon1(ij), lat1(ij), dist)
!TVS      IF( dist > dist_zero) CYCLE
!TVS
!TVS      DO ichan=1,ntvsch(ntvs_use_inst(n))
!TVS        tmperr=tvserr(ichan,ntvs_use_prof(n),ntvs_use_inst(n),ntvs_use_slot(n))
!TVS        tmpqc=tvsqc(ichan,ntvs_use_prof(n),ntvs_use_inst(n),ntvs_use_slot(n))
!TVS        tmpwgt(:)=tvswgt(:,ichan, &
!TVS                         & ntvs_use_prof(n), &
!TVS                         & ntvs_use_inst(n), &
!TVS                         & ntvs_use_slot(n))
!TVS        IF( tmpqc == 1 .AND. tmpwgt(ilev) > 0.05D0 ) THEN
!TVS          nobsl = nobsl + 1
!TVS          DO im = 1, nbv
!TVS            hdxf(nobsl,im) = tvshdxf(im,ichan, &
!TVS                              & ntvs_use_prof(n), &
!TVS                              & ntvs_use_inst(n), &
!TVS                              & ntvs_use_slot(n))
!TVS          END DO
!TVS          dep(nobsl)    = tvsdep(ichan, &
!TVS                              & ntvs_use_prof(n), &
!TVS                              & ntvs_use_inst(n), &
!TVS                              & ntvs_use_slot(n))
!TVS          rdiag(nobsl)  = tmperr * tmperr &
!TVS                        & * exp(0.5d0 * (dist/sigma_obs)**2) &
!TVS                        & / (tmpwgt(ilev) * tmpwgt(ilev))
!TVS        END IF
!TVS      END DO
!TVS    END DO
!TVS  END IF
!
! DEBUG
! IF( ILEV == 1 .AND. ILON == 1 ) &
! & WRITE(6,*) 'ILEV,ILON,ILAT,NN,TVNN,NOBSL=',ilev,ij,nn,tvnn,nobsl
!
  IF( nobsl > nobstotal ) THEN
    WRITE(6,'(A,I5,A,I5)') 'FATAL ERROR, NOBSL=',nobsl,' > NOBSTOTAL=',nobstotal
    WRITE(6,*) 'IJ,NN,TVNN=', ij, nn, tvnn
    STOP 99
  END IF

  if(num_obs_limit > 0) then
     !observation number limit
     !write(6, *) "nobsl, tmp_nobsl", nobsl, sum(tmp_nobsl)
     tmp_nobsl_max = maxval(tmp_nobsl)
     allocate(tmp_hdxf(nbv, tmp_nobsl_max, nid_obs), tmp_rdiag(tmp_nobsl_max, nid_obs), &
          &   tmp_rloc(tmp_nobsl_max, nid_obs), tmp_dep(tmp_nobsl_max, nid_obs))
     do iobs = 1, nid_obs
        !call system_clock(time1, timerate, timemax) 
        count = 0
        do n = 1, nobsl
           if(iobsl(n) == iobs) then
              count = count + 1
              tmp_hdxf(:, count, iobs) = hdxf(:, n)
              tmp_dep(count, iobs) = dep(n)
              tmp_rdiag(count, iobs) = rdiag(n)
              tmp_rloc(count, iobs) = rloc(n)
           end if
        end do !n
        if(tmp_nobsl(iobs) > num_obs_limit) &
             & call observation_number_limit(tmp_hdxf(:, :, iobs), tmp_dep(:, iobs), &
             &                               tmp_rdiag(:, iobs), tmp_rloc(:, iobs), tmp_nobsl(iobs))
        !call system_clock(time2, timerate, timemax) 
        !write(6, *) "iobs", iobs, (time2 - time1) / dble(timerate)
     end do !iobs

     !call system_clock(time1, timerate, timemax) 
     nobsl = 0
     do iobs = 1, nid_obs
        hdxf(:, (nobsl + 1):(nobsl + tmp_nobsl(iobs))) = tmp_hdxf(:, 1:tmp_nobsl(iobs), iobs)
        dep((nobsl + 1):(nobsl + tmp_nobsl(iobs))) = tmp_dep(1:tmp_nobsl(iobs), iobs)
        rdiag((nobsl + 1):(nobsl + tmp_nobsl(iobs))) = tmp_rdiag(1:tmp_nobsl(iobs), iobs)
        rloc((nobsl + 1):(nobsl + tmp_nobsl(iobs))) = tmp_rloc(1:tmp_nobsl(iobs), iobs)
        nobsl = nobsl + tmp_nobsl(iobs)
     end do !iobs
     !call system_clock(time2, timerate, timemax) 
     !write(6, *) "unpack", (time2 - time1) / dble(timerate)
     deallocate(tmp_hdxf, tmp_rdiag, tmp_rloc, tmp_dep)
  end if
!
  IF(allocated(nobs_use)) DEALLOCATE(nobs_use)
  if(allocated(iobsl)) deallocate(iobsl)
!TVS  IF( ntvs > 0 ) THEN
!TVS    DEALLOCATE(ntvs_use_prof)
!TVS    DEALLOCATE(ntvs_use_inst)
!TVS    DEALLOCATE(ntvs_use_slot)
!TVS  END IF
!
  RETURN
END SUBROUTINE obs_local

!DEC$ ATTRIBUTES FORCEINLINE :: obs_local_sub
SUBROUTINE obs_local_sub(imin,imax,jmin,jmax,nn, nobs_use)
  INTEGER,INTENT(IN) :: imin,imax,jmin,jmax
  INTEGER,INTENT(INOUT) :: nn, nobs_use(nobs)
  INTEGER :: j,n,ib,ie,ip

  DO j=jmin,jmax
    IF(imin > 1) THEN
      ib = nobsgrd(imin-1,j)+1
    ELSE
      IF(j > 1) THEN
        ib = nobsgrd(nlon,j-1)+1
      ELSE
        ib = 1
      END IF
    END IF
    ie = nobsgrd(imax,j)
    n = ie - ib + 1
    IF(n == 0) CYCLE
    IF(nn + n > nobs) THEN
       WRITE(6,*) 'FATALERROR, NN > NOBS', NN + n, NOBS
    END IF
    DO ip=ib,ie
      nobs_use(nn) = ip
      nn = nn + 1
    END DO
  END DO

  RETURN
END SUBROUTINE obs_local_sub

!TVSSUBROUTINE tvs_local_sub(imin,imax,jmin,jmax,nn,ntvs_prof,ntvs_inst,ntvs_slot)
!TVS  INTEGER,INTENT(IN) :: imin,imax,jmin,jmax
!TVS  INTEGER,INTENT(INOUT) :: nn, ntvs_prof(ntvs), ntvs_inst(ntvs), ntvs_slot(ntvs)
!TVS  INTEGER :: j,n,ib,ie,ip
!TVS  INTEGER :: islot, iinst
!TVS
!TVS  DO j=jmin,jmax
!TVS    DO islot=1,nslots
!TVS      DO iinst=1,ninstrument
!TVS        IF(imin > 1) THEN
!TVS          ib = ntvsgrd(imin-1,j,iinst,islot)+1
!TVS        ELSE
!TVS          IF(j > 1) THEN
!TVS            ib = ntvsgrd(nlon,j-1,iinst,islot)+1
!TVS          ELSE
!TVS            ib = 1
!TVS          END IF
!TVS        END IF
!TVS        ie = ntvsgrd(imax,j,iinst,islot)
!TVS        n = ie - ib + 1
!TVS        IF(n == 0) CYCLE
!TVS        DO ip=ib,ie
!TVS          IF(nn > nobs) THEN
!TVS            WRITE(6,*) 'FATALERROR, NN > NTVS', NN, NTVS
!TVS          END IF
!TVS          ntvs_prof(nn)=ip
!TVS          ntvs_inst(nn)=iinst
!TVS          ntvs_slot(nn)=islot
!TVS          nn = nn + 1
!TVS        END DO
!TVS      END DO
!TVS    END DO
!TVS  END DO
!TVS  RETURN
!TVSEND SUBROUTINE tvs_local_sub
!TVS!-----------------------------------------------------------------------
!TVS! Data Assimilation for VARBC
!TVS!-----------------------------------------------------------------------
!TVSSUBROUTINE das_vbc(um,vm,tm,qm,qlm,psm,vbcf,vbca)
!TVS  USE common_mtx
!TVS  IMPLICIT NONE
!TVS  REAL(r_size),INTENT(IN) :: um(nij1,nlev)
!TVS  REAL(r_size),INTENT(IN) :: vm(nij1,nlev)
!TVS  REAL(r_size),INTENT(IN) :: tm(nij1,nlev)
!TVS  REAL(r_size),INTENT(IN) :: qm(nij1,nlev)
!TVS  REAL(r_size),INTENT(IN) :: qlm(nij1,nlev)
!TVS  REAL(r_size),INTENT(IN) :: psm(nij1)
!TVS  REAL(r_size),INTENT(INOUT) :: vbcf(maxvbc,maxtvsch,ninstrument)
!TVS  REAL(r_size),INTENT(OUT)   :: vbca(maxvbc,maxtvsch,ninstrument)
!TVS  REAL(r_sngl) :: u4(nlon,nlat,nlev)
!TVS  REAL(r_sngl) :: v4(nlon,nlat,nlev)
!TVS  REAL(r_sngl) :: t4(nlon,nlat,nlev)
!TVS  REAL(r_sngl) :: q4(nlon,nlat,nlev)
!TVS  REAL(r_sngl) :: ql4(nlon,nlat,nlev)
!TVS  REAL(r_sngl) :: ps4(nlon,nlat)
!TVS  REAL(r_size) :: u(nlon,nlat,nlev)
!TVS  REAL(r_size) :: v(nlon,nlat,nlev)
!TVS  REAL(r_size) :: t(nlon,nlat,nlev)
!TVS  REAL(r_size) :: q(nlon,nlat,nlev)
!TVS  REAL(r_size) :: ql(nlon,nlat,nlev)
!TVS  REAL(r_size) :: ps(nlon,nlat)
!TVS  REAL(r_size) :: p_full(nlon,nlat,nlev)
!TVS  REAL(r_size),ALLOCATABLE :: hx(:,:,:,:)
!TVS  REAL(r_size),ALLOCATABLE :: pred(:,:,:,:,:)
!TVS  INTEGER,ALLOCATABLE :: tmpqc(:,:,:)
!TVS  REAL(r_size),ALLOCATABLE :: tmpwgt(:,:,:,:)
!TVS  REAL(r_size) :: a(maxvbc,maxvbc)
!TVS  REAL(r_size) :: b(maxvbc)
!TVS  REAL(r_size) :: ainv(maxvbc,maxvbc)
!TVS  INTEGER:: ntvschan1(maxtvsch,ninstrument)
!TVS  INTEGER:: i,j,k,n,islot,nn
!TVS
!TVS  PRINT *,'Hello from das_vbc'
!TVS
!TVS  IF(ntvs == 0) THEN
!TVS    PRINT *,'No radiance data: das_vbc skipped..'
!TVS!$OMP PARALLEL WORKSHARE
!TVS    vbca = vbcf
!TVS!$OMP END PARALLEL WORKSHARE
!TVS    RETURN
!TVS  END IF
!TVS
!TVS  CALL gather_grd_mpi(0,um,vm,tm,qm,qlm,psm,u4,v4,t4,q4,ql4,ps4)
!TVS  n = nlon*nlat*nlev
!TVS  CALL MPI_BARRIER(MPI_COMM_WORLD,i)
!TVS  CALL MPI_BCAST(u4(1,1,1),n,MPI_REAL,0,MPI_COMM_WORLD,i)
!TVS  CALL MPI_BCAST(v4(1,1,1),n,MPI_REAL,0,MPI_COMM_WORLD,i)
!TVS  CALL MPI_BCAST(t4(1,1,1),n,MPI_REAL,0,MPI_COMM_WORLD,i)
!TVS  CALL MPI_BCAST(q4(1,1,1),n,MPI_REAL,0,MPI_COMM_WORLD,i)
!TVS  CALL MPI_BCAST(ql4(1,1,1),n,MPI_REAL,0,MPI_COMM_WORLD,i)
!TVS  n = nlon*nlat
!TVS  CALL MPI_BCAST(ps4(1,1),n,MPI_REAL,0,MPI_COMM_WORLD,i)
!TVS!$OMP PARALLEL WORKSHARE
!TVS  u = REAL(u4,r_size)
!TVS  v = REAL(v4,r_size)
!TVS  t = REAL(t4,r_size)
!TVS  q = REAL(q4,r_size)
!TVS  ql = REAL(ql4,r_size)
!TVS  ps = REAL(ps4,r_size)
!TVS!$OMP END PARALLEL WORKSHARE
!TVS  CALL calc_pfull(ps,p_full)
!TVS
!TVS  ALLOCATE( hx(maxtvsch,maxtvsprof,ninstrument,nslots) )
!TVS  ALLOCATE( pred(maxvbc,maxtvsch,maxtvsprof,ninstrument,nslots) )
!TVS  ALLOCATE( tmpqc(maxtvsch,maxtvsprof,ninstrument) )
!TVS  ALLOCATE( tmpwgt(nlev,maxtvsch,maxtvsprof,ninstrument) )
!TVS  DO islot=1,nslots
!TVS!    IF(SUM(ntvsprofslots(:,islot)) == 0) CYCLE
!TVS    ntvsprof(:) = ntvsprofslots(:,islot)
!TVS    CALL Trans_XtoY_tvs(u,v,t,q,ql,ps,p_full, &
!TVS      & tvslon(:,:,islot),tvslat(:,:,islot),tvszenith(:,:,islot),&
!TVS      & tvsskin(:,:,islot),tvsstmp(:,:,islot),tvsclw(:,:,islot),&
!TVS      & tvsemis(:,:,:,islot),tmpqc,hx(:,:,:,islot),tmpwgt,pred(:,:,:,:,islot))
!TVS  END DO
!TVS  DEALLOCATE(tmpqc,tmpwgt)
!TVS
!TVS!$OMP PARALLEL PRIVATE(j,k,n,a,b,ainv)
!TVS!$OMP WORKSHARE
!TVS  vbca = 0.0d0
!TVS!$OMP END WORKSHARE
!TVS!$OMP DO SCHEDULE(DYNAMIC)
!TVS  DO k=1,ninstrument
!TVS    DO j=1,maxtvsch
!TVS      !
!TVS      ! Parallel processing
!TVS      !
!TVS      IF(MOD(j+maxtvsch*(k-1)-1,nprocs) /= myrank) CYCLE
!TVS      !
!TVS      ! DATA NUMBER
!TVS      !
!TVS      ntvschan(j,k) = SUM(tvsqc(j,:,k,:))
!TVS      IF(msw_vbc .AND. ntvschan(j,k) /= 0 ) THEN
!TVS        PRINT '(3A,I3,A,I6)',' >> VBC executed for instrument,channel,ntvsl: ',&
!TVS                            & tvsname(k),',',tvsch(j,k),',',ntvschan(j,k)
!TVS        CALL vbc_local(j,k,ntvschan(j,k),hx,pred,a,b)
!TVS        CALL mtx_inv(maxvbc,a,ainv)
!TVS        vbca(:,j,k) = vbcf(:,j,k)
!TVS        DO n=1,maxvbc
!TVS          vbca(:,j,k) = vbca(:,j,k) - ainv(:,n)*b(n) !ATTN: sign for beta
!TVS        END DO
!TVS      ELSE
!TVS        PRINT '(3A,I3,A,I6)',' !! NO VBC executed for instrument,channel,ntvsl: ',&
!TVS                            & tvsname(k),',',tvsch(j,k),',',ntvschan(j,k)
!TVS        vbca(:,j,k) = vbcf(:,j,k)
!TVS      END IF
!TVS    END DO
!TVS  END DO
!TVS!$OMP END DO
!TVS!$OMP WORKSHARE
!TVS  vbcf = vbca
!TVS  ntvschan1 = ntvschan
!TVS!$OMP END WORKSHARE
!TVS!$OMP END PARALLEL
!TVS  DEALLOCATE(hx,pred)
!TVS  n = maxvbc*maxtvsch*ninstrument
!TVS  CALL MPI_BARRIER(MPI_COMM_WORLD,j)
!TVS  CALL MPI_ALLREDUCE(vbcf,vbca,n,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,j)
!TVS  n = maxtvsch*ninstrument
!TVS  CALL MPI_BARRIER(MPI_COMM_WORLD,j)
!TVS  CALL MPI_ALLREDUCE(ntvschan1,ntvschan,n,MPI_INTEGER,MPI_SUM,MPI_COMM_WORLD,j)
!TVS
!TVS  RETURN
!TVSEND SUBROUTINE das_vbc
!TVS!-----------------------------------------------------------------------
!TVS!  (in) ichan: channnel
!TVS!  (in) iinst: sensor
!TVS!  (out) a = B_beta^-1 + p R^-1 p^T
!TVS!  (out) b = p R^-1 d
!TVS!-----------------------------------------------------------------------
!TVSSUBROUTINE vbc_local(ichan,iinst,ntvsl,hx,pred,a,b)
!TVS  IMPLICIT NONE
!TVS  INTEGER,PARAMETER :: msw=1
!TVS  INTEGER,PARAMETER :: nmin=400
!TVS  INTEGER,INTENT(IN) :: ichan,iinst,ntvsl
!TVS  REAL(r_size),INTENT(IN) :: hx(maxtvsch,maxtvsprof,ninstrument,nslots)
!TVS  REAL(r_size),INTENT(IN) :: pred(maxvbc,maxtvsch,maxtvsprof,ninstrument,nslots)
!TVS  REAL(r_size),INTENT(OUT) :: a(maxvbc,maxvbc)
!TVS  REAL(r_size),INTENT(OUT) :: b(maxvbc)
!TVS  REAL(r_size) :: dep,dep0
!TVS  REAL(r_size) :: bias,bias0
!TVS  REAL(r_size) :: r,tmp
!TVS  INTEGER:: islot, iprof, i,j,n
!TVS
!TVS  a = 0.0d0
!TVS  b = 0.0d0
!TVS  dep = 0.0d0
!TVS  dep0 = 0.0d0
!TVS  bias = 0.0d0
!TVS  bias0 = 0.0d0
!TVS  n = 0
!TVS  DO islot=1,nslots
!TVS    DO iprof=1,maxtvsprof
!TVS      IF(tvsqc(ichan,iprof,iinst,islot)/=1) CYCLE
!TVS      !
!TVS      ! R
!TVS      !
!TVS      r = tvserr(ichan,iprof,iinst,islot)**2
!TVS      !
!TVS      ! p R^-1 p^T
!TVS      !
!TVS      DO j=1,maxvbc
!TVS        DO i=1,maxvbc
!TVS          a(i,j) = a(i,j) &
!TVS               & + pred(i,ichan,iprof,iinst,islot) &
!TVS               & * pred(j,ichan,iprof,iinst,islot) / r
!TVS        END DO
!TVS      END DO
!TVS      !
!TVS      ! B_beta^-1
!TVS      !
!TVS      IF(msw == 1) THEN ! Y.Sato
!TVS        IF(ntvsl < nmin) THEN
!TVS          tmp = REAL(nmin,r_size) / r
!TVS
!TVS        ELSE
!TVS          tmp = (REAL(ntvsl,r_size) &
!TVS            & / (LOG10(REAL(ntvsl,r_size)/REAL(nmin,r_size))+1.0d0)) / r
!TVS        END IF
!TVS      ELSE IF(msw == 2) THEN ! D.Dee
!TVS        tmp = REAL(ntvsl,r_size) / r
!TVS      ELSE ! Constant
!TVS        tmp = 100.0d0
!TVS      END IF
!TVS      DO i=1,maxvbc
!TVS        a(i,i) = a(i,i) + tmp
!TVS      END DO
!TVS      !
!TVS      ! p R^-1 d
!TVS      !
!TVS      b(:) = b(:) + pred(:,ichan,iprof,iinst,islot) / r &
!TVS                & *(tvsdat(ichan,iprof,iinst,islot)-hx(ichan,iprof,iinst,islot))
!TVS      bias = bias+tvsdat(ichan,iprof,iinst,islot)-hx(ichan,iprof,iinst,islot)
!TVS      dep = dep+(tvsdat(ichan,iprof,iinst,islot)-hx(ichan,iprof,iinst,islot))**2
!TVS      bias0= bias0+tvsdep(ichan,iprof,iinst,islot)
!TVS      dep0= dep0+tvsdep(ichan,iprof,iinst,islot)**2
!TVS      n = n+1
!TVS    END DO
!TVS  END DO
!TVS
!TVS  dep = SQRT(dep / REAL(n,r_size))
!TVS  dep0 = SQRT(dep0 / REAL(n,r_size))
!TVS  bias = bias / REAL(n,r_size)
!TVS  bias0 = bias0 / REAL(n,r_size)
!TVS  PRINT '(2A,I3,4F12.4)',' >> D monit: ',tvsname(iinst),tvsch(ichan,iinst),bias0,bias,dep0,dep
!TVS
!TVS  RETURN
!TVSEND SUBROUTINE vbc_local

subroutine observation_number_limit(hdxf, dep, rdiag, rloc, nobsl)
  real(r_size), intent(inout) :: hdxf(nbv, nobsl) !transposed from original code
  real(r_size), intent(inout) :: rdiag(nobsl), rloc(nobsl), dep(nobsl)
  integer, intent(inout) :: nobsl

  real(r_size) :: tmp_hdxf(nbv, num_obs_limit), tmp_rdiag(num_obs_limit), tmp_dep(num_obs_limit)
  integer i, idx(nobsl)

  write(6, *) "Obs Num Limit", nobsl, num_obs_limit

  do i = 1, nobsl
     idx(i) = i
  end do

  call observation_number_limit_sort_fast(rloc, idx, 1, nobsl, num_obs_limit)
  nobsl = num_obs_limit

  do i = 1, nobsl
     tmp_hdxf(:, i) = hdxf(:, idx(i))
     tmp_dep(i) = dep(idx(i))
     tmp_rdiag(i) = rdiag(idx(i))
  end do
  hdxf(:, 1:nobsl) = tmp_hdxf(:, 1:nobsl)
  dep(1:nobsl) = tmp_dep(1:nobsl)
  rdiag(1:nobsl) = tmp_rdiag(1:nobsl)
  return
end subroutine observation_number_limit

subroutine observation_number_limit_sort(rloc, idx, istart, iend)
  real(r_size), intent(inout) :: rloc(nobstotal)
  integer, intent(inout) :: idx(istart:iend)
  integer, intent(in) :: istart, iend

  integer :: i, imax, skip(iend - istart + 1)

  skip(1) = 1
  do i = 2, iend - istart + 1
     skip(i) = 3 * skip(i - 1) + 1
     if(skip(i) > iend - istart + 1) then
        imax = i - 1
        exit
     end if
  end do !i

  do i = imax, 1, -1
     call observation_number_limit_sort_core(rloc, idx, istart, iend, skip(i))
  end do !i

  return
end subroutine observation_number_limit_sort

subroutine observation_number_limit_sort_core(rloc, idx, istart, iend, skip)
  real(r_size), intent(inout) :: rloc(nobstotal)
  integer, intent(inout) :: idx(istart:iend)
  integer, intent(in) :: istart, iend, skip

  real(r_size) :: tmp_rloc
  integer i, j, k, tmp_idx

  do k = 0, skip - 1
     do i = istart + skip + k, iend
        tmp_idx = idx(i)
        tmp_rloc = rloc(i)
        if(rloc(i - skip) < tmp_rloc) then
           do j = i, istart, -skip
              idx(j) = idx(j - skip)
              rloc(j) = rloc(j - skip)
              if(rloc(j - skip) < tmp_rloc) exit
           end do !j
           idx(j - skip) = tmp_idx
           rloc(j - skip) = tmp_rloc
        end if
     end do !i
  end do !k
  return
end subroutine observation_number_limit_sort_core

subroutine observation_number_limit_sort_fast(rloc, idx, istart, iend, limit)
  real(r_size), intent(inout) :: rloc(nobstotal)
  integer, intent(inout) :: idx(istart:iend)
  integer, intent(in) :: istart, iend, limit

  real(r_size) tmp_rloc, pivot
  integer :: i, imax, skip(iend - istart + 1), i0, i1, i00, i11, tmp_idx
  i0 = istart
  i1 = iend
  i00 = i0
  i11 = i1
  do while(i00 < i11)
     !pivot = (rloc(i0) + rloc(i0 + (i1 - i0) / 2) + rloc(i1)) / 3
     if(rloc(i0) > rloc(i0 + (i1 - i0) / 2)) then
        if(rloc(i1) > rloc(i0)) then
           pivot = rloc(i0)
        else if(rloc(i1) > rloc(i0 + (i1 - i0) / 2)) then
           pivot = rloc(i1)
        else
           pivot = rloc(i0 + (i1 - i0) / 2)
        end if
     else if(rloc(i1) > rloc(i0 + (i1 - i0) / 2)) then
        pivot = rloc(i0 + (i1 - i0) / 2)
     else if(rloc(i1) > rloc(i0)) then
        pivot = rloc(i1)
     else
        pivot = rloc(i0)
     end if

     do
        do while(rloc(i0) > pivot)
           i0 = i0 + 1
        end do
        do while(rloc(i1) < pivot)
           i1 = i1 - 1
        end do
        if(i0 >= i1) exit
        tmp_rloc = rloc(i0)
        rloc(i0) = rloc(i1)
        rloc(i1) = tmp_rloc
        tmp_idx = idx(i0)
        idx(i0) = idx(i1)
        idx(i1) = tmp_idx
        i0 = i0 + 1
        i1 = i1 - 1
     end do
     if(i0 == limit) then
        exit
     else if(i0 > limit) then
        i1 = i0 - 1
        i0 = i00
     else
        i0 = i0 + 1
        i1 = i11
     end if
     i00 = i0
     i11 = i1
  end do
  return
end subroutine observation_number_limit_sort_fast

END MODULE letkf_tools
