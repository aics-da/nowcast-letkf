module letkf_nowcast_config
#ifndef COMPILE_OBSOPE
  use common_mpi
  use letkf_tools
  use letkf_obs
#endif
  use common_letkf
  use common_nowcast

  contains
  
    subroutine read_config
      integer :: mpiintbuf(4)
      real(r_size) :: mpirealbuf(2)

      write(6, *) 'hello from read_config'
      open(13, file = "letkf.namelist")
      read(13, nml = common_nowcast_config)
#ifndef COMPILE_OBSOPE
      read(13, nml = letkf_tools_config)
      read(13, nml = letkf_obs_config)
#endif
      read(13, nml = common_letkf_config)
      close(13)
      write(6, *) 'bye from read_config'
    end subroutine read_config
end module letkf_nowcast_config
