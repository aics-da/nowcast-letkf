require "narray"
require "fileutils"
require "yaml"

$qsub_job_ids = []

def qsub(nodes, cpus, nmpi, dir, prg, nowait = false)
  host = `uname -n`
  mpiconf = "select=#{nodes}:ncpus=#{cpus}:mpiprocs=#{nmpi}:ompthreads=#{cpus / nmpi}"
  ompconf = "select=#{nodes}:ncpus=#{cpus}"
  p resource = nmpi > 1 ? mpiconf : ompconf
  if /hakushu/ =~ host
    script = <<"EOF"
#!/bin/sh
#PBS -l #{resource}
. /usr/share/modules/init/sh
module unload pgi-12.10
module load intel-2013.1.117
ulimit -u 200
ulimit -a
umask 0022
  cd #{dir}
  #{prg}
EOF

  elsif /yamazaki/ =~ host
    script = <<"EOF"
#!/bin/sh
#PBS -l nodes=#{nodes}:ppn=#{cpus.to_i / nodes.to_i}
. /usr/share/modules/init/sh
module unload pgi-12.10
module load intel-2013.1.117
module load mpt/2.05

  cd #{dir}
  #{prg}
EOF
  else
    raise "configuration for #{host} not found"
  end

  File.open("run-cycle-#{$$}.sh", "w"){|f| f.puts script}
  puts qsub = `qsub run-cycle-#{$$}.sh`
  raise unless /^([0-9]+)/ =~ qsub
  jobid = $1
  if nowait
    $qsub_job_ids.push jobid
  else
    while /#{jobid}/ =~ `qstat`
      sleep 2
    end
    puts "job #{jobid} finished"
  end
end

def qsub_wait
  waitflag = true
  p $qsub_job_ids
  while true
    sleep 5
    qstat = `qstat`
    $qsub_job_ids.dup.each{|jobid|
      unless /#{jobid}/ =~ qstat
        puts "job #{jobid} finished"
        $qsub_job_ids.delete(jobid)
      end
    }
    p $qsub_job_ids
    break if $qsub_job_ids.length == 0
  end
end

def letkf_anal_to_vector_uvw(indir, fname, outdir)
  File.open(File.join(indir, fname)){|fin|
    size = fin.size
    raise unless size % 3 == 0
    FileUtils.mkdir_p(outdir)
    %w(u v w).each{|var|
      File.open("#{outdir}/vector_#{var}_001.bin", "w"){|fout|
        fout.print(fin.read(size / 3))
      }
    }
  }
end

raise "locked by another run" if File.exist?("run_cycle.lock")
system("touch run_cycle.lock")

begin
  p yml = YAML.load(File.read("run_cycle.yml"))

  st = yml["start"].utc
  et = yml["end"].utc
  input = File.expand_path(yml["input"])
  intv = yml["interval"]

  workdir = yml["workdir"]
  outdir = yml["outdir"]
  confdir = yml["confdir"]
  initdir = yml["initdir"]

  ens_size = yml["ens_size"]

  init_by_param_ens = yml["init_by_param_ens"] || nil

  anald_old = init_by_param_ens ? confdir : initdir

  model = File.expand_path(yml["model"])
  vect2obs = File.expand_path(yml["vect2obs"])
  obsope = File.expand_path(yml["obsope"])
  letkf = File.expand_path(yml["letkf"])

  nodes = yml["nodes"]
  ncpus = yml["ncpus"]
  nmpi = yml["nmpi"] || 1

  x_intv = yml['x_intv'] || 10
  y_intv = yml['y_intv'] || 10

  ### SETUP GRID INFO
  File.open(File.join(confdir, "lon.dat"), "w"){|f|
    f.puts NArray.sfloat(3600).indgen!.mul!(0.1).add!(0.05).to_a.join(", ")
  }
  File.open(File.join(confdir, "lat.dat"), "w"){|f|
    f.puts NArray.sfloat(1200).indgen!.mul!(0.1).add!(-59.95).to_a.join(", ")
  }
  File.open(File.join(confdir, "lev.dat"), "w"){|f|
    f.puts "0.975"
  }

  pwd = Dir.pwd
  while st + intv <= et
    p st

    ### SETUP INPUT FILES FOR ENSEMBLE FORECAST
    p infile1 = st.strftime(input)
    old_infile2cp = File.join(workdir, st.strftime("%Y%m%d%H%M%S"), "input_0002.bin")
    st += intv
    p infile2 = st.strftime(input)
    wkd = File.join(workdir, st.strftime("%Y%m%d%H%M%S"))
    outd = File.join(outdir, st.strftime("%Y%m%d%H%M%S"))
    FileUtils.mkdir_p(wkd)
    FileUtils.mkdir_p(outd)
    infile1cp = File.join(wkd, "input_0001.bin")
    unless File.exist?(infile1cp)
      if File.exist?(old_infile2cp)
        FileUtils.ln_sf(old_infile2cp, infile1cp)
      else
        FileUtils.cp(infile1, infile1cp, :preserve => true)
      end
    end
    infile2cp = File.join(wkd, "input_0002.bin")
    FileUtils.cp(infile2, infile2cp, :preserve => true) unless File.exist?(infile2cp)
    guesd = File.join(outd, "gues")
    fcstd = File.join(outd, "fcst")

    unless File.exist?(guesd)
      ### RUN ENSEMBLE FORECAST
      ### usage: ruby ensfcst.rb workdir indir outdir model enssize infile [infile2] [init_by_param_ens]
      #raise unless system("ruby ensfcst.rb #{wkd} #{anald_old} #{guesd} #{model} #{ens_size} #{infile1cp} #{infile2cp} #{init_by_param_ens}")
      #qsub(nodes, ncpus, 1, Dir.pwd, "ruby ensfcst.rb #{wkd} #{anald_old} #{guesd} #{model} #{ens_size} #{infile1cp} #{infile2cp} #{init_by_param_ens}")
      system("rm -f ensfcst.done")
      qsub(nodes, ncpus, nmpi, Dir.pwd, "ruby ensfcst.rb #{wkd} #{anald_old} #{guesd} #{model} #{ens_size} #{infile1cp} #{infile2cp} #{init_by_param_ens}")
      raise unless File.exist?("ensfcst.done")
      system("rm -f ensfcst.done")
    else
      puts "skip ens fcst"
    end

    ### RUN TREC
    trecd = File.join(wkd, "trec")
    unless File.exist?(trecd) || yml['skip_trec_in_first_cycle']
      puts "start TREC ..."
      FileUtils.mkdir_p(trecd)
      FileUtils.ln_sf(File.join(wkd, "input_0001.bin"), trecd)
      FileUtils.ln_sf(File.join(wkd, "input_0002.bin"), trecd)
      FileUtils.ln_sf(model, trecd)
      FileUtils.cp(File.join(confdir, "setting.namelist.trec"), File.join(trecd, "setting.namelist"))
      Dir.chdir(trecd)
      #raise unless system(File.basename(model))
      qsub(nodes, ncpus, nmpi, Dir.pwd, "mpiexec --hostfile $PBS_NODEFILE --bind-to socket ./#{File.basename(model)}")
    else
      puts "skip TREC"
    end

    ### CREATE OBSERVATION DATA
    ### usage: ruby vect2obs.rb lon.dat lat.dat lev.dat
    letkfd = File.join(wkd, "letkf")
    if yml['skip_trec_in_first_cycle']
      FileUtils.mkdir_p(letkfd)
      raise unless system("touch #{File.join(letkfd, 'obsin.dat')}")
      yml['skip_trec_in_first_cycle'] = false
    else
      Dir.chdir(trecd)
      unless File.exist?(File.join(letkfd, "obsin.dat"))
        raise unless system("ruby #{vect2obs} #{File.join(confdir, 'lon.dat')}  #{File.join(confdir, 'lat.dat')}  #{File.join(confdir, 'lev.dat')} #{x_intv} #{y_intv} #{yml['u_err']} #{yml['v_err']} #{yml['w_err']}")
      else
        puts "skip converting observation data"
      end
    end

    ### RUN OBSOPE
    unless File.exist?(letkfd)
      FileUtils.mkdir_p(letkfd)
      FileUtils.mv(File.join(trecd, "obsin.dat"), letkfd) if File.exist?(File.join(trecd, "obsin.dat"))
      FileUtils.ln_sf(File.join(confdir, "letkf.namelist"), letkfd)
      FileUtils.ln_sf(File.join(confdir, "lon.dat"), letkfd)
      FileUtils.ln_sf(File.join(confdir, "lat.dat"), letkfd)
      FileUtils.ln_sf(File.join(confdir, "lev.dat"), letkfd)
      FileUtils.ln_sf(letkf, letkfd)
      FileUtils.ln_sf(obsope, letkfd)
    end
    Dir.chdir(letkfd)
    p Dir.pwd
    anald = File.join(outd, "anal")
    if File.exist?(infl_mul = File.join(anald_old, "infl_mul.grd"))
      FileUtils.cp(infl_mul, letkfd)
    end
    unless File.exist?(File.join(anald, "anal_me.grd"))
      puts "start OBSOPE and LETKF ..."
      prog = "set -e\n"
      ens_size.times{|i|
        p i + 1
        memid = format('%03d', i + 1)
        gsf = File.join(guesd, "gs01#{memid}.grd")
        raise "#{gsf} not found" unless File.exist?(gsf)
        #FileUtils.ln_sf(gsf, letkfd)
        obsoped = File.join(letkfd, memid)
        #FileUtils.mkdir_p(obsoped)
        prog << <<"EOF"
set -e
mkdir -p #{obsoped}
cd #{obsoped}
ln -sf ../letkf.namelist .
ln -sf ../lon.dat .
ln -sf ../lat.dat .
ln -sf ../lev.dat .
ln -sf ../obsin.dat .
ln -sf #{gsf} ../
ln -sf #{gsf} ./gues.grd
#module load common/valgrind
#/home/otsuka/valgrind/bin/valgrind --leak-check=full --show-leak-kinds=all --max-stackframe=17280000 --main-stacksize=20000000 ../#{File.basename(obsope)} &
../#{File.basename(obsope)} &
cd ..
EOF
      }
      prog << "wait\n"
      ens_size.times{|i|
        memid = format('%03d', i + 1)
        prog << "ln -sf #{memid}/obsout.dat ./obs01#{memid}.dat\n"
      }
      prog << <<"EOF"
sleep 1
# LD_PRELOAD=/home/otsuka/valgrind/lib/valgrind/libmpiwrap-amd64-linux.so mpiexec_mpt -n #{nodes * ncpus} /home/otsuka/valgrind/valgrind --leak-check=full --show-leak-kinds=all --max-stackframe=17280000 --main-stacksize=20000000 ./#{File.basename(letkf)}
#mpiexec_mpt -n #{nodes * ncpus} ./#{File.basename(letkf)}
mpiexec -n #{nodes * ncpus} ./#{File.basename(letkf)}
mkdir -p #{anald}
mv #{letkfd}/anal*.grd #{letkfd}/gues_*.grd #{letkfd}/NOUT* #{anald}
touch #{letkfd}/successful
EOF
      if File.exist?(File.join(letkfd, "infl_mul.grd"))
        prog << "mv -f #{letkfd}/infl_mul.grd #{anald}\n"
      end
      10.times do |conunt_retry|
        qsub(nodes, nodes * ncpus, ncpus, letkfd, prog)
        break if File.exist?("#{letkfd}/successful")
        puts "failed to run LETKF. retry."
      end
      raise "error while executing LETKF" unless File.exist?("#{letkfd}/successful")
    else
      puts "#{anald}/anal_me.grd exists; skip LETKF"
    end

    unless File.exist?(File.join(anald, "vector_w_001.bin"))
      puts "create initial conditions for the next ensemble forecasts"
      ens_size.times{|i|
        ### OUTPUT INITIAL CONDITION FOR NEXT ENSEMBLE FORECAST
        memid = format("%03d", i + 1)
        10.times do |j|
          break if File.exist?("#{anald}/anal#{memid}.grd")
          raise "File #{anald}/anal#{memid}.grd not found" if j == 9
          puts "waiting for #{anald}/anal#{memid}.grd"
          sleep 5
        end
        letkf_anal_to_vector_uvw(anald, "anal#{memid}.grd", "#{anald}/#{i + 1}")
      }
      ### OUTPUT INITIAL CONDITION FOR NEXT DETERMINISTIC FORECAST
      letkf_anal_to_vector_uvw(anald, "anal_me.grd", anald)
    else
      puts "skip creating initial conditions for the next ensemble forecasts"
    end

    if yml["clean_ens_mem_files"] && File.exist?("#{anald}/anal001.grd")
      puts "clean ensemble member files"
      raise unless system("rm -r #{wkd}/[0-9]*/*.bin #{wkd}/letkf/[0-9]*/obsout.dat #{guesd}/* #{anald}/anal[0-9]*.grd")
      if (anald_old != (init_by_param_ens ? confdir : initdir)) and !(/000000$/ =~ File.dirname(anald_old))
        ### keep initial conditions at least once per day
        puts "clean ensemble member files in #{anald_old}"
        raise unless system("rm #{anald_old}/*/vector*_001.bin")
      end
    end

    init_by_param_ens = ""
    anald_old = anald
    FileUtils.cp(File.join(confdir, "setting.namelist"), anald, :preserve => true) unless File.exist?(File.join(anald, "setting.namelist"))

    if yml["run_extended_fcst"] && (!File.exist?(File.join(fcstd, "finish")))
      puts "run extended forecast"
      FileUtils.mkdir_p(fcstd)
      FileUtils.ln_sf(model, fcstd)
      [1, 2].each{|i| FileUtils.ln_sf(File.join(wkd, "input_000#{i}.bin"), fcstd)}
      %w(u v w).each{|dd| FileUtils.ln_sf(File.join(anald, "vector_#{dd}_001.bin"), fcstd)}
      FileUtils.ln_sf(File.join(confdir, "setting.namelist.extended"), File.join(fcstd, "setting.namelist"))
      qsub(1, ncpus, nmpi, fcstd, "mpiexec --hostfile $PBS_NODEFILE --bind-to socket #{model}")
      raise unless system("touch #{fcstd}/finish")
    end

    Dir.chdir(pwd)
  end
ensure
  system("rm -f run_cycle.lock")
end
