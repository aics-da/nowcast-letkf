require "fileutils"
require "numru/gphys"
#require "numru/narray"
require File.join(File.expand_path(File.dirname(__FILE__)), "parse_namelist")
include NumRu

### ensemble forecast of nowcasting system ###
puts "usage: ruby ensfcst.rb workdir indir outdir model enssize infile [infile2] [init_by_param_ens] [no_time_integration] [base_frame_num] [vrfile1] [vrfile2]"
p nodes   = ARGV[0].to_i
p nmpi    = ARGV[1].to_i
p workdir = File.expand_path(ARGV[2] || raise)
p indir   = File.expand_path(ARGV[3] || raise)
p outdir  = File.expand_path(ARGV[4] || raise)
p model   = File.expand_path(ARGV[5] || raise)
p enssize = (ARGV[6] || raise).to_i
p infile  = ARGV[7] || raise
p infile2 = ARGV[8] || infile
p init_by_param_ens = eval(ARGV[9] || "false")
p no_time_integration = eval(ARGV[10] || "false")
p base_frame_num = (ARGV[11] || 21).to_i
p vrfile1 = ARGV[12]
p vrfile2 = ARGV[13]

def create_letkf_input(i, indir, outdir, init_by_param_ens, no_time_integration)
  ### CREATE INPUT FOR LETKF
  puts "create input for LETKF"
  t0 = Time.now
  memname = (i + 1).to_s
  nml = parse_namelist("setting.namelist")
  domconf = nml["domain_config"]
  datshape = [domconf["dx"], domconf["dy"], domconf["dz"]].map!{|m| m.to_i}
  datsize = datshape.reduce(:*) * 4

  File.open(File.join(outdir, "gs01#{format('%03d', i + 1)}.grd"), "w"){|fout|
    if init_by_param_ens
      if File.exist?("vector_u_filter_001.nc")
        fout.print(GPhys::IO.open("vector_u_filter_001.nc", "u").val.to_s)
        fout.print(GPhys::IO.open("vector_v_filter_001.nc", "v").val.to_s)
        fout.print(GPhys::IO.open("vector_w_filter_001.nc", "w").val.to_s)
      else
        fout.print(File.read("vector_u_filter_001.bin"))
        fout.print(File.read("vector_v_filter_001.bin"))
        fout.print(File.read("vector_w_filter_001.bin"))
      end
    elsif no_time_integration
      fout.print(File.read(File.join(indir, memname, "vector_u_001.bin")))
      fout.print(File.read(File.join(indir, memname, "vector_v_001.bin")))
      fout.print(File.read(File.join(indir, memname, "vector_w_001.bin")))
    else
      [["U", "gridx"], ["V", "gridy"], ["W", "gridz"]].each{|var|
        if File.exist?("burgers#{var[0]}_001.nc")
          na = GPhys::IO.open("burgers#{var[0]}_001.nc", var[0].downcase)[false, 1].val
          na = na.to_na!(0) if(na.is_a?(NArrayMiss))
          ### CONVERT PHYSICAL SPACE TO GRID NUMBER SPACE
          na.mul!(nml["input_config"]["input_interval"].to_f / domconf[var[1]].to_f)
          fout.print(na.to_s)
        else
          File.open("burgers#{var[0]}_001.bin"){|fin|
            fin.seek(datsize)
            na = NArray.to_na(fin.read(datsize), NArray::SFLOAT, *datshape)
            ### CONVERT PHYSICAL SPACE TO GRID NUMBER SPACE
            na.mul!(nml["input_config"]["input_interval"].to_f / domconf[var[1]].to_f)
            fout.print(na.to_s)
          }
        end
      }
    end
  }
  puts "#{Time.now - t0} s"
end

if File.exist?(namelist = File.join(indir, "1", "setting.namelist"))
elsif File.exist?(namelist = File.join(indir, "setting.namelist"))
else
  raise "setting.namelist is not found in #{indir}" unless no_time_integration
end
if /ensemble_size\s*=\s*([0-9]*)/ =~ File.read(namelist)
  nml_ens_size = ($1).to_i
else
  nml_ens_size = 1
end
intrinsic_ens_run = (nml_ens_size > 1)

orgdir = Dir.pwd

FileUtils.mkdir_p(outdir)

Dir.chdir(workdir)
enssize.times{|i|
  puts "ens: #{i + 1}"
  pwd = Dir.pwd
  memname = (i + 1).to_s
  FileUtils.mkdir_p(memname)
  Dir.chdir(memname)
  ### MODEL CONFIGURATION
  FileUtils.cp(namelist, ".", :preserve => true)
  if File.exist?(File.join(indir, memname, "radar_qc.namelist"))
    FileUtils.cp(File.join(indir, memname, "radar_qc.namelist"), ".", :preserve => true)
  elsif File.exist?(File.join(indir, "radar_qc.namelist"))
    FileUtils.cp(File.join(indir, "radar_qc.namelist"), ".", :preserve => true)
  end
  if File.exist?(File.join(workdir, "topo_mask.dat"))
    FileUtils.ln_sf(File.join(workdir, "topo_mask.dat"), ".")
  end
  %w(ref_vgrad swam tref).each{|qcparam|
    %w(clutter weather).each{|type|
      txt = "#{qcparam}pdf_#{type}.txt"
      FileUtils.ln_sf(File.join(workdir, txt), ".") if File.exist?(File.join(workdir, txt))
    }
  }
  if init_by_param_ens
    ### SETTING FOR PARAMETER ENSEMBLE
    puts "setup parameter ensemble..."
    nml = File.open("setting.namelist"){|f| f.readlines}
    File.open("setting.namelist", "w"){|fout|
      nml.each{|l|
        case l
        when /use_precomputed_vector =/
          fout.puts "use_precomputed_vector = 0"
        when /calnt =/
          fout.puts "calnt = 1"
        when /frame_num =/
          fout.puts "frame_num = #{base_frame_num + i}"
        when /switching_lanczos =/
          fout.puts "switching_lanczos = 1"
        when /lanczos_window =/
          fout.puts "lanczos_window = #{base_frame_num + i}"
        when /lanczos_critical_wavelength =/
          fout.puts "lanczos_critical_wavelength = #{base_frame_num + i}"
        when /use_data_assimilation =/
          fout.puts "use_data_assimilation = 0"
        else
          fout.puts l
        end
      }
    }
  elsif no_time_integration
    ### NOTHING TO DO
    puts "use input ensemble without time integration"
  else
    ### INITIAL CONDITION
    %w(u v w).each{|var|
      FileUtils.ln_sf(File.join(indir, memname, "vector_#{var}_001.bin"), ".")}
  end
  unless no_time_integration
    FileUtils.ln_sf(infile, "./input_0001.bin")
    FileUtils.ln_sf(infile2, "./input_0002.bin")
    FileUtils.ln_sf(vrfile1, "./vr_0001.bin") if vrfile1 and vrfile1 != "-"
    FileUtils.ln_sf(vrfile2, "./vr_0002.bin") if vrfile2 and vrfile2 != "-"
    unless intrinsic_ens_run
      ### MODEL
      FileUtils.ln_sf(model, ".")

      ### RUN MODEL
      puts "run model"
      t0 = Time.now
      puts `mpiexec -n #{nodes * nmpi} --hostfile $PBS_NODEFILE --bind-to socket ./#{File.basename(model)}`
      puts "#{Time.now - t0} s"
    end
  end
  create_letkf_input(i, indir, outdir, init_by_param_ens, no_time_integration) unless (!no_time_integration and intrinsic_ens_run)

  Dir.chdir(pwd)
}

if !no_time_integration and intrinsic_ens_run
  Dir.chdir(workdir)
  FileUtils.cp(namelist, ".", :preserve => true)
  ### MODEL
  FileUtils.ln_sf(model, ".")

  ### RUN MODEL
  puts "run model"
  t0 = Time.now
  raise unless system("mpiexec -n #{nodes * nmpi} --hostfile $PBS_NODEFILE --bind-to socket ./#{File.basename(model)}")
  puts "#{Time.now - t0} s"

  enssize.times{|i|
    memname = (i + 1).to_s
    Dir.chdir(memname)
    create_letkf_input(i, indir, outdir, init_by_param_ens, no_time_integration)
    Dir.chdir(workdir)
  }
end

File.open(File.join(orgdir, "ensfcst.done"), "w"){|f| f.print "done"}
