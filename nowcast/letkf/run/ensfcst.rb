require "fileutils"
require "narray"
require File.join(File.expand_path(File.dirname(__FILE__)), "parse_namelist")

### ensemble forecast of nowcasting system ###
puts "usage: ruby ensfcst.rb workdir indir outdir model enssize infile [infile2] [init_by_param_ens]"
p workdir = File.expand_path(ARGV[0] || raise)
p indir   = File.expand_path(ARGV[1] || raise)
p outdir  = File.expand_path(ARGV[2] || raise)
p model   = File.expand_path(ARGV[3] || raise)
p enssize = (ARGV[4] || raise).to_i
p infile  = ARGV[5] || raise
p infile2 = ARGV[6] || infile
p init_by_param_ens = ARGV[7] || false

FileUtils.mkdir_p(outdir)

Dir.chdir(workdir)
enssize.times{|i|
  puts "ens: #{i + 1}"
  pwd = Dir.pwd
  memname = (i + 1).to_s
  FileUtils.mkdir_p(memname)
  Dir.chdir(memname)
  ### MODEL CONFIGURATION
  if File.exist?(File.join(indir, memname, "setting.namelist"))
    FileUtils.cp(File.join(indir, memname, "setting.namelist"), ".", :preserve => true)
  elsif File.exist?(File.join(indir, "setting.namelist"))
    FileUtils.cp(File.join(indir, "setting.namelist"), ".", :preserve => true)
  else
    raise "setting.namelist is not found in #{indir}"
  end
  if init_by_param_ens
    ### SETTING FOR PARAMETER ENSEMBLE
    puts "setup parameter ensemble..."
    nml = File.open("setting.namelist"){|f| f.readlines}
    File.open("setting.namelist", "w"){|fout|
      nml.each{|l|
        case l
        when /use_precomputed_vector =/
          fout.puts "use_precomputed_vector = 0"
        when /calnt =/
          fout.puts "calnt = 1"
        when /frame_num =/
          fout.puts "frame_num = #{21 + i}"
        when /switching_lanczos =/
          fout.puts "switching_lanczos = 1"
        when /lanczos_window =/
          fout.puts "lanczos_window = #{21 + i}"
        when /lanczos_critical_wavelength =/
          fout.puts "lanczos_critical_wavelength = #{21 + i}"
        when /use_data_assimilation =/
          fout.puts "use_data_assimilation = 0"
        else
          fout.puts l
        end
      }
    }
  else
    ### INITIAL CONDITION
    %w(u v w).each{|var|
      FileUtils.ln_sf(File.join(indir, memname, "vector_#{var}_001.bin"), ".")}
  end
  FileUtils.ln_sf(infile, "./input_0001.bin")
  FileUtils.ln_sf(infile2, "./input_0002.bin")
  ### MODEL
  FileUtils.ln_sf(model, ".")

  ### RUN MODEL
  puts `./#{File.basename(model)}`

  ### CREATE INPUT FOR LETKF
  nml = parse_namelist("setting.namelist")
  domconf = nml["domain_config"]
  datshape = [domconf["dx"], domconf["dy"], domconf["dz"]].map!{|m| m.to_i}
  datsize = datshape.reduce(:*) * 4

  File.open(File.join(outdir, "gs01#{format('%03d', i + 1)}.grd"), "w"){|fout|
    if init_by_param_ens
      fout.print(File.read("vector_u_filter_001.bin"))
      fout.print(File.read("vector_v_filter_001.bin"))
      fout.print(File.read("vector_w_filter_001.bin"))
    else
      [["U", "gridx"], ["V", "gridy"], ["W", "gridz"]].each{|var|
        File.open("burgers#{var[0]}_001.bin"){|fin|
          fin.seek(datsize)
          na = NArray.to_na(fin.read(datsize), NArray::SFLOAT, *datshape)
          ### CONVERT PHYSICAL SPACE TO GRID NUMBER SPACE
          na.mul!(nml["input_config"]["input_interval"].to_f / domconf[var[1]].to_f)
          fout.print(na.to_s)
        }
      }
    end
  }

  Dir.chdir(pwd)
}
