require "narray"
require "fileutils"
require "yaml"

$qsub_job_ids = []

def qsub(nodes, cpus, nmpi, dir, prg, nowait = false)
  host = `uname -n`
  if /hakushu/ =~ host
    script = <<"EOF"
#!/bin/sh
#PBS -l select=#{nodes}:ncpus=#{cpus}:mpiprocs=#{nmpi}:ompthreads=#{cpus / nmpi}
. /usr/share/modules/init/sh
module unload pgi-12.10
module load intel/2017.1.132
ulimit -u 200
ulimit -a
umask 0022
  cd #{dir}
  #{prg}
EOF

  elsif /yamazaki/ =~ host
    script = <<"EOF"
#!/bin/sh
#PBS -l nodes=#{nodes}:ppn=#{cpus}
. /usr/share/modules/init/sh
module unload pgi-12.10
module load intel-2013.1.117
module load mpt/2.05

  cd #{dir}
  #{prg}
EOF

  elsif /bookers/ =~ host
    # DO NOTHING
  else
    raise "configuration for #{host} not found"
  end

  if /bookers/ =~ host
    pwd = Dir.pwd
    raise unless Dir.chdir(dir)
    prg = prg.gsub("--hostfile $PBS_NODEFILE", "")
    prg = "export OMP_NUM_THREADS=#{cpus / nmpi}\n" << prg
    puts prg
    raise unless system(prg)
    Dir.chdir(pwd)
  else
    File.open("run-cycle-#{$$}.sh", "w"){|f| f.puts script}
    puts qsub = `qsub run-cycle-#{$$}.sh`
    raise unless /^([0-9]+)/ =~ qsub
    jobid = $1
    if nowait
      $qsub_job_ids.push jobid
    else
      while /#{jobid}/ =~ `qstat`
        sleep 2
      end
      puts "job #{jobid} finished"
    end
  end
end

def qsub_wait
  waitflag = true
  p $qsub_job_ids
  while true
    sleep 5
    qstat = `qstat`
    $qsub_job_ids.dup.each{|jobid|
      unless /#{jobid}/ =~ qstat
        puts "job #{jobid} finished"
        $qsub_job_ids.delete(jobid)
      end
    }
    p $qsub_job_ids
    break if $qsub_job_ids.length == 0
  end
end

def letkf_anal_to_vector_uvw(indir, fname, outdir)
  File.open(File.join(indir, fname)){|fin|
    size = fin.size
    raise unless size % 3 == 0
    FileUtils.mkdir_p(outdir)
    %w(u v w).each{|var|
      File.open("#{outdir}/vector_#{var}_001.bin", "w"){|fout|
        fout.print(fin.read(size / 3))
      }
    }
  }
end

raise "locked by another run" if File.exist?("run_cycle.lock")
system("touch run_cycle.lock")

begin
  p yml = YAML.load(File.read("run_cycle.yml"))

  st = yml["start"].utc
  et = yml["end"].utc
  input = File.expand_path(yml["input"])
  input_vr = File.expand_path(yml["input_vr"]) if yml["input_vr"]
  intv = yml["interval"]

  workdir = yml["workdir"]
  outdir = yml["outdir"]
  confdir = yml["confdir"]
  initdir = yml["initdir"]

  ens_size = yml["ens_size"]

  init_by_param_ens = yml["init_by_param_ens"]
  init_by_param_ens = false if init_by_param_ens.nil?

  no_initial_time_integration = yml["no_initial_time_integration"]
  no_initial_time_integration = false if no_initial_time_integration.nil?

  base_frame_num = yml["base_frame_num"]
  raise "base_frame_num is not provided" if init_by_param_ens and !base_frame_num

  trecd_old = init_by_param_ens ? confdir : initdir
  anald_old = init_by_param_ens ? confdir : initdir

  model = File.expand_path(yml["model"])
  vect2obs = File.expand_path(yml["vect2obs"])
  obsope = File.expand_path(yml["obsope"])
  letkf = File.expand_path(yml["letkf"])

  nodes = yml["nodes"] || raise("parameter nodes is not specified")
  ncpus = yml["ncpus_per_node"] || raise("parameter ncpus_per_node is not specified")
  nmpi  = yml["nmpi_per_node"] || raise("parameter nmpi_per_node is not specified")

  x_intv = yml['x_intv'] || 10
  y_intv = yml['y_intv'] || 10
  z_intv = yml['z_intv'] || 10

  ### SETUP GRID INFO
  nx = yml['nx'] || raise
  ny = yml['ny'] || raise
  nz = yml['nz'] || raise
  dx = yml['dx'] || raise
  dy = yml['dy'] || raise
  dz = yml['dz'] || raise
  x0 = yml['x0'] || raise
  y0 = yml['y0'] || raise
  z0 = yml['z0'] || raise
  File.open(File.join(confdir, "lon.dat"), "w"){|f|
    f.puts NArray.sfloat(nx).indgen!.mul!(dx).add!(x0).to_a.join(", ")
  }
  File.open(File.join(confdir, "lat.dat"), "w"){|f|
    f.puts NArray.sfloat(ny).indgen!.mul!(dy).add!(y0).to_a.join(", ")
  }
  File.open(File.join(confdir, "lev.dat"), "w"){|f|
    f.puts NArray.sfloat(nz).indgen!.mul!(dz).add!(z0).to_a.join(", ")
  }
  ### FOR PAWR QC
  qcnml = "radar_qc.namelist"

  pwd = Dir.pwd
  while st + intv <= et
    p st

    ### SETUP INPUT FILES FOR ENSEMBLE FORECAST
    p infile1 = st.strftime(input)
    p vrfile1 = st.strftime(input_vr) if input_vr
    old_infile2cp = File.join(workdir, st.strftime("%Y%m/%d%H%M%S"), "input_0002.bin")
    old_vrfile2cp = File.join(workdir, st.strftime("%Y%m/%d%H%M%S"), "vr_0002.bin") if input_vr
    st += intv
    p infile2 = st.strftime(input)
    p vrfile2 = st.strftime(input_vr) if input_vr
    wkd = File.join(workdir, st.strftime("%Y%m/%d%H%M%S"))
    outd = File.join(outdir, st.strftime("%Y%m/%d%H%M%S"))
    FileUtils.mkdir_p(wkd)
    FileUtils.mkdir_p(outd)
    infile1cp = File.join(wkd, "input_0001.bin")
    vrfile1cp = input_vr ? File.join(wkd, "vr_0001.bin") : "-"
    unless File.exist?(infile1cp)
      if File.exist?(old_infile2cp)
        FileUtils.ln_sf(old_infile2cp, infile1cp)
        FileUtils.ln_sf(old_vrfile2cp, vrfile1cp) if input_vr
      else
        FileUtils.cp(infile1, infile1cp, :preserve => true)
        FileUtils.cp(vrfile1, vrfile1cp, :preserve => true) if input_vr
      end
    end
    infile2cp = File.join(wkd, "input_0002.bin")
    vrfile2cp = input_vr ? File.join(wkd, "vr_0002.bin") : "-"
    FileUtils.cp(infile2, infile2cp, :preserve => true) unless File.exist?(infile2cp)
    FileUtils.cp(vrfile2, vrfile2cp, :preserve => true) if input_vr and !File.exist?(vrfile2cp)
    guesd = File.join(outd, "gues")
    fcstd = File.join(outd, "fcst")
    if(File.exist?(File.join(confdir, "topo_mask.dat")) and !File.exist?(File.join(wkd, "topo_mask.dat")))
      FileUtils.ln_s(File.join(confdir, "topo_mask.dat"), wkd)
    end
    %w(ref_vgrad swam tref).each{|qcparam|
      %w(clutter weather).each{|type|
        txt = "#{qcparam}pdf_#{type}.txt"
        FileUtils.ln_sf(File.join(confdir, txt), wkd) if File.exist?(File.join(confdir, txt))
      }
    }

    unless File.exist?(guesd)
      ### RUN ENSEMBLE FORECAST
      ### usage: ruby ensfcst.rb workdir indir outdir model enssize infile [infile2] [init_by_param_ens]
      system("rm -f ensfcst.done")
      qsub(nodes, ncpus, nmpi, Dir.pwd, <<EOC)
echo "localhost" > nodefile && PBS_NODEFILE=#{File.join(Dir.pwd, 'nodefile')} \
ruby ensfcst-3D.rb #{nodes} #{nmpi} \
                   #{wkd} #{anald_old} #{guesd} #{model} #{ens_size} #{infile1cp} #{infile2cp} \
                   #{init_by_param_ens} #{no_initial_time_integration} #{base_frame_num} \
                   #{vrfile1cp} #{vrfile2cp}
EOC
      raise("error while running ensemble forecasts") unless File.exist?("ensfcst.done")
      system("rm -f ensfcst.done")
    else
      puts "skip ens fcst"
    end
    no_initial_time_integration = false

    ### RUN TREC
    trecd = File.join(wkd, "trec")
    unless File.exist?(trecd) || yml['skip_trec_in_first_cycle'] || yml['ignore_obs']
      puts "start TREC ..."
      FileUtils.mkdir_p(trecd)
      FileUtils.ln_sf(File.join(wkd, "input_0001.bin"), trecd)
      FileUtils.ln_sf(File.join(wkd, "input_0002.bin"), trecd)
      FileUtils.ln_sf(File.join(wkd, "vr_0001.bin"), trecd) if input_vr
      FileUtils.ln_sf(File.join(wkd, "vr_0002.bin"), trecd) if input_vr
      FileUtils.ln_sf(model, trecd)
      FileUtils.cp(File.join(confdir, "setting.namelist.trec"), File.join(trecd, "setting.namelist"))
      FileUtils.cp(File.join(confdir, "radar_qc.namelist"), trecd) if File.exist?(File.join(confdir, "radar_qc.namelist"))
      if(File.exist?(File.join(confdir, "topo_mask.dat")))
        FileUtils.ln_sf(File.join(confdir, "topo_mask.dat"), trecd)
      end
      if(File.exist?(File.join(trecd_old, "mean_vect_history.bin")))
        FileUtils.cp(File.join(trecd_old, "mean_vect_history.bin"), trecd)
      end
      %w(ref_vgrad swam tref).each{|qcparam|
        %w(clutter weather).each{|type|
          txt = "#{qcparam}pdf_#{type}.txt"
          FileUtils.ln_sf(File.join(confdir, txt), trecd) if File.exist?(File.join(confdir, txt))
        }
      }
      Dir.chdir(trecd)
      #raise unless system(File.basename(model))
      if nmpi == 1 ### ASSUME NON-MPI VERSION
        qsub(nodes, ncpus, nmpi, Dir.pwd, "./#{File.basename(model)} && touch trec.done")
      else
        qsub(nodes, ncpus, nmpi, Dir.pwd, "mpiexec -n #{nodes * nmpi} --hostfile $PBS_NODEFILE --bind-to socket ./#{File.basename(model)} && touch trec.done")
      end
      raise "TREC terminated abnormally" unless File.exist?(File.join(trecd, "trec.done"))
    else
      puts "skip TREC"
    end
    trecd_old = trecd

    ### CREATE OBSERVATION DATA
    ### usage: ruby vect2obs.rb lon.dat lat.dat lev.dat
    letkfd = File.join(wkd, "letkf")
    if yml['skip_trec_in_first_cycle'] || yml['ignore_obs']
      FileUtils.mkdir_p(letkfd)
      raise unless system("touch #{File.join(letkfd, 'obsin.dat')}")
      yml['skip_trec_in_first_cycle'] = false
    else
      Dir.chdir(trecd)
      unless File.exist?(File.join(letkfd, "obsin.dat"))
        raise unless system("ruby #{vect2obs} #{File.join(confdir, 'lon.dat')}  #{File.join(confdir, 'lat.dat')}  #{File.join(confdir, 'lev.dat')} #{x_intv} #{y_intv} #{z_intv} #{yml['u_err']} #{yml['v_err']} #{yml['w_err']} #{nz > 1}")
      else
        puts "skip converting observation data"
      end
    end

    ### RUN OBSOPE
    unless File.exist?(File.join(letkfd, "letkf.namelist"))
      FileUtils.mkdir_p(letkfd)
      FileUtils.mv(File.join(trecd, "obsin.dat"), letkfd) if File.exist?(File.join(trecd, "obsin.dat"))
      FileUtils.ln_sf(File.join(confdir, "letkf.namelist"), letkfd)
      FileUtils.ln_sf(File.join(confdir, "lon.dat"), letkfd)
      FileUtils.ln_sf(File.join(confdir, "lat.dat"), letkfd)
      FileUtils.ln_sf(File.join(confdir, "lev.dat"), letkfd)
      FileUtils.ln_sf(letkf, letkfd)
      FileUtils.ln_sf(obsope, letkfd)
    end
    Dir.chdir(letkfd)
    p Dir.pwd
    anald = File.join(outd, "anal")
    if File.exist?(infl_mul = File.join(anald_old, "infl_mul.grd"))
      FileUtils.cp(infl_mul, letkfd)
    end
    unless File.exist?(File.join(anald, "anal_me.grd"))
      puts "start OBSOPE and LETKF ..."
      prog = "set -e\n"
      ens_size.times{|i|
        p i + 1
        memid = format('%03d', i + 1)
        gsf = File.join(guesd, "gs01#{memid}.grd")
        raise "#{gsf} not found" unless File.exist?(gsf)
        #FileUtils.ln_sf(gsf, letkfd)
        obsoped = File.join(letkfd, memid)
        #FileUtils.mkdir_p(obsoped)
        prog << <<"EOF"
set -e
mkdir -p #{obsoped}
cd #{obsoped}
ln -sf ../letkf.namelist .
ln -sf ../lon.dat .
ln -sf ../lat.dat .
ln -sf ../lev.dat .
ln -sf ../obsin.dat .
ln -sf #{gsf} ../
ln -sf #{gsf} ./gues.grd
../#{File.basename(obsope)} &
cd ..
EOF
      }
      prog << "wait\n"
      ens_size.times{|i|
        memid = format('%03d', i + 1)
        prog << "ln -sf #{memid}/obsout.dat ./obs01#{memid}.dat\n"
      }
      prog << <<"EOF"
sleep 1
mpiexec -n #{nodes * ncpus} --hostfile $PBS_NODEFILE --bind-to socket ./#{File.basename(letkf)}
mkdir -p #{anald}
mv #{letkfd}/anal*.grd #{letkfd}/gues_*.grd #{letkfd}/NOUT* #{anald}
touch #{letkfd}/successful
EOF
      if File.exist?(File.join(letkfd, "infl_mul.grd"))
        prog << "mv -f #{letkfd}/infl_mul.grd #{anald}\n"
      end
      10.times do |conunt_retry|
        qsub(nodes, ncpus, ncpus, letkfd, prog) # nmpi = ncpus for LETKF
        break if File.exist?("#{letkfd}/successful")
        puts "failed to run LETKF. retry."
      end
      raise "error while executing LETKF" unless File.exist?("#{letkfd}/successful")
    else
      puts "#{anald}/anal_me.grd exists; skip LETKF"
    end

    unless File.exist?(File.join(anald, "vector_w_001.bin"))
      puts "create initial conditions for the next ensemble forecasts"
      ens_size.times{|i|
        ### OUTPUT INITIAL CONDITION FOR NEXT ENSEMBLE FORECAST
        memid = format("%03d", i + 1)
        10.times do |j|
          break if File.exist?("#{anald}/anal#{memid}.grd")
          raise "File #{anald}/anal#{memid}.grd not found" if j == 9
          puts "waiting for #{anald}/anal#{memid}.grd"
          sleep 5
        end
        letkf_anal_to_vector_uvw(anald, "anal#{memid}.grd", "#{anald}/#{i + 1}")
      }
      ### OUTPUT INITIAL CONDITION FOR NEXT DETERMINISTIC FORECAST
      letkf_anal_to_vector_uvw(anald, "anal_me.grd", anald)
    else
      puts "skip creating initial conditions for the next ensemble forecasts"
    end

    if yml["clean_ens_mem_files"] && File.exist?("#{anald}/anal001.grd")
      puts "clean ensemble member files"
      raise unless system("rm -rf #{wkd}/[0-9]*/*.{bin,nc} #{wkd}/letkf/[0-9]*/obsout.dat #{guesd}/* #{anald}/anal[0-9]*.grd")
      if (anald_old != (init_by_param_ens ? confdir : initdir)) and !(/000000$/ =~ File.dirname(anald_old))
        ### keep initial conditions at least once per day
        puts "clean ensemble member files in #{anald_old}"
        raise unless system("rm -f #{anald_old}/*/vector*_001.bin")
      end
    end

    init_by_param_ens = false unless yml["always_init_by_param_ens"]
    anald_old = anald
    FileUtils.cp(File.join(confdir, "setting.namelist"), anald, :preserve => true) unless File.exist?(File.join(anald, "setting.namelist"))
    FileUtils.cp(File.join(confdir, qcnml), anald, :preserve => true) if File.exist?(File.join(confdir, qcnml)) and !File.exist?(File.join(anald, qcnml))
    if yml["run_extended_fcst"] && (!File.exist?(File.join(fcstd, "finish")))
      puts "run extended forecast"
      FileUtils.mkdir_p(fcstd)
      FileUtils.ln_sf(model, fcstd)
      [1, 2].each{|i| FileUtils.ln_sf(File.join(wkd, "input_000#{i}.bin"), fcstd)}
      [1, 2].each{|i| FileUtils.ln_sf(File.join(wkd, "vr_000#{i}.bin"), fcstd)} if input_vr
      %w(u v w).each{|dd| FileUtils.ln_sf(File.join(anald, "vector_#{dd}_001.bin"), fcstd)}
      FileUtils.ln_sf(File.join(confdir, "setting.namelist.extended"), File.join(fcstd, "setting.namelist"))
      FileUtils.ln_sf(File.join(confdir, "radar_qc.namelist"), fcstd) if input_vr
      if(File.exist?(File.join(confdir, "topo_mask.dat")))
        FileUtils.ln_sf(File.join(confdir, "topo_mask.dat"), fcstd)
      end
      %w(ref_vgrad swam tref).each{|qcparam|
        %w(clutter weather).each{|type|
          txt = "#{qcparam}pdf_#{type}.txt"
          FileUtils.ln_sf(File.join(confdir, txt), fcstd) if File.exist?(File.join(confdir, txt))
        }
      }
      Dir.chdir(fcstd)
      if nmpi == 1 ### ASSUME NON-MPI VERSION
        prog = "#{model}\n"
      else
        prog = "mpiexec -n #{nodes * nmpi} --hostfile $PBS_NODEFILE --bind-to socket #{model}\n"
      end
      prog << "touch #{fcstd}/finish\n"
      qsub(nodes, ncpus, nmpi, fcstd, prog)
      raise("extended forecast terminated abnormally") unless File.exist?(File.join(fcstd, "finish"))
    end

    Dir.chdir(pwd)
  end
ensure
  system("rm -f run_cycle.lock")
end
