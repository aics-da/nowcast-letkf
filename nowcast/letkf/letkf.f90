PROGRAM letkf
!=======================================================================
!
! [PURPOSE:] Main program of LETKF
!
! [HISTORY:]
!   01/16/2009 Takemasa Miyoshi  created
!
!=======================================================================
  !$USE OMP_LIB
  use mpi_f08
  USE common
  USE common_mpi
  USE common_nowcast
  USE common_mpi_nowcast
  USE common_letkf
  use common_mtx
  USE letkf_obs
  USE letkf_tools
  USE letkf_nowcast_config

  IMPLICIT NONE
  REAL(r_size),ALLOCATABLE :: gues3d(:,:,:,:)
  REAL(r_size),ALLOCATABLE :: gues2d(:,:,:)
  REAL(r_size),ALLOCATABLE :: anal3d(:,:,:,:)
  REAL(r_size),ALLOCATABLE :: anal2d(:,:,:)
  INTEGER :: ierr
  CHARACTER(8) :: stdoutf='NOUT-000'
  CHARACTER(4) :: guesf='gs00'
  integer time0, time1, time2, timerate, timemax
!-----------------------------------------------------------------------
! Initial settings
!-----------------------------------------------------------------------
  call system_clock(time0)
  CALL initialize_mpi
  WRITE(stdoutf(6:8), '(I3.3)') myrank
  WRITE(6,'(3A,I3.3)') 'STDOUT goes to ',stdoutf,' for MYRANK ', myrank
  OPEN(6,FILE=stdoutf)
  WRITE(6,'(A,I3.3,2A)') 'MYRANK=',myrank,', STDOUTF=',stdoutf
!
  call system_clock(time2, timerate, timemax)
  WRITE(6,'(A,2F10.2)') '### TIMER(initialize_mpi):', (time2 - time0) / dble(timerate), (time2 - time0) / dble(timerate) 
  time1 = time2
!
  call read_config
  call system_clock(time2, timerate, timemax)
  WRITE(6,'(A,2F10.2)') '### TIMER(read_config):', (time2 - time0) / dble(timerate), (time2 - time1) / dble(timerate) 
  time1 = time2
!
  WRITE(6,'(A)') '============================================='
  WRITE(6,'(A)') '  LOCAL ENSEMBLE TRANSFORM KALMAN FILTERING  '
  WRITE(6,'(A)') '                                             '
  WRITE(6,'(A)') '   LL      EEEEEE  TTTTTT  KK  KK  FFFFFF    '
  WRITE(6,'(A)') '   LL      EE        TT    KK KK   FF        '
  WRITE(6,'(A)') '   LL      EEEEE     TT    KKK     FFFFF     '
  WRITE(6,'(A)') '   LL      EE        TT    KK KK   FF        '
  WRITE(6,'(A)') '   LLLLLL  EEEEEE    TT    KK  KK  FF        '
  WRITE(6,'(A)') '                                             '
  WRITE(6,'(A)') '             WITHOUT LOCAL PATCH             '
  WRITE(6,'(A)') '                                             '
  WRITE(6,'(A)') '          Coded by Takemasa Miyoshi          '
  WRITE(6,'(A)') '  Based on Ott et al (2004) and Hunt (2005)  '
  WRITE(6,'(A)') '  Tested by Miyoshi and Yamane (2006)        '
  WRITE(6,'(A)') '============================================='
  WRITE(6,'(A)') '              LETKF PARAMETERS               '
  WRITE(6,'(A)') ' ------------------------------------------- '
  WRITE(6,'(A,I15)')   '   nbv        :',nbv
  WRITE(6,'(A,I15)')   '   nslots     :',nslots
  WRITE(6,'(A,I15)')   '   nbslot     :',nbslot
  WRITE(6,'(A,F15.2)') '   sigma_obs  :',sigma_obs
  WRITE(6,'(A,F15.2)') '   sigma_obsv :',sigma_obsv
  WRITE(6,'(A,F15.2)') '   sigma_obst :',sigma_obst
  WRITE(6,'(A)') '============================================='
  CALL set_common_nowcast
  call system_clock(time2, timerate, timemax)
  WRITE(6,'(A,2F10.2)') '### TIMER(set_common_nowcast):', (time2 - time0) / dble(timerate), (time2 - time1) / dble(timerate) 
  time1 = time2
!
  CALL set_common_mpi_nowcast
  call system_clock(time2, timerate, timemax)
  WRITE(6,'(A,2F10.2)') '### TIMER(set_common_mpi_nowcast):', (time2 - time0) / dble(timerate), (time2 - time1) / dble(timerate) 
  time1 = time2
!
  ALLOCATE(gues3d(nij1,nlev,nbv,nv3d))
  ALLOCATE(gues2d(nij1,nbv,nv2d))
  ALLOCATE(anal3d(nij1,nlev,nbv,nv3d))
  ALLOCATE(anal2d(nij1,nbv,nv2d))
  call system_clock(time2, timerate, timemax)
  WRITE(6,'(A,2F10.2)') '### TIMER(allocate):', (time2 - time0) / dble(timerate), (time2 - time1) / dble(timerate) 
  time1 = time2
!-----------------------------------------------------------------------
! Observations
!-----------------------------------------------------------------------
  !
  ! CONVENTIONAL OBS
  !
  CALL set_letkf_obs
!

  call system_clock(time2, timerate, timemax)
  WRITE(6,'(A,2F10.2)') '### TIMER(set_letkf_obs):', (time2 - time0) / dble(timerate), (time2 - time1) / dble(timerate) 
  time1 = time2
!-----------------------------------------------------------------------
! First guess ensemble
!-----------------------------------------------------------------------
  !
  ! READ GUES
  !
  CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)
  WRITE(guesf(3:4),'(I2.2)') nbslot
  CALL read_ens_mpi(guesf,nbv,gues3d,gues2d)
!
  call system_clock(time2, timerate, timemax)
  WRITE(6,'(A,2F10.2)') '### TIMER(read_ens_mpi):', (time2 - time0) / dble(timerate), (time2 - time1) / dble(timerate) 
  time1 = time2
  !
  ! WRITE ENS MEAN and SPRD
  !
  CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)
  CALL write_ensmspr_mpi('gues',nbv,gues3d,gues2d)
!
  call system_clock(time2, timerate, timemax)
  WRITE(6,'(A,2F10.2)') '### TIMER(write_ensmspr_mpi(gues)):', (time2 - time0) / dble(timerate), (time2 - time1) / dble(timerate) 
  time1 = time2
!-----------------------------------------------------------------------
! Data Assimilation
!-----------------------------------------------------------------------
  !
  ! LETKF
  !
  CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)
  CALL das_letkf(gues3d,gues2d,anal3d,anal2d)
!
  call system_clock(time2, timerate, timemax)
  WRITE(6,'(A,2F10.2)') '### TIMER(das_letkf):', (time2 - time0) / dble(timerate), (time2 - time1) / dble(timerate) 
  time1 = time2
!-----------------------------------------------------------------------
! Analysis ensemble
!-----------------------------------------------------------------------
  !
  ! WRITE ANAL
  !
  CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)
  CALL write_ens_mpi('anal',nbv,anal3d,anal2d)
!
  call system_clock(time2, timerate, timemax)
  WRITE(6,'(A,2F10.2)') '### TIMER(write_ens_mpi(anal)):', (time2 - time0) / dble(timerate), (time2 - time1) / dble(timerate) 
  time1 = time2
  !
  ! WRITE ENS MEAN and SPRD
  !
  CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)
  CALL write_ensmspr_mpi('anal',nbv,anal3d,anal2d)
!
  call system_clock(time2, timerate, timemax)
  WRITE(6,'(A,2F10.2)') '### TIMER(write_ensmspr_mpi(anal)):', (time2 - time0) / dble(timerate), (time2 - time1) / dble(timerate) 
  time1 = time2
!-----------------------------------------------------------------------
! Monitor
!-----------------------------------------------------------------------
  CALL monit_mean('gues')
  CALL monit_mean('anal')
!
  call system_clock(time2, timerate, timemax)
  WRITE(6,'(A,2F10.2)') '### TIMER(monit_mean):', (time2 - time0) / dble(timerate), (time2 - time1) / dble(timerate) 
  time1 = time2
!-----------------------------------------------------------------------
! Finalize
!-----------------------------------------------------------------------
  CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)
  CALL finalize_mpi
  call unset_common_nowcast
  call unset_common_mpi_nowcast
  call unset_letkf_obs
  deallocate(gues3d, gues2d, anal3d, anal2d)

  STOP
END PROGRAM letkf
