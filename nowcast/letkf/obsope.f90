PROGRAM obsope
!=======================================================================
!
! [PURPOSE:] Main program of observation operator
!
! [HISTORY:]
!   04/03/2013 Takemasa Miyoshi  created
!
!=======================================================================
  use mpi_f08
  USE common
  USE common_nowcast
  USE common_obs_nowcast
  USE letkf_nowcast_config
  use letkf_obs, only: nslots

  IMPLICIT NONE
  CHARACTER(9)  :: obsinfile  = 'obsin.dat'    !IN
  CHARACTER(11) :: guesfile   = 'gs01001.grd'  !IN
  CHARACTER(12) :: obsoutfile = 'obs01001.dat' !OUT
  REAL(r_size),ALLOCATABLE :: elem(:)
  REAL(r_size),ALLOCATABLE :: rlon(:)
  REAL(r_size),ALLOCATABLE :: rlat(:)
  REAL(r_size),ALLOCATABLE :: rlev(:)
  REAL(r_size),ALLOCATABLE :: odat(:)
  REAL(r_size),ALLOCATABLE :: oerr(:)
  REAL(r_size),ALLOCATABLE :: ohx(:)
  INTEGER,ALLOCATABLE :: oqc(:)
  REAL(r_size),ALLOCATABLE :: v3d(:,:,:,:)
  REAL(r_size),ALLOCATABLE :: v2d(:,:,:)
  !REAL(r_size),ALLOCATABLE :: p_full(:,:,:)
  REAL(r_size),PARAMETER :: threshold_dz=1000.0d0
  REAL(r_size) :: dz,tg,qg
  INTEGER :: nobs
  REAL(r_size) :: ri,rj,rk
  INTEGER :: n
  integer mpi_thread_provided, mpiprocs, myrank, mpi_ierr
  integer islot, iens

  call mpi_init_thread(MPI_THREAD_MULTIPLE, mpi_thread_provided, mpi_ierr)
  call mpi_comm_size(MPI_COMM_WORLD, mpiprocs, mpi_ierr)
  call mpi_comm_rank(MPI_COMM_WORLD, myrank, mpi_ierr)

  call read_config
  CALL set_common_nowcast


  CALL get_nobs(obsinfile, 6, nobs)
  ALLOCATE(elem(nobs), rlon(nobs), rlat(nobs), rlev(nobs), &
       &   odat(nobs), oerr(nobs), ohx(nobs), oqc(nobs) )
  CALL read_obs(obsinfile, nobs, elem, rlon, rlat, rlev, odat, oerr)
  ALLOCATE(v3d(nlon, nlat, nlev, nv3d), v2d(nlon, nlat, nv2d))
  !ALLOCATE( p_full(nlon,nlat,nlev) )

  do islot = 1, nslots
     do iens = 1, nbv
        if(mod(iens - 1 + (islot - 1) * nbv, mpiprocs) == myrank) then
           WRITE(guesfile(3:7), '(I2.2, I3.3)') islot, iens
           WRITE(obsoutfile(4:8), '(I2.2, I3.3)') islot, iens

           CALL read_grd(guesfile, v3d, v2d)
           !CALL calc_pfull(nlon,nlat,v2d(:,:,iv2d_ps),p_full)

           ohx=0.0d0
           oqc=0
           DO n=1,nobs
              !CALL phys2ijk(p_full,elem(n),rlon(n),rlat(n),rlev(n),ri,rj,rk)
              CALL phys2ijk(elem(n), rlon(n), rlat(n), rlev(n), ri, rj, rk)
              IF(ceiling_safe(ri, nlon + 1) < 2 .OR. nlon + 1 < ceiling_safe(ri, nlon + 1)) THEN
                 WRITE(6,'(A)') '* X-coordinate out of range'
                 WRITE(6,'(A,F6.2,A,F6.2)') '*   ri=',ri,', rlon=',rlon(n)
                 CYCLE
              END IF
              IF(ceiling_safe(rj, nlat) < 2 .OR. nlat < ceiling_safe(rj, nlat)) THEN
                 WRITE(6,'(A)') '* Y-coordinate out of range'
                 WRITE(6,'(A,F6.2,A,F6.2)') '*   rj=',rj,', rlat=',rlat(n)
                 !write(6, *) "ceiling = ", ceiling(rj)
                 !write(6, *) "floor = ", floor(rj)
                 !write(6, *) "ceiling - floor = ", ceiling(rj) - floor(rj)
                 !write(6, *) "ceiling_safe = ", ceiling_safe(rj, nlat)
                 CYCLE
              END IF
              IF(ceiling_safe(rk, nlev) > nlev) THEN
                 CALL itpl_2d(phi0,ri,rj,dz)
                 WRITE(6,'(A)') '* Z-coordinate out of range'
                 WRITE(6,'(A,F6.2,A,F10.2,A,F6.2,A,F6.2,A,F10.2)') &
                      & '*   rk=',rk,', rlev=',rlev(n),&
                      & ', (lon,lat)=(',rlon(n),',',rlat(n),'), phi0=',dz
                 CYCLE
              END IF
              !IF(CEILING(rk) < 2 .AND. NINT(elem(n)) /= id_ps_obs) THEN
              IF(ceiling(rk) < 2) THEN
                 IF(NINT(elem(n)) == id_u_obs .OR. NINT(elem(n)) == id_v_obs .OR. NINT(elem(n)) == id_w_obs) THEN
                    rk = 1.00001d0
                 ELSE
                    CALL itpl_2d(phi0, ri, rj, dz)
                    WRITE(6,'(A)') '* Z-coordinate out of range'
                    WRITE(6,'(A,F6.2,A,F10.2,A,F6.2,A,F6.2,A,F10.2)') &
                         & '*   rk=',rk,', rlev=',rlev(n),&
                         & ', (lon,lat)=(',rlon(n),',',rlat(n),'), phi0=',dz
                    CYCLE
                 END IF
              END IF
              !IF(NINT(elem(n)) == id_ps_obs .AND. odat(n) < -100.0d0) THEN
              !  CYCLE
              !END IF
              !   IF(NINT(tmpelm(nn+n)) == id_ps_obs) THEN
              !     CALL itpl_2d(phi0,ri,rj,dz)
              !     dz = dz - tmplev(nn+n)
              !     IF(ABS(dz) < threshold_dz) THEN ! pressure adjustment threshold
              !       CALL itpl_2d(t(:,:,1),ri,rj,tg)
              !       CALL itpl_2d(q(:,:,1),ri,rj,qg)
              !       CALL prsadj(tmpdat(nn+n),dz,tg,qg)
              !     ELSE
              !       PRINT '(A)','PS obs vertical adjustment beyond threshold'
              !       PRINT '(A,F10.2,A,F6.2,A,F6.2,A)',&
              !         & '  dz=',dz,', (lon,lat)=(',tmplon(nn+n),',',tmplat(nn+n),')'
              !       CYCLE
              !     END IF
              !   END IF
              !
              ! observational operator
              !
              !CALL Trans_XtoY(elem(n),ri,rj,rk,v3d,v2d,p_full,ohx(n))
              CALL Trans_XtoY(elem(n), ri, rj, rk, v3d, v2d, ohx(n))
              oqc(n) = 1
           END DO

           CALL write_obs2(obsoutfile, nobs, elem, rlon, rlat, rlev, odat, oerr, ohx, oqc)

           !DEALLOCATE( elem,rlon,rlat,rlev,odat,oerr,ohx,oqc,v3d,v2d,p_full )
        end if
     end do !iens
  end do !islot
  DEALLOCATE(elem, rlon, rlat, rlev, odat, oerr, ohx, oqc, v3d, v2d)

  CALL unset_common_nowcast

  call mpi_finalize(mpi_ierr)
END PROGRAM obsope
