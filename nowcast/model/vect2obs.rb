require "numru/gphys"
include NumRu

rmiss = 1e4

puts "usage: ruby vect2obs.rb lon.dat lat.dat lev.dat i_interval j_interval k_interval u_err v_err w_err w_enabled"

lonfile = ARGV[0] || "lon.dat"
latfile = ARGV[1] || "lat.dat"
levfile = ARGV[2] || "lev.dat"
iintv = (ARGV[3] || raise()).to_i
jintv = (ARGV[4] || raise()).to_i
kintv = (ARGV[5] || raise()).to_i
u_err = (ARGV[6] || raise()).to_f
v_err = (ARGV[7] || raise()).to_f
w_err = (ARGV[8] || raise()).to_f
w_enabled = ARGV[9]

lon = File.read(lonfile).split(" ").map!{|m| m.to_f}
lat = File.read(latfile).split(" ").map!{|m| m.to_f}
lev = File.read(levfile).split(" ").map!{|m| m.to_f}

nx = lon.length
ny = lat.length
nz = lev.length

if File.exist?("vector_u_001.nc")
  u = GPhys::IO.open("vector_u_001.nc", "u").val[true, true, true, 0]
else
  u = NArray.to_na(File.read("vector_u_001.bin"), NArray::SFLOAT, nx, ny, nz)
end
if File.exist?("vector_v_001.nc")
  v = GPhys::IO.open("vector_v_001.nc", "v").val[true, true, true, 0]
else
  v = NArray.to_na(File.read("vector_v_001.bin"), NArray::SFLOAT, nx, ny, nz)
end
if File.exist?("vector_w_001.nc")
  w = GPhys::IO.open("vector_w_001.nc", "w").val[true, true, true, 0]
else
  w = NArray.to_na(File.read("vector_w_001.bin"), NArray::SFLOAT, nx, ny, nz)
end

obslen = NArray.int(1).fill!(6 * 4).to_s
obslen_encoded = NArray.to_na(obslen, NArray::SFLOAT, 1)
obsdat = NArray.sfloat(6)
count = 0
File.open("obsin.dat", "w"){|fobs|

  iidx = NArray.int((nx - 1) / iintv + 1).indgen!(0, iintv)
  jidx = NArray.int((ny - 1) / jintv + 1).indgen!(0, jintv)
  kidx = NArray.int((nz - 1) / kintv + 1).indgen!(0, kintv)
  udat = u[iidx, jidx, kidx]
  vdat = v[iidx, jidx, kidx]
  wdat = w[iidx, jidx, kidx] if w_enabled
  mask = udat.lt(rmiss)
  londat = NArray.to_na(lon)[iidx].newdim(1, 1) * NArray.float(1, jidx.total, kidx.total).fill!(1)
  latdat = NArray.to_na(lat)[jidx].newdim(0, 1) * NArray.float(iidx.total, 1, kidx.total).fill!(1)
  levdat = NArray.to_na(lev)[kidx].newdim(0, 0) * NArray.float(iidx.total, jidx.total, 1).fill!(1)

  count = mask.count_true
  maskidx = mask.where

  totaldat = NArray.sfloat(8, w_enabled ? 3 : 2, count)
  totaldat[0, true, true] = obslen_encoded
  totaldat[1, 0, true] = 2819 # u wind
  totaldat[1, 1, true] = 2820 # v wind
  totaldat[1, 2, true] = 2821 if w_enabled # w wind
  totaldat[2, true, true] = londat[maskidx].newdim(0)
  totaldat[3, true, true] = latdat[maskidx].newdim(0)
  totaldat[4, true, true] = levdat[maskidx].newdim(0)
  totaldat[5, 0, true] = udat[maskidx]
  totaldat[5, 1, true] = vdat[maskidx]
  totaldat[5, 2, true] = wdat[maskidx] if w_enabled
  totaldat[6, 0, true] = u_err
  totaldat[6, 1, true] = v_err
  totaldat[6, 2, true] = w_err if w_enabled
  totaldat[7, true, true] = obslen_encoded

  fobs.print totaldat.to_s
}
puts "vect2obs: #{count * (w_enabled ? 3 : 2)} observations are converted"
